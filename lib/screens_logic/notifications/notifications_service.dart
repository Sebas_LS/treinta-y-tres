// import 'package:flutter/cupertino.dart';
// import 'package:flutter_local_notifications/flutter_local_notifications.dart';
//
// class LocalNotifications {
//
//   static final FlutterLocalNotificationsPlugin _flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
//
//   static Future init() async {
//
//     AndroidInitializationSettings initializationAdroindSettings = AndroidInitializationSettings('@mipmap/ic_launcher');
//     DarwinInitializationSettings initializationDarwinSettings = DarwinInitializationSettings(onDidReceiveLocalNotification: (id, title, body, payload) => null);
//     LinuxInitializationSettings initializationLinuxSettings = LinuxInitializationSettings(defaultActionName: 'Open notification');
//
//     final InitializationSettings initializationSettings = InitializationSettings(
//       android: initializationAdroindSettings,
//       iOS: initializationDarwinSettings,
//       linux: initializationLinuxSettings
//     );
//
//     _flutterLocalNotificationsPlugin.initialize(
//       initializationSettings,
//       onDidReceiveNotificationResponse: (details) => null,
//     );
//   }
//
//   static Future showNotification({
//     required String title,
//     required String body,
//     required String payload,
// }) async {
//     const AndroidNotificationDetails androidNotificationDetails = AndroidNotificationDetails(
//       'your channel id',
//       'your channel name',
//       channelDescription: 'your channel description',
//       importance: Importance.max,
//       priority: Priority.high,
//       ticker: 'ticker'
//     );
//
//     const NotificationDetails notificationDetails = NotificationDetails(android: androidNotificationDetails);
//     await _flutterLocalNotificationsPlugin.show(0, title, body, notificationDetails, payload: payload);
//   }
// }