import 'package:treinta_y_tres/application/application_dependencies.dart';
import 'package:http/http.dart' as http;

Future<List<String>> getDriverTypes() async {

  final response = await http.get(Uri.parse('${Enviroment.apiSecurityApp}/users/getDriversType'));

  if (response.statusCode == 200) {

    final List<dynamic> data = json.decode(response.body);
    List<String> driverTypes = [];

    for (var item in data) {
      driverTypes.add(item['type']);
    }

    return driverTypes;
  }

  else {
    throw Exception('Failed to load driver types');
  }
}
