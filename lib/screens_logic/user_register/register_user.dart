import 'package:treinta_y_tres/application/application_dependencies.dart';
import 'package:http/http.dart' as http;

Future<void> registerUser({
  BuildContext? context,
  String? username,
  String? email,
  String? password,
  String? name,
  String? plaque,
  String? keyword,
  String? carModelName,
  int? driverTypeId,
  String? image,
}) async {

  var request = http.MultipartRequest('POST', Uri.parse('${Enviroment.apiSecurityApp}/users/registerUser'));

  request.fields['username'] = username!;
  request.fields['email'] = email!;
  request.fields['password'] = password!;
  request.fields['name'] = name!;
  request.fields['plaque'] = plaque!;
  request.fields['keyword'] = keyword!;
  request.fields['carModelName'] = carModelName!;
  request.fields['driverTypeId'] = driverTypeId.toString();

  if (image != null && image.isNotEmpty) {
    request.files.add(await http.MultipartFile.fromPath('file', image));
  }

  var res = await request.send();

  if (res.statusCode == 200) {
    customScaffoldMessenger(context!, Icons.check_box, Colors.green, 'Cuenta creada con exito');
    Navigator.pushNamedAndRemoveUntil(context, '/login_user', (route) => false);
  } else {
    customScaffoldMessenger(context!, Icons.lock, Colors.red, 'Error al crear usuario');
  }
}