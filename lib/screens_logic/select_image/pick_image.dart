import 'package:treinta_y_tres/application/application_dependencies.dart';

Future<String?> pickImageToPhoto(ImageSource source) async {
  final ImagePicker picker = ImagePicker();
  final XFile? image = await picker.pickImage(source: source);

  if (image != null) {
    return image.path;
  } else {
    print('No se seleccionó ninguna imagen');
    return null;
  }
}