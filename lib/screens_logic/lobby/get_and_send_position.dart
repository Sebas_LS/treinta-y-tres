import 'package:geolocator/geolocator.dart';
import 'package:treinta_y_tres/application/application_dependencies.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;
import 'package:location/location.dart';
import 'package:treinta_y_tres/screens_logic/lobby/user_location_tracking.dart';
import 'package:treinta_y_tres/screens_logic/notifications/notifications_service.dart';
import 'dart:math' as math;
import '../../main.dart';
import 'package:awesome_notifications/awesome_notifications.dart';

class LobbySocketService {

  // final notificationsService = LocalNotifications();
  late IO.Socket _lobbySocket;
  LocationData? _currentPosition;
  double? _lastLatitude;
  double? _lastLongitude;
  Location location = Location();
  Timer? _locationTimer;

  void onConnectToLobbySocket(int userID, BuildContext context) {
    _lobbySocket = IO.io("${Enviroment.apiSecurityApp}/sockets/lobby", <String, dynamic> {
      'transports': ['websocket'],
      'autoConnect': false,
    });

    _lobbySocket.onConnect((_) {
      print('connected to lobby socket');
      _lobbySocket.emit('user_connected', userID);

      _lobbySocket.on('alert_triggered', (data) async {

        // LocalNotifications.showNotification(title: 'Alerta en proceso', body: 'Evaluando situación de emergencia cercana.', payload: '');

        final user = Provider.of<User>(context, listen: false);
        final currentUserID = user.id;
        final userInDangerID = data['userID'].toString();

        if (currentUserID == userInDangerID) {
          print('Ignorando la propia alerta de usuario');
          return;
        }

        final userInDangerData = Provider.of<UserInDangerData>(context, listen: false);
        userInDangerData.updateFromMap(data);

        Provider.of<UserInDangerNotifier>(context, listen: false).setUserInDangerId(data['userID'].toString());

        var locationService = Provider.of<LocationService>(context, listen: false);
        var currentPosition = locationService.currentLocation;

        if (currentPosition != null) {

          double distance = getDistanceBetweenTwoPoints(
            currentPosition.latitude,
            currentPosition.longitude,
            userInDangerData.latitude,
            userInDangerData.longitude,
          );

          if (distance > 12) {
            print('Demasiado lejos para mostar alerta de ayuda');
          } else {
            AwesomeNotifications().createNotification(
              content: NotificationContent(
                id: 10,
                channelKey: 'basic_channel',
                actionType: ActionType.Default,
                title: '¡Usuario en peligro!',
                body: 'Revisa sus datos y comprueba si puedes brindar asistencia.',
              )
            );
          }
        }
      });
    });

    _lobbySocket.onConnectError((data) {
      print('Connection error: $data');
    });

    _lobbySocket.onConnectTimeout((data) {
      print('Connection timeout: $data');
    });

    _lobbySocket.onDisconnect((_) {
      print('disconnected from lobby socket');
    });

    _lobbySocket.connect();
  }

  Future<void> _getCurrentLocation() async {
    try {
      _currentPosition = await location.getLocation();
    } on Exception {
      _currentPosition = null;
    }
  }

  void onEmergencyButtonPress(User user, String messageToSend, Position currentPosition, BuildContext context) {
    Map<String, dynamic> userData = {
      'socketId': _lobbySocket.id,
      'userID': user.id,
      'imageUrl': user.image,
      'name': user.name,
      'message': messageToSend,
      'carModel': user.carModel,
      'plaque': user.plaque,
      'keyword': user.keyword,
      'latitude': currentPosition.latitude,
      'longitude': currentPosition.longitude
    };

    print('User in danger ID: ${user.id}');

    _lobbySocket.emit('user_in_danger', userData);

    print('User in danger event emitted.');

    final alertStatusNotifier = Provider.of<AlertStatusNotifier>(context, listen: false);
    alertStatusNotifier.triggerAlert();

    customScaffoldMessenger(context, Icons.warning, Colors.green, 'Alerta disparada');

    startSendingLocation(user);
  }

  Future<bool> offerHelp(String helperID, String userInDangerID) async {
    Completer<bool> completer = Completer();

    _lobbySocket.once('room_full', (message) {
      print(message);
      if (!completer.isCompleted) {
        completer.complete(false);
      }
    });

    _lobbySocket.emit('offer_help', {
      'helperID': helperID,
      'userInDangerID': userInDangerID,
    });
    print('Offer help event emitted.');

    Future.delayed(const Duration(seconds: 5), () {
      if (!completer.isCompleted) {
        completer.complete(true);
      }
    });

    return completer.future;
  }

  void startSendingLocation(User user) {
    _locationTimer = Timer.periodic(const Duration(seconds: 1), (timer) async {
      Position currentPosition = await Geolocator.getCurrentPosition();
      if (_lastLatitude != currentPosition.latitude || _lastLongitude != currentPosition.longitude) {
        _lobbySocket.emit('update_location', {
          'userID': user.id,
          'latitude': currentPosition.latitude,
          'longitude': currentPosition.longitude
        });
        _lastLatitude = currentPosition.latitude;
        _lastLongitude = currentPosition.longitude;
      }
    });
  }

  void stopSendingLocation(String userID) {
    _locationTimer?.cancel();
    _lobbySocket.emit('stop_sending_location', {'userID': userID});
  }

  double getDistanceBetweenTwoPoints(lat1, lng1, lat2, lng2) {
    lat1 *= math.pi / 180;
    lng1 *= math.pi / 180;
    lat2 *= math.pi / 180;
    lng2 *= math.pi / 180;

    // Haversine formula
    final dLng = lng2 - lng1;
    final dLat = lat2 - lat1;

    final a = math.pow(math.sin(dLat / 2), 2) +
        math.cos(lat1) * math.cos(lat2) * math.pow(math.sin(dLng / 2), 2);

    final c = 2 * math.asin(math.sqrt(a));

    // Convert to KM (6371) - convert to miles (3956)
    return (c * 6371);
  }

  bool isConnected() {
    return _lobbySocket.connected;
  }

  void onDisconnectToLobbySocket() {
    _lobbySocket.disconnect();
  }
}

class DataTransmissionSocketService {

  late IO.Socket dataTransmissionSocket;
  bool isDialogShown = false;

  void onDataTransmissionSocketConnect(int roomID, BuildContext context) {
    dataTransmissionSocket = IO.io("${Enviroment.apiSecurityApp}/sockets/data-transmission", <String, dynamic>{
      'transports': ['websocket'],
      'autoConnect': false,
    });

    dataTransmissionSocket.onConnect((_) {
      print('connected to data transmission socket');

      dataTransmissionSocket.emit('join_room', roomID);

      final userID = Provider.of<User>(context, listen: false).id;
      print(userID);
      dataTransmissionSocket.emit('joined_data_transmission', {
        'socketId': dataTransmissionSocket.id,
        'roomID': roomID,
        'userA_id': userID
      });

      dataTransmissionSocket.on('user_in_danger_data', (data) {
        final userInDangerData = Provider.of<UserInDangerData>(context, listen: false);
        userInDangerData.updateFromMap(data);
        userInDangerData.notifyListeners();
      });

      dataTransmissionSocket.on('user_location_updated', (data) {
        print('User location updated: ${data['latitude']}, ${data['longitude']}');

        final userInDangerData = Provider.of<UserInDangerData>(context, listen: false);
        userInDangerData.updateFromMap(data);
        userInDangerData.notifyListeners();
      });

      dataTransmissionSocket.on('helper_has_left', (data) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20),
              ),
              title: Text(
                '¡Problema!',
                style: GoogleFonts.poppins(
                  fontSize: 15,
                  fontWeight: FontWeight.bold,
                  color: Colors.black87,
                ),
              ),
              content: Text(
                'El usuario que estaba intentando ayudarte se ha desconectado de tu sala',
                style: GoogleFonts.poppins(
                  fontSize: 15,
                  fontWeight: FontWeight.w500,
                  color: Colors.black,
                ),
              ),
              actions: <Widget>[
                Container(
                  margin: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.03),
                  child: TextButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text(
                      'Aceptar',
                      style: GoogleFonts.poppins(
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      ),
                    ),
                  ),
                ),
              ],
            );
          },
        );
      });

      dataTransmissionSocket.on('helper_coming', (data) {
        customScaffoldMessenger(context, Icons.check_box, Colors.green, '¡Ayuda en camino!');
      });
    });

    dataTransmissionSocket.on('user_stopped_sharing_location', (data) {
      if (!isDialogShown) {
        _handleUserStoppedSharingLocation(data, context);
      }
    });

    dataTransmissionSocket.onConnectError((data) {
      print('Connection error: $data');
    });

    dataTransmissionSocket.onConnectTimeout((data) {
      print('Connection timeout: $data');
    });

    dataTransmissionSocket.onDisconnect((_) {
      print('disconnected from data transmission socket');
    });

    dataTransmissionSocket.connect();
  }

  void _handleUserStoppedSharingLocation(dynamic data, BuildContext context) {
    isDialogShown = true;
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
          title: Text(
            '¡Ahora todo marcha bien!',
            style: GoogleFonts.poppins(
              fontSize: 18,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
          ),
          content: Text(
            'El usuario que disparó la alerta de peligro, notificó que ya está fuera de peligro',
            style: GoogleFonts.poppins(
              fontSize: 15,
              fontWeight: FontWeight.w500,
              color: Colors.black,
            ),
          ),
          actions: <Widget>[
            Container(
              margin: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.03),
              child: TextButton(
                onPressed: () {
                  isDialogShown = false;
                  final lobbySocketService = Provider.of<LobbySocketService>(context, listen: false);
                  lobbySocketService.onConnectToLobbySocket(Provider.of<User>(context, listen: false).id, navigatorKey.currentState!.context);
                  Navigator.of(context).pop();
                  Navigator.pushNamedAndRemoveUntil(navigatorKey.currentState!.context, '/lobby', (route) => false);
                },
                child: Text(
                  'Aceptar',
                  style: GoogleFonts.poppins(
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
              ),
            ),
          ],
        );
      },
    );
  }

  Future helperLeft(String roomId, String userID) async {
    dataTransmissionSocket.emit('helper_left', {
      'room': roomId,
      'userA_id': userID
    });
  }

  void emitHelperOnTheWay(String helperID, String userInDangerId) {
    dataTransmissionSocket.emit('helper_on_the_way', {
      'helperID': helperID,
      'userInDangerId': userInDangerId,
    });
  }

  void endDanger(int roomID) {
    print('Stop transmission $roomID');
    dataTransmissionSocket.emit('dangerEnded', {'roomID': roomID});
    dataTransmissionSocket.emit('stop_transmission', roomID);
  }

  void onDisconnectToDataTransmissionSocket() {
    dataTransmissionSocket.disconnect();
  }
}