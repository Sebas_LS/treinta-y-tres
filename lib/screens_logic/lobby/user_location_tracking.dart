import 'dart:async';
import 'package:flutter/foundation.dart';
import 'package:location/location.dart';

class LocationService with ChangeNotifier {
  Location location = Location();
  Timer? _timer;
  int _retryCount = 0;
  final int _maxRetries = 5;
  final int _retryInterval = 5;
  LocationData? _currentLocation;

  LocationService() {
    getCurrentLocation();
  }

  Future<bool> requestPermission() async {
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) return false;
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) return false;
    }

    return true;
  }

  Future<LocationData?> getCurrentLocation() async {
    try {
      _currentLocation = await location.getLocation();
      if (_isLocationValid(_currentLocation)) {
        notifyListeners();
      } else {
        _scheduleRetry();
      }
    } catch (e) {
      _scheduleRetry();
    }

    if (_isLocationValid(currentLocation)) {
      return currentLocation;
    } else {
      _scheduleRetry();
      return null;
    }
  }

  void updateCurrentLocation(LocationData newLocation) {
    _currentLocation = newLocation;
    notifyListeners();
  }

  void _scheduleRetry() {
    if (_retryCount < _maxRetries) {
      _retryCount++;
      _timer?.cancel();
      _timer = Timer(Duration(seconds: _retryInterval), getCurrentLocation);
    }
  }

  bool _isLocationValid(LocationData? locationData) {
    return locationData != null && locationData.latitude != null && locationData.longitude != null;
  }

  LocationData? get currentLocation => _currentLocation;
}