import 'package:treinta_y_tres/application/application_dependencies.dart';

void copyEmail(BuildContext context) {
  FlutterClipboard.copy('Consultas@treintaytresapp.com')
      .then((value) {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
          duration: Duration(milliseconds: 1000),
          content: Row(
            children: <Widget> [
              Icon(
                Icons.email,
                color: Colors.white,
              ),
              SizedBox(width: 20),
              Expanded(
                child: Text(
                  'Correo copiado',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    },
  );
}