import 'package:treinta_y_tres/application/application_dependencies.dart';
import 'package:http/http.dart' as http;

void updateProfile(BuildContext context, int id, String keyword, String carModel, String plaque, File? image) async {
  try {
    final url = Uri.parse('${Enviroment.apiSecurityApp}/users/updateProfile');
    var request = http.MultipartRequest('PUT', url);
    request.fields['id'] = id.toString();
    request.fields['keyword'] = keyword;
    request.fields['carModelName'] = carModel;
    request.fields['plaque'] = plaque;

    if(image != null && image.path.isNotEmpty) {
      request.files.add(await http.MultipartFile.fromPath('file', image.path));
      print("Image path: ${image.path}");
    }

    print("URL: $url");
    print("Fields: ${request.fields}");

    var response = await request.send();
    final respStr = await response.stream.bytesToString();
    print("Response status: ${response.statusCode}");
    print("Response body: $respStr");

    if (response.statusCode == 200) {
      var jsonResponse = jsonDecode(respStr);

      User user = Provider.of<User>(context, listen: false);
      user.updateFromJson(jsonResponse['user']);

      customScaffoldMessenger(context, Icons.update, Colors.green, 'Perfil actualizado');

    } else {
      customScaffoldMessenger(context, Icons.update, Colors.red, 'Error al actualizar el perfil');
    }
  } catch (e) {
    print("Error during profile update: $e");
  }
}