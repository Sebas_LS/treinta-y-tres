import 'package:treinta_y_tres/application/application_dependencies.dart';
import 'package:http/http.dart' as http;

Future<String> getDriverType(int driverTypeId) async {

  final url = Uri.parse('${Enviroment.apiSecurityApp}/users/getDriverTypeById/$driverTypeId');
  final response = await http.get(url);

  if (response.statusCode == 200) {
    var jsonResponse = jsonDecode(response.body);
    return jsonResponse['type'];
  } else {
    throw Exception('Failed to get driver type.');
  }
}