import 'package:treinta_y_tres/application/application_dependencies.dart';
import 'package:http/http.dart' as http;

import '../lobby/user_location_tracking.dart';

Future<Map<String, dynamic>> loginUser({
  BuildContext? context,
  String? email,
  String? password,
}) async {

  showDialog(
    context: context!,
    barrierDismissible: true,
    builder: (BuildContext context) {
      return AlertDialog(
        backgroundColor: const Color(0XFF1C1C1C),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              'Iniciando sesion',
              style: GoogleFonts.poppins(
                fontSize: 20,
                fontWeight: FontWeight.w600,
                color: Colors.white,
              ),
            ),
            const SizedBox(height: 30),
            const CircularProgressIndicator(
              color: Colors.white,
            ),
            const SizedBox(height: 30),
            Text(
              'Verificando datos',
              style: GoogleFonts.poppins(
                fontSize: 15,
                fontWeight: FontWeight.w500,
                color: Colors.white,
              ),
            ),
            const SizedBox(height: 10),
            Text(
              'Obteniendo ubicacion',
              style: GoogleFonts.poppins(
                fontSize: 15,
                fontWeight: FontWeight.w500,
                color: Colors.white,
              ),
            ),
          ],
        ),
      );
    },
  );

  final url = Uri.parse('${Enviroment.apiSecurityApp}/users/loginUser');
  final response = await http.post(
    url,
    headers: {"Content-Type": "application/json"},
    body: jsonEncode({
      'email': email,
      'password': password,
    }),
  );

  if (response.statusCode == 200) {

    Navigator.of(context).pop();

    print('Response body: ${response.body}');

    var jsonResponse = jsonDecode(response.body);

    User user = Provider.of<User>(context!, listen: false);
    user.updateFromJson(jsonResponse['user']);

    if (user.image != null) {
      RegisterNotifier registerNotifier = Provider.of<RegisterNotifier>(context, listen: false);
      String fixedUrl = user.image.replaceFirst("localhost", "10.0.2.2").replaceAll("\\", "/");
      registerNotifier.updateImagePath(fixedUrl);
    }

    var locationService = Provider.of<LocationService>(context, listen: false);
    var currentLocation = await locationService.getCurrentLocation();

    if (currentLocation != null) {
      customScaffoldMessenger(context!, Icons.check_box, Colors.green, 'Inicio de sesion exitoso');
      Navigator.pushNamedAndRemoveUntil(context, '/lobby', (route) => false);
    } else {
      customScaffoldMessenger(context, Icons.location_on, Colors.red, 'No se pudo obtener la ubicacion');
    }

    return jsonResponse;
  } else {
    Navigator.of(context).pop();
    customScaffoldMessenger(context, Icons.lock, Colors.red, 'Error al iniciar sesion');
    return throw Exception('Error de inicio de sesión');
  }
}