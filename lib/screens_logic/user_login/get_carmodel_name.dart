import 'package:treinta_y_tres/application/application_dependencies.dart';
import 'package:http/http.dart' as http;

Future<String> getCarModel(int id) async {

  final url = Uri.parse('${Enviroment.apiSecurityApp}/users/getCarModelByID/$id');
  final response = await http.get(url);

  if (response.statusCode == 200) {
    var jsonResponse = jsonDecode(response.body);
    return jsonResponse['model'];
  } else {
    throw Exception('Failed to get car model.');
  }
}