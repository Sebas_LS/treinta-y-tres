import 'package:shared_preferences/shared_preferences.dart';
import 'package:treinta_y_tres/application/application_dependencies.dart';
import 'dart:convert';

class SharedPrefManager {
  static Future<SharedPreferences> get _instance async =>
      _prefsInstance ??= await SharedPreferences.getInstance();
  static SharedPreferences? _prefsInstance;

  static Future<SharedPreferences?> init() async {
    _prefsInstance = await _instance;
    return _prefsInstance;
  }

  static Future<bool> putStringList(String key, List<Map<String, dynamic>> data) async {
    final prefs = await SharedPreferences.getInstance();
    List<String> stringList = data.map((item) => jsonEncode(item)).toList();
    return prefs.setStringList(key, stringList);
  }

  static Future<List<Map<String, dynamic>>?> getStringListEmergencyContacts(String key) async {
    final prefs = await SharedPreferences.getInstance();
    List<String>? stringList = prefs.getStringList(key);

    if (stringList == null) return null;

    List<Map<String, dynamic>> mapList = stringList.map((item) => jsonDecode(item) as Map<String, dynamic>).toList();
    return mapList;
  }

  static Future<List<Alert>> getAlertHistory() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> alertHistoryStrings = prefs.getStringList('alertHistory') ?? [];
    List<Alert> alertHistory = alertHistoryStrings.map((alertString) => Alert.fromJSON(json.decode(alertString))).toList();
    return alertHistory;
  }

  static Future<void> setAlertHistory(List<Alert> alertHistory) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> alertHistoryStrings = alertHistory.map((alert) => json.encode(alert.toJSON())).toList();
    await prefs.setStringList('alertHistory', alertHistoryStrings);
  }

  static Future<void> saveData(List<int> iconIndices) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    for(int i = 0; i < iconIndices.length; i++) {
      await prefs.setInt('iconIndex$i', iconIndices[i]);
    }
  }

  static Future<void> loadData(BuildContext context) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    IconDataNotifier iconDataNotifier = Provider.of<IconDataNotifier>(context, listen: false);
    for(int i = 0; i < 4; i++) {
      iconDataNotifier.updateIcon(i, iconDataNotifier.globalIcons[prefs.getInt('iconIndex$i') ?? i]);
    }
  }

  static Future<bool> setString(String key, String value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(key, value);
  }

  static Future<String?> getString(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(key);
  }

  static Future<bool> setBool(String key, bool value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setBool(key, value);
  }

  static Future<bool?> getBool(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getBool(key);
  }

  static Future<bool> setInt(String key, int value) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    bool result = await prefs.setInt(key, value);
    await prefs.commit();
    return result;
  }

  static Future<int?> getInt(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getInt(key);
  }

  static Future setSentAlerts(int value) async {
    var prefs = await _instance;
    prefs.setInt('sentAlerts', value);
  }

  static Future<int> getSentAlerts() async {
    var prefs = await _instance;
    return prefs.getInt('sentAlerts') ?? 0;
  }

  static Future setReceivedAlerts(int value) async {
    var prefs = await _instance;
    prefs.setInt('receivedAlerts', value);
  }

  static Future<int> getReceivedAlerts() async {
    var prefs = await _instance;
    return prefs.getInt('receivedAlerts') ?? 0;
  }

  static Future setTakenAlerts(int value) async {
    var prefs = await _instance;
    prefs.setInt('takenAlerts', value);
  }

  static Future<int> getTakenAlerts() async {
    var prefs = await _instance;
    return prefs.getInt('takenAlerts') ?? 0;
  }

  // static Future<bool> setStringList(String key, List<Map<String, dynamic>> list) async {
  //   final SharedPreferences prefs = await SharedPreferences.getInstance();
  //   final stringList = list.map((item) => json.encode(item)).toList();
  //   return prefs.setStringList(key, stringList);
  // }
  //
  // static Future<List<dynamic>> getStringList(String key) async {
  //   final SharedPreferences prefs = await SharedPreferences.getInstance();
  //   final stringList = prefs.getStringList(key);
  //   if (stringList != null) {
  //     return stringList.map((item) => json.decode(item)).toList();
  //   } else {
  //     return [];
  //   }
  // }
}