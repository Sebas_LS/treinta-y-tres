import 'package:treinta_y_tres/application/application_dependencies.dart';

final FlutterContactPicker contactPicker = FlutterContactPicker();

void addContact(
    EmergencyContactsNotifier emergencyContactsNotifier,
    EmergencyContactType contactType,
    Function updateState,
    ) async {
  PermissionStatus status = await Permission.contacts.status;

  if (status.isDenied) {
    status = await Permission.contacts.request();
  }

  if (status.isGranted) {
    Contact? contact = await contactPicker.selectContact();

    if (contact != null) {
      bool alreadyExists;
      switch (contactType) {
        case EmergencyContactType.danger:
          alreadyExists = emergencyContactsNotifier.dangerContacts.map((e) => e.name).contains(contact.fullName);
          break;
        case EmergencyContactType.unsafety:
          alreadyExists = emergencyContactsNotifier.unsafetyContacts.map((e) => e.name).contains(contact.fullName);
          break;
        case EmergencyContactType.suspicion:
          alreadyExists = emergencyContactsNotifier.suspicionContacts.map((e) => e.name).contains(contact.fullName);
          break;
        case EmergencyContactType.transit:
          alreadyExists = emergencyContactsNotifier.locationContacts.map((e) => e.name).contains(contact.fullName);
          break;
      }

      if (!alreadyExists) {
        EmergencyContact newContact = EmergencyContact(name: contact.fullName!);
        switch (contactType) {
          case EmergencyContactType.danger:
            emergencyContactsNotifier.addDangerContact(newContact);
            break;
          case EmergencyContactType.unsafety:
            emergencyContactsNotifier.addUnsafetyContact(newContact);
            break;
          case EmergencyContactType.suspicion:
            emergencyContactsNotifier.addSuspicionContact(newContact);
            break;
          case EmergencyContactType.transit:
            emergencyContactsNotifier.addLocationContact(newContact);
            break;
        }
        emergencyContactsNotifier.saveData();
        updateState();
      }
    }
  }
}

enum EmergencyContactType { danger, unsafety, suspicion, transit }