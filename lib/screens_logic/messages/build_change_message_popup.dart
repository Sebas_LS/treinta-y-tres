import 'package:treinta_y_tres/application/application_dependencies.dart';

void showChangeMessagePopup(
  BuildContext context,
  String backgroundURL,
  String message,
  String currentMessage,
  Function(String) onNewMessage,
  Color acceptButtonColor,
  Color textFieldDownBorderColor,
) {
  String newMessage = currentMessage;

  showDialog(
    context: context,
    builder: (BuildContext context) {
      return Dialog(
        backgroundColor: Colors.transparent,
        insetPadding: EdgeInsets.zero,
        child: Container(
          decoration: const BoxDecoration(
            color: Color(0XFFEAEAEA),
            borderRadius: BorderRadius.only(
              bottomRight: Radius.circular(70),
              topLeft: Radius.circular(70),
              topRight: Radius.circular(15),
              bottomLeft: Radius.circular(15),
            ),
          ),
          width: MediaQuery.of(context).size.width * 0.85,
          height: MediaQuery.of(context).size.height * 0.45,
          child: Column(
            children: [
              Container(
                width: double.infinity,
                height: MediaQuery.of(context).size.height * 0.25,
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(70),
                    topRight: Radius.circular(15),
                  ),
                  image: DecorationImage(
                    image: AssetImage(backgroundURL),
                    fit: BoxFit.cover,
                  ),
                ),
                child: Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.02,
                    left: MediaQuery.of(context).size.width * 0.07,
                    right: MediaQuery.of(context).size.width * 0.05,
                  ),
                  child: Text(
                    message,
                    style: GoogleFonts.montserrat(
                      fontSize: MediaQuery.of(context).size.height * 0.018,
                      fontWeight: FontWeight.bold,
                      color: Colors.black87,
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.02,
                  left: MediaQuery.of(context).size.width * 0.05,
                  right: MediaQuery.of(context).size.width * 0.05,
                ),
                width: double.infinity,
                child: Container(
                  margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.015,
                    left: MediaQuery.of(context).size.width * 0.05,
                    right: MediaQuery.of(context).size.width * 0.05,
                  ),
                  child: Column(
                    children: [
                      TextField(
                        decoration: InputDecoration(
                          hintText: currentMessage,
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: textFieldDownBorderColor),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: textFieldDownBorderColor),
                          ),
                        ),
                        style: GoogleFonts.montserrat(
                          fontSize: MediaQuery.of(context).size.height * 0.017,
                          fontWeight: FontWeight.bold,
                          color: Colors.black87,
                        ),
                        onChanged: (value) {
                          newMessage = value;
                        },
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.02,
                        ),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor: acceptButtonColor,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20),
                            ),
                            padding: EdgeInsets.symmetric(
                              horizontal: MediaQuery.of(context).size.width * 0.2,
                              vertical: MediaQuery.of(context).size.height * 0.015,
                            ),
                            elevation: 0,
                          ),
                          onPressed: () {
                            onNewMessage(newMessage);
                            Navigator.of(context).pop();
                          },
                          child: Text(
                            'Aceptar',
                            style: GoogleFonts.montserrat(
                              fontSize: MediaQuery.of(context).size.height * 0.017,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    },
  );
}