import 'package:location/location.dart';
import 'package:flutter/material.dart';
import 'package:treinta_y_tres/screens_logic/shared_preferences_manager/shared_preferences.dart';

Future showMapsRequestPermission(BuildContext context, Function granted, Function denied) async {
  Location location = Location();
  bool serviceEnabled;
  PermissionStatus permissionGranted;

  serviceEnabled = await location.serviceEnabled();
  if (!serviceEnabled) {
    serviceEnabled = await location.requestService();
    if (!serviceEnabled) {
      denied();
      return;
    }
  }

  permissionGranted = await location.hasPermission();
  switch (permissionGranted) {
    case PermissionStatus.granted:
    case PermissionStatus.grantedLimited:
      await SharedPrefManager.setBool('locationPermissionGranted', true);
      granted();
      break;
    case PermissionStatus.denied:
    case PermissionStatus.deniedForever:
      permissionGranted = await location.requestPermission();
      if (permissionGranted == PermissionStatus.granted || permissionGranted == PermissionStatus.grantedLimited) {
        await SharedPrefManager.setBool('locationPermissionGranted', true);
        granted();
      } else {
        denied();
      }
      break;
  }
}