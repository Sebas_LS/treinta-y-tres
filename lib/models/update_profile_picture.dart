import '../application/application_dependencies.dart';

class UserProfile {
  String imagePath;

  UserProfile({required this.imagePath});
}

class UserProfileNotifier with ChangeNotifier {
  UserProfile _userProfile = UserProfile(imagePath: '');

  UserProfile get userProfile => _userProfile;

  void updateImagePath(String newPath) {
    _userProfile.imagePath = newPath;
    notifyListeners();
  }
}