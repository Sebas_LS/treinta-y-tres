import 'package:treinta_y_tres/application/application_dependencies.dart';

class UserInDangerData with ChangeNotifier {
  String socketId = '';
  String imageUrl;
  String name;
  String message;
  String carModel;
  String plaque;
  String keyword;
  double latitude;
  double longitude;

  UserInDangerData({
    required this.imageUrl,
    required this.name,
    required this.message,
    required this.carModel,
    required this.plaque,
    required this.keyword,
    required this.latitude,
    required this.longitude,
  });

  void updateSocketId(String id) {
    socketId = id;
    notifyListeners();
  }

  factory UserInDangerData.fromMap(Map<String, dynamic> data) {
    return UserInDangerData(
      imageUrl: data['imageUrl'],
      name: data['name'],
      message: data['message'],
      carModel: data['carModel'],
      plaque: data['plaque'],
      keyword: data['keyword'],
      latitude: double.parse(data['latitude']),
      longitude: double.parse(data['longitude']),
    );
  }

  void updateFromMap(Map<String, dynamic> data) {
    imageUrl = data['imageUrl'];
    name = data['name'];
    message = data['message'];
    carModel = data['carModel'];
    plaque = data['plaque'];
    keyword = data['keyword'];
    latitude = data['latitude'] is String ? double.parse(data['latitude']) : data['latitude'];
    longitude = data['longitude'] is String ? double.parse(data['longitude']) : data['longitude'];

    notifyListeners();
  }

  void clear() {
    imageUrl = '';
    name = '';
    message = '';
    carModel = '';
    plaque = '';
    keyword = '';
    latitude = 0;
    longitude = 0;

    notifyListeners();
  }
}