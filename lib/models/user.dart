import 'package:treinta_y_tres/application/application_dependencies.dart';

class User with ChangeNotifier {
  int id;
  String name;
  String username;
  String email;
  String keyword;
  String carModel;
  String plaque;
  String driverType;
  String image;

  User({
    required this.id,
    required this.name,
    required this.username,
    required this.email,
    required this.keyword,
    required this.carModel,
    required this.plaque,
    required this.driverType,
    required this.image,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      id: json['id'],
      name: json['name'],
      username: json['username'],
      email: json['email'],
      keyword: json['keyword'],
      carModel: json['carModelId'],
      plaque: json['plaque'],
      driverType: json['driverTypeId'],
      image: json['image'],
    );
  }

  void updateFromJson(Map<String, dynamic> json) async {
    id = json['id'];
    name = json['name'];
    username = json['username'];
    email = json['email'];
    keyword = json['keyword'];
    plaque = json['plaque'];
    image = json['image'];
    carModel = await getCarModel(json['carModelId']);
    driverType = await getDriverType(json['driverTypeId']);

    notifyListeners();
  }

  void clear() {
    id = 0;
    name = '';
    username = '';
    email = '';
    keyword = '';
    carModel = '';
    plaque = '';
    driverType = '';
    image = '';

    notifyListeners();
  }
}