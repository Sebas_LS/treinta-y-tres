import 'package:treinta_y_tres/application/application_dependencies.dart';

class RequestMapPermission extends StatefulWidget {
  const RequestMapPermission({Key? key}) : super(key: key);

  @override
  State<RequestMapPermission> createState() => _RequestMapPermissionState();
}

class _RequestMapPermissionState extends State<RequestMapPermission> {

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: Color(0XFF1C1C1C),
      body: RequestMapPermissionBody(),
    );
  }
}