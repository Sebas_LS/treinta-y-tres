import 'package:treinta_y_tres/application/application_dependencies.dart';

class RequestMapPermissionBody extends StatefulWidget {
  const RequestMapPermissionBody({Key? key}) : super(key: key);

  @override
  State<RequestMapPermissionBody> createState() => _RequestMapPermissionBodyState();
}

class _RequestMapPermissionBodyState extends State<RequestMapPermissionBody> {

  @override
  Widget build(BuildContext context) {
    return const Stack(
      children: [
        BuildMapLogo(),
        BuildRequestMapPermissionNextButton(),
      ],
    );
  }
}
