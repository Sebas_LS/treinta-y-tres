import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildRequestMapPermissionNextButton extends StatefulWidget {
  const BuildRequestMapPermissionNextButton({Key? key}) : super(key: key);

  @override
  State<BuildRequestMapPermissionNextButton> createState() => _BuildRequestMapPermissionNextButtonState();
}

class _BuildRequestMapPermissionNextButtonState extends State<BuildRequestMapPermissionNextButton> {

  @override
  Widget build(BuildContext context) {

    double screenWidth = MediaQuery.of(context).size.width;

    return Positioned(
      top: MediaQuery.of(context).size.height * 0.8,
      child: SizedBox(
        height: MediaQuery.of(context).size.height * 0.2,
        width: screenWidth,
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.8,
                child: ElevatedButton(
                  onPressed: () {
                    showMapsRequestPermission(
                      context,
                      () => Navigator.pushNamedAndRemoveUntil(context, '/request_notifications_permission', (route) => false),
                      () => Navigator.pushNamedAndRemoveUntil(context, '/request_map_permission_denied', (route) => false),
                    );
                  },
                  style: ElevatedButton.styleFrom(
                    padding: const EdgeInsets.symmetric(vertical: 17),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                    backgroundColor: Colors.white,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Continuar',
                        style: GoogleFonts.poppins(
                          fontSize: 17,
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}