import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildMapLogo extends StatefulWidget {
  const BuildMapLogo({Key? key}) : super(key: key);

  @override
  State<BuildMapLogo> createState() => _BuildMapLogoState();
}

class _BuildMapLogoState extends State<BuildMapLogo> {

  @override
  Widget build(BuildContext context) {

    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    return Positioned(
      top: 0,
      child: SizedBox(
        height: MediaQuery.of(context).size.height * 0.8,
        width: screenWidth,
        child: SizedBox(
          height: MediaQuery.of(context).size.height * 0.8,
          width: double.infinity,
          child: Container(
            margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.1),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Image.asset(
                    'assets/images/request_map_permission_logo.png',
                    width: screenWidth * 0.48,
                    height: screenHeight * 0.18,
                  ),
                  Container(
                    margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
                    child: Text(
                      "Ubicacion",
                      style: GoogleFonts.poppins(
                        fontSize: 27,
                        fontWeight: FontWeight.w500,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0.05,
                      right: MediaQuery.of(context).size.height * 0.03,
                      left: MediaQuery.of(context).size.height * 0.03,
                    ),
                    child: Text(
                      "Permite el acceso a tu ubicacion todo el tiempo para una mejor experiencia",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                        fontSize: 17,
                        fontWeight: FontWeight.w500,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0.06,
                      right: MediaQuery.of(context).size.height * 0.03,
                    ),
                    child: Text(
                          "1 - Ve a informacion de Treinta Y Tres"
                          "\n"
                          "\n"
                          "2 - Entra en permisos de la aplicacion"
                          "\n"
                          "\n"
                          "3 - Selecciona: Ubicacion"
                          "\n"
                          "\n"
                          "4 - Selecciona: Permitir todo el tiempo",
                      textAlign: TextAlign.left,
                      style: GoogleFonts.poppins(
                        fontSize: 15,
                        fontWeight: FontWeight.w500,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}