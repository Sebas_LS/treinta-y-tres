import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildAlertsRecord extends StatefulWidget {
  const BuildAlertsRecord({Key? key}) : super(key: key);

  @override
  State<BuildAlertsRecord> createState() => _BuildAlertsRecordState();
}

class _BuildAlertsRecordState extends State<BuildAlertsRecord> {

  @override
  Widget build(BuildContext context) {
    return Consumer<AlertHistoryNotifier>(
      builder: (context, alertHistoryNotifier, child) {
        List<Alert> alerts = alertHistoryNotifier.alertHistory;
        return Container(
          margin: EdgeInsets.only(
              top: MediaQuery.of(context).size.height * 0.02,
              left: MediaQuery.of(context).size.width * 0.05,
              right: MediaQuery.of(context).size.width * 0.05
          ),
          child: ConstrainedBox(
            constraints: BoxConstraints(
              maxHeight: MediaQuery.of(context).size.height * 0.5,
            ),
            child: SingleChildScrollView(
              child: Column(
                children: alerts.isEmpty
                    ? [SizedBox(
                  width: MediaQuery.of(context).size.width * 0.855,
                  child: Container(
                    padding: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0.01,
                      bottom: MediaQuery.of(context).size.height * 0.01,
                      left: MediaQuery.of(context).size.width * 0.05,
                      right: MediaQuery.of(context).size.width * 0.05,
                    ),
                    decoration: const BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(5),
                        bottomRight: Radius.circular(40),
                        bottomLeft: Radius.circular(5),
                      ),
                      image: DecorationImage(
                        image: AssetImage('assets/background/drawer_header_background.png'),
                        fit: BoxFit.cover,
                      ),
                    ),
                    child: Text(
                      "No hay alertas",
                      style: GoogleFonts.poppins(
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      ),
                    ),
                  ),
                )]
                    : alerts.map((alert) => Container(
                  margin: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.025),
                  decoration: const BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(40),
                      topRight: Radius.circular(5),
                      bottomRight: Radius.circular(40),
                      bottomLeft: Radius.circular(5),
                    ),
                    image: DecorationImage(
                      image: AssetImage('assets/background/drawer_header_background.png'),
                      fit: BoxFit.cover,
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(5),
                    child: SizedBox(
                      width: MediaQuery.of(context).size.width * 0.9,
                      child: Column(
                        children: [
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.only(
                              top: MediaQuery.of(context).size.height * 0.02,
                              left: MediaQuery.of(context).size.width * 0.08,
                              right: MediaQuery.of(context).size.width * 0.1,
                            ),
                            child: Text(
                              'Emitiste una alerta el ${alert.date.day} del ${alert.date.month} del ${alert.date.year} a las ${alert.date.hour}:${alert.date.minute} PM con el título :',
                              style: GoogleFonts.poppins(
                                fontSize: 12,
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                              ),
                            ),
                          ),
                          Container(
                            alignment: Alignment.centerLeft,
                            margin: EdgeInsets.only(
                              top: MediaQuery.of(context).size.height * 0.01,
                              left: MediaQuery.of(context).size.width * 0.08,
                              right: MediaQuery.of(context).size.width * 0.1,
                              bottom: MediaQuery.of(context).size.width * 0.02,
                            ),
                            child: Text(
                              alert.title,
                              style: GoogleFonts.poppins(
                                fontSize: 13,
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                )).toList(),
              ),
            ),
          ),
        );
      },
    );
  }
}