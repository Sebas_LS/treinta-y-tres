import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildSectionTitle extends StatefulWidget {
  final String? title;
  final double? marginTop;
  final String? imageURL;
  final bool? deleteAlertRecord;

  const BuildSectionTitle({
    Key? key,
    required this.title,
    required this.marginTop,
    this.imageURL,
    required this.deleteAlertRecord,
  }) : super(key: key);

  @override
  State<BuildSectionTitle> createState() => _BuildSectionTitleState();
}

class _BuildSectionTitleState extends State<BuildSectionTitle> {

  @override
  Widget build(BuildContext context) {
    String imageToShow;

    if(widget.title == 'Estado de conexión'){
      imageToShow = Provider.of<ConnectionStatusNotifier>(context).isOnline
          ? 'assets/images/conexion_status_logo_online.png'
          : 'assets/images/conexion_status_offline_logo.png';
    } else {
      imageToShow = widget.imageURL!;
    }

    return Container(
      margin: EdgeInsets.only(
        top: widget.marginTop!,
        left: MediaQuery.of(context).size.width * 0.05,
      ),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          children: [
            Text(
              widget.title!,
              style: GoogleFonts.poppins(
                fontSize: 15,
                fontWeight: FontWeight.bold,
                color: Colors.white,
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05),
              child: Image.asset(
                imageToShow,
                width: MediaQuery.of(context).size.width * 0.065,
              ),
            ),
            const Spacer(),
            if (widget.deleteAlertRecord!)
              Container(
                margin: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.03),
                child: IconButton(
                  onPressed: () {
                    Provider.of<AlertHistoryNotifier>(context, listen: false).clearAlerts();
                  },
                  icon: Icon(
                    Icons.delete,
                    color: Colors.red,
                    size: MediaQuery.of(context).size.height * 0.04,
                  ),
                ),
              ),
          ]
        ),
      ),
    );
  }
}