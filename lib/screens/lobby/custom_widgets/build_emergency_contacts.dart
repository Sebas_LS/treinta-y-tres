import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildEmergencyContacts extends StatefulWidget {
  const BuildEmergencyContacts({Key? key}) : super(key: key);

  @override
  State<BuildEmergencyContacts> createState() => _BuildEmergencyContactsState();
}

class _BuildEmergencyContactsState extends State<BuildEmergencyContacts> {
  late EmergencyContactsNotifier emergencyContactsNotifier;
  late Future<void> loadContactsFuture;

  @override
  void initState() {
    super.initState();
    emergencyContactsNotifier = Provider.of<EmergencyContactsNotifier>(context, listen: false);
    loadContactsFuture = emergencyContactsNotifier.loadData();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: loadContactsFuture,
      builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
        if (snapshot.connectionState == ConnectionState.waiting) {
          return CircularProgressIndicator();
        } else if (snapshot.hasError) {
          return Text('Error: ${snapshot.error}');
        } else {
          EmergencyContactsNotifier emergencyContactsNotifier = Provider.of<EmergencyContactsNotifier>(context, listen: true);
          return Container(
            margin: EdgeInsets.only(
              top: MediaQuery.of(context).size.height * 0.01,
              left: MediaQuery.of(context).size.width * 0.05,
            ),
            height: MediaQuery.of(context).size.height * 0.15,
            child: emergencyContactsNotifier.getAllContacts().isEmpty
                ? SizedBox(
              width: MediaQuery.of(context).size.width * 0.855,
              child: Container(
                margin: EdgeInsets.only(
                  right: MediaQuery.of(context).size.width * 0.05,
                ),
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.01,
                  bottom: MediaQuery.of(context).size.height * 0.01,
                  left: MediaQuery.of(context).size.width * 0.05,
                  right: MediaQuery.of(context).size.width * 0.08,
                ),
                decoration: const BoxDecoration(
                  color: Colors.red,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(5),
                    bottomRight: Radius.circular(40),
                    bottomLeft: Radius.circular(5),
                  ),
                  image: DecorationImage(
                    image: AssetImage('assets/background/drawer_header_background.png'),
                    fit: BoxFit.cover,
                  ),
                ),
                child: Center(
                  child: Text(
                    "Tus contactos de confianza aparecerán acá",
                    textAlign: TextAlign.start,
                    style: GoogleFonts.poppins(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                  ),
                ),
              ),
            )
                : ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: emergencyContactsNotifier.getAllContacts().toSet().toList().length,
              itemBuilder: (context, index) {
                return SizedBox(
                  width: MediaQuery.of(context).size.width * 0.35,
                  height: MediaQuery.of(context).size.height * 0.1,
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(20),
                      image: const DecorationImage(
                        image: AssetImage('assets/background/home_default_message_background.jpeg'),
                        fit: BoxFit.cover,
                      ),
                    ),
                    margin: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0.01,
                      left: MediaQuery.of(context).size.width * 0.02,
                      right: MediaQuery.of(context).size.width * 0.02,
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        const CircleAvatar(
                          backgroundColor: Colors.transparent,
                          radius: 20,
                          backgroundImage: AssetImage('assets/images/user_logo.png'),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
                          child: Column(
                            children: [
                              Text(
                                emergencyContactsNotifier.getAllContacts()[index].name,
                                textAlign: TextAlign.center,
                                style: GoogleFonts.poppins(
                                  fontSize: 12,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),
          );
        }
      },
    );
  }
}
