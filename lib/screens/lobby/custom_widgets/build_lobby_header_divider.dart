import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildLobbyHeaderDivider extends StatefulWidget {
  const BuildLobbyHeaderDivider({Key? key}) : super(key: key);

  @override
  State<BuildLobbyHeaderDivider> createState() => _BuildLobbyHeaderDividerState();
}

class _BuildLobbyHeaderDividerState extends State<BuildLobbyHeaderDivider> {

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * 0.4,
      margin: EdgeInsets.only(
        top: MediaQuery.of(context).size.height * 0.02,
        left: MediaQuery.of(context).size.width * 0.1,
        right: MediaQuery.of(context).size.width * 0.1,
      ),
      child: const Divider(
        thickness: 1,
        color: Colors.white,
      ),
    );
  }
}
