import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildConnectionStatus extends StatefulWidget {
  const BuildConnectionStatus({Key? key}) : super(key: key);

  @override
  State<BuildConnectionStatus> createState() => _BuildConnectionStatusState();
}

class _BuildConnectionStatusState extends State<BuildConnectionStatus> {

  String? _connectionStatus = 'Desconectado';

  void onConnectToLobbySocket() async {
    final lobbySocketService = Provider.of<LobbySocketService>(context, listen: false);
    lobbySocketService.onConnectToLobbySocket(Provider.of<User>(context, listen: false).id, navigatorKey.currentState!.context);
  }

  void onDisconnectToLobbySocket() async {
    final lobbySocketService = Provider.of<LobbySocketService>(context, listen: false);
    lobbySocketService.onDisconnectToLobbySocket();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: MediaQuery.of(context).size.height * 0.01,
        left: MediaQuery.of(context).size.width * 0.05,
        right: MediaQuery.of(context).size.width * 0.05,
      ),
      child: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/background/suspicion_default_message_background.jpeg'),
            fit: BoxFit.cover,
          ),
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30),
            bottomRight: Radius.circular(30),
            bottomLeft: Radius.circular(5),
            topRight: Radius.circular(5),
          ),
        ),
        child: ListTile(
          title: Consumer2<ConnectionStatusNotifier, AlertStatusNotifier>(
            builder: (context, connectionStatus, alertStatus, child) {
              if (alertStatus.isAlertTriggered.value) {
                _connectionStatus = 'Emitiendo ubicación';
              } else {
                _connectionStatus = connectionStatus.isOnline ? 'Conectado' : 'Desconectado';
              }
              return Text(
                _connectionStatus!,
                style: GoogleFonts.poppins(
                  fontSize: 17,
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
              );
            }
          ),
          trailing: Consumer<ConnectionStatusNotifier>(
            builder: (context, connectionStatus, child) {
              return Switch(
                inactiveTrackColor: Colors.black45,
                activeColor: Colors.black,
                inactiveThumbColor: Colors.redAccent,
                value: connectionStatus.isOnline,
                onChanged: (value) {
                  connectionStatus.toggle();
                  if (connectionStatus.isOnline) {
                    onConnectToLobbySocket();
                    Provider.of<ButtonLockNotifier>(context, listen: false).unlockButtons();
                  } else {
                    if (Provider.of<AlertStatusNotifier>(context, listen: false).isAlertTriggered.value) {
                      Provider.of<AlertStatusNotifier>(context, listen: false).stopSendingData();
                    } else {
                      onDisconnectToLobbySocket();
                    }
                  }
                },
              );
            }
          ),
        ),
      ),
    );
  }
}
