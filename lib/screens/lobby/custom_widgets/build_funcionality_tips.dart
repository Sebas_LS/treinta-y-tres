import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildFuncionalityTips extends StatefulWidget {
  const BuildFuncionalityTips({Key? key}) : super(key: key);

  @override
  State<BuildFuncionalityTips> createState() => _BuildFuncionalityTipsState();
}

class _BuildFuncionalityTipsState extends State<BuildFuncionalityTips> {

  List<String> securityTipsImages = [
    'assets/images/funcionality_image_1.jpg',
    'assets/images/funcionality_image_2.jpg',
    'assets/images/funcionality_image_3.jpg',
    'assets/images/funcionality_image_4.jpg',
    'assets/images/funcionality_image_5.jpg',
    'assets/images/funcionality_image_6.jpg',
  ];

  List<String> securityTipsText = [
    'Estos celulares requieren una configuracion especial para funcionar correctamente, dirigete al inicio de aplicaciones, desactiva la gestion automatica y activa la opcion de ejecutar en segundo plano',
    'Para una mejor experiencia y un manejo mas fluído en cuanto a comunicacion se refiere, manten tu perfil al dia con los datos correctos. De esta manera al interactuar con usuarios no habran confusiones',
    'Ten en cuenta que para que la aplicacion funcione, tienes que estar conectado, asegurate de estar disponible cuando lo estes, para brindar asistencia de forma mas directa, tambien estar desconectado si no estas disponible',
    'Para que la aplicacion pueda compartir tu ubicacion aun si no esta en primer plano, asegurate de darle permisos todo el tiempo, ve a ajustes de la aplicacion, permisos, ubicacion y marca la opcion "Permitir todo el tiempo"',
    'Con el objetivo de que esta aplicacion vele por tu seguridad, la aplicacion cuenta con un estado sigiloso. Asegurate de colocar un fondo de pantalla que no llame la atencion, como por ejemplo tu fondo de pantalla normal',
    'Esta aplicacion da soporte a dispositivos NFC, esto para complementar e impulsar la idea de ser lo mas sigiloso posible en una desafortunada situacion, solo reposa tu celular en un dispositivo NFC y esta actuara automaticamente',
  ];

  List<String> securityTipsTitle = [
    'Telefonos Honor',
    'Perfil al dia',
    'Estado de conexion',
    'Ubicacion',
    'Fondo de pantalla',
    'Dispositivos NFC',
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: MediaQuery.of(context).size.height * 0.02,
        left: MediaQuery.of(context).size.width * 0.06,
        right: MediaQuery.of(context).size.width * 0.05,
      ),
      height: MediaQuery.of(context).size.height * 0.25,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: 6,
        itemBuilder: (context, index) {
          return Padding(
            padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.05),
            child: GestureDetector(
              onTap: () => _showDialog(context, securityTipsImages[index], securityTipsText[index]),
              child: Column(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width * 0.63,
                    height: MediaQuery.of(context).size.height * 0.14,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                        image: AssetImage(securityTipsImages[index]),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.55,
                    margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.03),
                    alignment: Alignment.centerLeft,
                    child: Row(
                      children: [
                        Expanded(
                          child: Text(
                            securityTipsTitle[index],
                            overflow: TextOverflow.ellipsis,
                            style: GoogleFonts.poppins(
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
                          child: const Icon(
                            Icons.arrow_circle_right,
                            size: 30,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  void _showDialog(BuildContext context, String imagePath, String dialogText) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          insetPadding: EdgeInsets.zero,
          child: Container(
            color: Colors.black87,
            width: MediaQuery.of(context).size.width * 0.85,
            height: MediaQuery.of(context).size.height * 0.45,
            child: Column(
              children: [
                Container(
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height * 0.23,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(imagePath),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.03,
                    left: MediaQuery.of(context).size.width * 0.05,
                    right: MediaQuery.of(context).size.width * 0.05,
                  ),
                  width: double.infinity,
                  child: Text(
                      dialogText,
                      style: GoogleFonts.poppins(
                        fontSize: 13,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      )
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}