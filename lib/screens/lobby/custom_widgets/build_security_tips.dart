import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildSecurityTips extends StatefulWidget {
  const BuildSecurityTips({Key? key}) : super(key: key);

  @override
  State<BuildSecurityTips> createState() => _BuildSecurityTipsState();
}

class _BuildSecurityTipsState extends State<BuildSecurityTips> {

  List<String> securityTipsImages = [
    'assets/images/tip_1.jpg',
    'assets/images/tip_2.jpg',
    'assets/images/tip_3.jpg'
  ];

  List<String> securityTipsText = [
    'Asegurate de ajustar todos tus dispositivos moviles antes de iniciar tu viaje. De esta manera, evitaras distracciones innecesarias durante el trayecto',
    'Siempre ten en cuenta tu velocidad. Un control adecuado de la misma puede ser la diferencia entre llegar seguro a tu destino o enfrentarte a situaciones de riesgo innecesarias',
    'Respeta a los peatones y ciclistas. Manten una distancia segura y siempre anticípate a sus movimientos, ellos tambien tienen derecho a moverse de forma segura'
  ];

  List<String> securityTipsTitle = [
    'Dispositivos',
    'Control de velocidad',
    'Respeta a los demás'
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: MediaQuery.of(context).size.height * 0.02,
        left: MediaQuery.of(context).size.width * 0.06,
        right: MediaQuery.of(context).size.width * 0.05,
      ),
      height: MediaQuery.of(context).size.height * 0.25,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: 3,
        itemBuilder: (context, index) {
          return Padding(
            padding: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.05),
            child: GestureDetector(
              onTap: () => _showDialog(context, securityTipsImages[index], securityTipsText[index]),
              child: Column(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width * 0.63,
                    height: MediaQuery.of(context).size.height * 0.14,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      image: DecorationImage(
                        image: AssetImage(securityTipsImages[index]),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.55,
                    margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.03),
                    alignment: Alignment.centerLeft,
                    child: Row(
                      children: [
                        Expanded(
                          child: Text(
                            securityTipsTitle[index],
                            overflow: TextOverflow.ellipsis,
                            style: GoogleFonts.poppins(
                              fontSize: 15,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                            ),
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.03),
                          child: const Icon(
                            Icons.arrow_circle_right,
                            size: 30,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  void _showDialog(BuildContext context, String imagePath, String dialogText) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          insetPadding: EdgeInsets.zero,
          child: Container(
            color: Colors.black87,
            width: MediaQuery.of(context).size.width * 0.85,
            height: MediaQuery.of(context).size.height * 0.41,
            child: Column(
              children: [
                Container(
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height * 0.23,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage(imagePath),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.03,
                    left: MediaQuery.of(context).size.width * 0.05,
                    right: MediaQuery.of(context).size.width * 0.05,
                  ),
                  width: double.infinity,
                  child: Text(
                    dialogText,
                    style: GoogleFonts.poppins(
                      fontSize: 13,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    )
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
