import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildLobbyHeader extends StatefulWidget {

  final PageController controller;
  final String? stateText;
  final int? state;
  final bool showDrawerIcon;

  const BuildLobbyHeader({
    Key? key,
    required this.controller,
    required this.stateText,
    required this.state,
    required this.showDrawerIcon
  }) : super(key: key);

  @override
  State<BuildLobbyHeader> createState() => _BuildLobbyHeaderState();
}

class _BuildLobbyHeaderState extends State<BuildLobbyHeader> {

  final ValueNotifier<bool> colorChangeNotifier = ValueNotifier<bool>(false);

  @override
  Widget build(BuildContext context) {
    return Builder(
      builder: (BuildContext context) {
        return Container(
          width: double.infinity,
          padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.08),
          child: Row(
            children: [
              if (widget.showDrawerIcon)
                Container(
                  margin: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05),
                  child: InkWell(
                    onTap: () {
                      Scaffold.of(context).openDrawer();
                    },
                    child: Image.asset(
                      'assets/images/open_drawer_icon.png',
                      fit: BoxFit.contain,
                      height: MediaQuery.of(context).size.height * 0.05,
                      color: Colors.white,
                    ),
                  ),
                ),
              Container(
                margin: EdgeInsets.only(
                  left: widget.showDrawerIcon
                    ? MediaQuery.of(context).size.width * 0.05
                    : MediaQuery.of(context).size.width * 0.1
                ),
                child: Image.asset(
                  'assets/application_logo/small_app_logo_white.png',
                  fit: BoxFit.contain,
                  height: MediaQuery.of(context).size.height * 0.03,
                  color: Colors.white,
                ),
              ),
              const Spacer(),
              Column(
                children: [
                  Container(
                    margin: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.1),
                    child: Column(
                      children: [
                        IconButton(
                          icon: const Icon(
                            Icons.swap_horiz,
                            size: 35,
                            color: Colors.white,
                          ),
                          onPressed: () {
                            widget.controller.animateToPage(
                              widget.state!,
                              duration: const Duration(milliseconds: 300),
                              curve: Curves.ease,
                            );
                          },
                        ),
                        Text(
                          widget.stateText!,
                          style: GoogleFonts.poppins(
                            fontSize: MediaQuery.of(context).size.height * 0.015,
                            fontWeight: FontWeight.bold,
                            color: Colors.white,
                          )
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}