import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:treinta_y_tres/application/application_dependencies.dart';
import 'package:google_fonts/google_fonts.dart';

class BuildDrawerHeader extends StatelessWidget {
  const BuildDrawerHeader({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    User user = Provider.of<User>(context, listen: false);
    final imagePath = user.image;

    return Container(
      height: MediaQuery.of(context).size.height * 0.2,
      padding: EdgeInsets.zero,
      margin: EdgeInsets.zero,
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/background/drawer_header_background.png'),
          fit: BoxFit.cover,
        ),
        color: Colors.white,
        borderRadius: BorderRadius.only(
          bottomRight: Radius.circular(80),
          topLeft: Radius.circular(50),
        ),
      ),
      child: Container(
        margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.05),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: imagePath.isEmpty ? const EdgeInsets.all(10) : const EdgeInsets.all(10),
              width: MediaQuery.of(context).size.width * 0.2,
              height: MediaQuery.of(context).size.height * 0.1,
              margin: const EdgeInsets.only(left: 10),
              decoration: const BoxDecoration(
                color: Color(0XFF1C1C1C),
                shape: BoxShape.circle,
              ),
              child: ClipOval(
                child: imagePath.isEmpty
                    ? Image.asset('assets/images/user_logo.png', fit: BoxFit.cover)
                    : Image.network(imagePath, fit: BoxFit.cover),
              ),
            ),
            SizedBox(width: MediaQuery.of(context).size.width * 0.04),
            Container(
              margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.008),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(user.name,
                      style: GoogleFonts.poppins(
                        fontSize: 15,
                        fontWeight: FontWeight.bold,
                        color: Colors.black87,
                      )),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.4,
                    margin: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0,
                    ),
                    child: const Divider(
                      thickness: 1,
                      color: Colors.black87,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0,
                    ),
                    child: Text(user.email,
                        style: GoogleFonts.poppins(
                          fontSize: 12,
                          fontWeight: FontWeight.bold,
                          color: Colors.black87,
                        )),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}