import 'package:treinta_y_tres/application/application_dependencies.dart';

class LobbyDrawer extends StatefulWidget {
  const LobbyDrawer({Key? key}) : super(key: key);

  @override
  State<LobbyDrawer> createState() => _LobbyDrawerState();
}

class _LobbyDrawerState extends State<LobbyDrawer> {

  @override
  Widget build(BuildContext context) {

    User user = Provider.of<User>(context, listen: false);
    RegisterNotifier registerNotifier = Provider.of<RegisterNotifier>(context, listen: false);

    return Drawer(
      backgroundColor: const Color(0xFF1C1C1C),
      child: ListView(
        children: <Widget>[
          const BuildDrawerHeader(),
          BuildDrawerItem(
            title: 'Perfil',
            subtitle: 'Editar perfil',
            icon: Icons.person,
            marginTop: MediaQuery.of(context).size.height * 0.05,
            onTap: () => {
              Navigator.pop(context),
              Navigator.pushNamed(context, '/profile'),
            },
          ),
          BuildDrawerItem(
            title: 'Mensajes',
            subtitle: 'Personalizar mensajes',
            icon: Icons.message,
            marginTop: MediaQuery.of(context).size.height * 0.02,
            onTap: () => {
              Navigator.pop(context),
              Navigator.pushNamed(context, '/messages'),
            },
          ),
          BuildDrawerItem(
            title: 'Contactenos',
            subtitle: 'Consultas@treintaytresapp.com',
            icon: Icons.app_settings_alt,
            marginTop: MediaQuery.of(context).size.height * 0.02,
            onTap: () => {copyEmail(context)},
          ),
          BuildDrawerItem(
            title: 'Sesion',
            subtitle: 'Cerrar sesion',
            icon: Icons.power_settings_new,
            marginTop: MediaQuery.of(context).size.height * 0.02,
            onTap: () => {
              user.clear(),
              registerNotifier.clearImagePath(),
              Navigator.pushNamedAndRemoveUntil(context, '/login_user', (route) => false),
              customScaffoldMessenger(context, Icons.check_box, Colors.green, 'Sesion cerrada'),
            },
          ),
        ],
      ),
    );
  }
}
