import 'package:treinta_y_tres/application/application_dependencies.dart';

import '../custom_widgets/build_funcionality_tips.dart';

class LobbyBody extends StatefulWidget {
  final PageController controller;
  const LobbyBody({Key? key, required this.controller}) : super(key: key);

  @override
  State<LobbyBody> createState() => _LobbyBodyState();
}

class _LobbyBodyState extends State<LobbyBody> {
  late EmergencyContactsNotifier emergencyContactsNotifier;
  String text = "Stop Service";

  @override
  void initState() {
    super.initState();
    emergencyContactsNotifier = Provider.of<EmergencyContactsNotifier>(context, listen: false);
    emergencyContactsNotifier.loadData();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        // MoveToBackground.moveTaskToBack();
        return false;
      },
      child: FutureBuilder(
        future: emergencyContactsNotifier.loadData(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return Scaffold(
              backgroundColor: const Color(0XFF1C1C1C),
              body: ListView(
                children: [
                  BuildLobbyHeader(
                    controller: widget.controller,
                    stateText: 'Pasar a sigiloso',
                    state: 1,
                    showDrawerIcon: true,
                  ),
                  const BuildLobbyHeaderDivider(),
                  BuildSectionTitle(
                    title: 'Estado de conexion',
                    imageURL: 'assets/images/conexion_status_offline_logo.png',
                    marginTop: MediaQuery.of(context).size.height * 0.02,
                    deleteAlertRecord: false,
                  ),
                  const BuildConnectionStatus(),
                  BuildSectionTitle(
                    title: 'Consejos de funcionalidad',
                    imageURL: 'assets/images/tips_logo.png',
                    marginTop: MediaQuery.of(context).size.height * 0.05,
                    deleteAlertRecord: false,
                  ),
                  const BuildFuncionalityTips(),
                  BuildSectionTitle(
                    title: 'Consejos de seguridad',
                    imageURL: 'assets/images/tips_logo.png',
                    marginTop: MediaQuery.of(context).size.height * 0.01,
                    deleteAlertRecord: false,
                  ),
                  const BuildSecurityTips(),
                  const BuildSectionTitle(
                    title: 'Historial de alertas',
                    imageURL: 'assets/images/danger_logo.png',
                    marginTop: 0,
                    deleteAlertRecord: true,
                  ),
                  const BuildAlertsRecord(),
                  Padding(padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.03)),
                ],
              ),
              drawer: const LobbyDrawer(),
            );
          } else {
            return const Scaffold(
              body: Center(
                child: CircularProgressIndicator(),
              ),
            );
          }
        },
      ),
    );
  }
}