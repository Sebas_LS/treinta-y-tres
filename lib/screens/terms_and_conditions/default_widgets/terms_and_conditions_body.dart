import 'package:treinta_y_tres/application/application_dependencies.dart';
import 'package:treinta_y_tres/reusable_widgets/user_agreement_information.dart' as user_agreement_information;

class TermsAndConditionsBody extends StatefulWidget {
  const TermsAndConditionsBody({Key? key}) : super(key: key);

  @override
  State<TermsAndConditionsBody> createState() => _TermsAndConditionsBodyState();
}

class _TermsAndConditionsBodyState extends State<TermsAndConditionsBody> {

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        buildContent(context, 'Términos y Condiciones', 0.11,
          children: [
            buildCustomText('Actualizado el 2022-08-12', 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Términos generales', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.termsAndConditionsFirstPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Licencia', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.termsAndConditionsSecondPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Definiciones y términos clave', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.termsAndConditionsThirdPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Restricciones', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.termsAndConditionsFourthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Política de Devolución y Reembolso', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.termsAndConditionsFifthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Tus sugerencias', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.termsAndConditionsSixthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Tu consentimiento', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.termsAndConditionsSeventhPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Enlaces a otros Sitios Web', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.termsAndConditionsEighthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Cookies', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.termsAndConditionsNinethPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Cambios en nuestros Términos y Condiciones', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.termsAndConditionsTenthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Modificaciones a nuestra plataforma', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.termsAndConditionsEleventhPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Actualizaciones a nuestra plataforma', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.termsAndConditionsTwelfthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Servicios de Terceros', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.termsAndConditionsThirteenthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Duración y Terminación', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.termsAndConditionsFourteenthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Aviso de infracción de Derechos de Autor', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.termsAndConditionsFifteenthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Indemnización', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.termsAndConditionsSixteenthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Sin garantías', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.termsAndConditionsSeventeenthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Limitación de Responsabilidad', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.termsAndConditionsEighteenthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Divisibilidad', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.termsAndConditionsNineteenthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Renuncia', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.termsAndConditionsTwentiethPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Enmiendas a este Acuerdo', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.termsAndConditionsTwentyFirstPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Acuerdo completo', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.termsAndConditionsTwentySecondPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Actualizaciones de nuestros Términos', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.termsAndConditionsTwentyThirdPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Propiedad intelectual', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.termsAndConditionsTwentyFourthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Acuerdo de Arbitraje', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.termsAndConditionsTwentyFifthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Aviso de Disputa', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.termsAndConditionsTwentySixthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Arbitraje Obligatorio', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.termsAndConditionsTwentySeventhPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Envíos y Privacidad', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.termsAndConditionsTwentyEighthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Promociones', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.termsAndConditionsTwentyNinethPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Errores Tipográficos', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.termsAndConditionsThirtiethPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Diverso', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.termsAndConditionsThirtyFirstPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Descargo de Responsabilidad', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.termsAndConditionsThirtySecondPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Contáctenos', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.termsAndConditionsThirtyThirdPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
          ],
          fontSize: 25,
        ),
      ],
    );
  }
}
