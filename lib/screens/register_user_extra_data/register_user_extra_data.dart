import 'package:treinta_y_tres/application/application_dependencies.dart';

class RegisterUserExtraData extends StatefulWidget {
  const RegisterUserExtraData({Key? key}) : super(key: key);

  @override
  State<RegisterUserExtraData> createState() => _RegisterUserExtraDataState();
}

class _RegisterUserExtraDataState extends State<RegisterUserExtraData> {

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: Color(0XFF1C1C1C),
      body: RegisterUserExtraDataBody(),
    );
  }
}