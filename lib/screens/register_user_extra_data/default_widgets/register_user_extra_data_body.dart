import 'package:treinta_y_tres/application/application_dependencies.dart';

class RegisterUserExtraDataBody extends StatefulWidget {
  const RegisterUserExtraDataBody({Key? key}) : super(key: key);

  @override
  State<RegisterUserExtraDataBody> createState() => _RegisterUserExtraDataBodyState();
}

class _RegisterUserExtraDataBodyState extends State<RegisterUserExtraDataBody> {

  bool isKeywordValid = false;
  bool isTermsAndConditionsChecked = false;

  @override
  Widget build(BuildContext context) {
    final registerData = Provider.of<RegisterNotifier>(context, listen: false).data;
    return SingleChildScrollView(
      child: Column(
        children: [
          const BuildCustomPaintBackNavigation(title: 'Registrarse', fontSize: 25),
          Container(
            width: double.infinity,
            height: MediaQuery.of(context).size.height * 0.88,
            decoration: const BoxDecoration(
              color: Color(0xFFF1F1F1),
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(120),
                bottomRight: Radius.circular(120),
                topRight: Radius.circular(15),
                bottomLeft: Radius.circular(15),
              ),
            ),
            child: Column(
              children: [
                const BuildUserPhotoAndHeaderText(),
                BuildActiveInput(
                  marginTop: MediaQuery.of(context).size.height * 0.04,
                  hintText: 'Palabra clave',
                  icon: Icons.assignment,
                  onChanged: (value) {
                    setState(() {
                      isKeywordValid = value.isNotEmpty;
                      Provider.of<RegisterNotifier>(context, listen: false).updateKeyword(value);
                    });
                  },
                  showEndIcon: true,
                ),
                const BuildDriversDropdown(),
                BuildTermsAndConditionsInfo(
                  callback: (value) {
                    setState(() {
                      isTermsAndConditionsChecked = value;
                    });
                  },
                ),
                BuildRegisterNextButton(
                  callback: () {

                    if (isKeywordValid && isTermsAndConditionsChecked) {

                      registerUser(
                        context: context,
                        username: registerData.username,
                        email: registerData.email,
                        password: registerData.password,
                        name: registerData.name,
                        plaque: registerData.plaque,
                        keyword: registerData.keyword,
                        carModelName: registerData.carModelName,
                        driverTypeId: registerData.driverTypeId,
                        image: registerData.imagePath,
                      );

                      RegisterNotifier registerNotifier = Provider.of<RegisterNotifier>(context, listen: false);
                      registerNotifier.updateImagePath('');

                    } else if (!isKeywordValid) {
                        customScaffoldMessenger(context, Icons.wrap_text, Colors.red, 'Llena todos los campos');
                    } else if (!isTermsAndConditionsChecked) {
                        customScaffoldMessenger(context, Icons.check_box_outline_blank, Colors.red, 'Acepta los terminos y condiciones');
                    }
                  }
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}