import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildTermsAndConditionsInfo extends StatefulWidget {
  final Function(bool)? callback;
  const BuildTermsAndConditionsInfo({Key? key, required this.callback}) : super(key: key);

  @override
  State<BuildTermsAndConditionsInfo> createState() => _BuildTermsAndConditionsInfoState();
}

class _BuildTermsAndConditionsInfoState extends State<BuildTermsAndConditionsInfo> {

  bool _isChecked = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 11, vertical: 3.5),
      margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.04, left: 35, right: 35),
      decoration: BoxDecoration(
        color: Colors.black87,
        borderRadius: BorderRadius.circular(30),
        boxShadow: const [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 10,
            offset: Offset(0, 3),
          ),
        ],
      ),
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.032),
            child: const Icon(
              Icons.assignment,
              color: Colors.white,
            ),
          ),
          Expanded(
            child: Container(
              alignment: Alignment.centerLeft,
              child: Container(
                margin: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.02),
                child: TextButton(
                  onPressed: () => Navigator.pushNamed(context, '/user_agreement'),
                  child: const Text(
                    'Términos y condiciones',
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w500,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
          ),
          Transform.scale(
            scale: 1.2,
            child: Checkbox(
              value: _isChecked,
              fillColor: MaterialStateProperty.all(Colors.white),
              onChanged: (bool? value) {
                setState(() {
                  _isChecked = value!;
                });
                widget.callback!(_isChecked);
              },
              activeColor: Colors.white,
              checkColor: Colors.black87,
            ),
          ),
        ],
      ),
    );
  }
}
