import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildUserPhotoAndHeaderText extends StatefulWidget {
  const BuildUserPhotoAndHeaderText({Key? key}) : super(key: key);

  @override
  State<BuildUserPhotoAndHeaderText> createState() => _BuildUserPhotoAndHeaderTextState();
}

class _BuildUserPhotoAndHeaderTextState extends State<BuildUserPhotoAndHeaderText> {

  String? _newImagePath;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.04),
      child: Column(
        children: [
          InkWell(
            onTap: () async {
              final String? imagePath = await pickImageToPhoto(ImageSource.gallery);
              setState(() {
                _newImagePath = imagePath;
              });

              RegisterNotifier registerNotifier = Provider.of<RegisterNotifier>(context, listen: false);
              registerNotifier.updateImagePath(_newImagePath!);
            },
            child: CircleAvatar(
              radius: 60,
              backgroundColor: Colors.black87,
              backgroundImage: _newImagePath == null || _newImagePath!.isEmpty
                  ? null
                  : FileImage(File(_newImagePath!)),
              child: _newImagePath == null || _newImagePath!.isEmpty
                  ? const Icon(Icons.person, size: 60, color: Colors.white)
                  : null,
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
            child: Text(
              'Sube una foto',
              style: GoogleFonts.poppins(
                fontSize: 18,
                fontWeight: FontWeight.w400,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              left:  MediaQuery.of(context).size.width * 0.15,
              right: MediaQuery.of(context).size.width * 0.15,
              top: MediaQuery.of(context).size.height * 0.01,
            ),
            child: const Divider(
              thickness: 1,
              color: Colors.black54,
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
            child: Text(
              'Datos adicionales',
              style: GoogleFonts.poppins(
                fontSize: 24,
                fontWeight: FontWeight.w400,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.005),
            child: Text(
              'Registro 3 / 3',
              style: GoogleFonts.poppins(
                fontSize: 15,
                fontWeight: FontWeight.w400,
                color: Colors.black,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
