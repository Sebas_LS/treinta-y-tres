import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildDriversDropdown extends StatefulWidget {
  const BuildDriversDropdown({Key? key}) : super(key: key);

  @override
  State<BuildDriversDropdown> createState() => _BuildDriversDropdownState();
}

class _BuildDriversDropdownState extends State<BuildDriversDropdown> {
  @override
  Widget build(BuildContext context) {
    return Consumer<DropdownSelectionModel>(
      builder: (context, model, child) {
        return FutureBuilder<List<String>>(
          future: getDriverTypes(),
          builder: (BuildContext context, AsyncSnapshot<List<String>> snapshot) {
            if (snapshot.hasData) {
              return Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(30),
                  boxShadow: const [
                    BoxShadow(
                      color: Colors.black26,
                      blurRadius: 10,
                      offset: Offset(0, 3),
                    ),
                  ],
                ),
                width: double.infinity,
                margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.04, left: 35, right: 35),
                padding: const EdgeInsets.only(left: 30, right: 30, top: 4, bottom: 4),
                child: DropdownButtonHideUnderline(
                  child: DropdownButton<String>(
                    dropdownColor: Colors.white,
                    icon: const Icon(Icons.arrow_drop_down, color: Colors.black87),
                    iconSize: 35,
                    elevation: 16,
                    items: snapshot.data!.map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(
                          value,
                          style: GoogleFonts.poppins(
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                            color: Colors.black87,
                          ),
                        ),
                      );
                    }).toList(),
                    onChanged: (String? newValue) {
                      model.setSelectedValue(newValue!);
                      if (newValue == 'Uber') Provider.of<RegisterNotifier>(context, listen: false).updateDriverType(1);
                      else if (newValue == 'Didi') Provider.of<RegisterNotifier>(context, listen: false).updateDriverType(2);
                      else if (newValue == 'Taxi') Provider.of<RegisterNotifier>(context, listen: false).updateDriverType(3);
                      else if (newValue == 'Circundante') Provider.of<RegisterNotifier>(context, listen: false).updateDriverType(4);
                    },
                    value: model.selectedValue,
                  ),
                ),
              );
            }
            else if (snapshot.hasError) {
              return Text('Error: ${snapshot.error}');
            }
            else {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        );
      },
    );
  }
}