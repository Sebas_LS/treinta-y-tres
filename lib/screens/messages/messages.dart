import 'package:treinta_y_tres/application/application_dependencies.dart';

class Messages extends StatefulWidget {
  const Messages({Key? key}) : super(key: key);

  @override
  State<Messages> createState() => _MessagesState();
}

class _MessagesState extends State<Messages> {

  void updateContacts() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: MessagesBody(
        updateContacts: updateContacts,
      ),
    );
  }
}