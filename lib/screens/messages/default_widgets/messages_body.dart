import 'package:shared_preferences/shared_preferences.dart';
import 'package:treinta_y_tres/application/application_dependencies.dart';

class MessagesBody extends StatefulWidget {
  final Function updateContacts;
  const MessagesBody({Key? key, required this.updateContacts}) : super(key: key);

  @override
  State<MessagesBody> createState() => _MessagesBodyState();
}

class _MessagesBodyState extends State<MessagesBody> {

  late ContactUpdateNotifier updateNotifier;
  String dangerMessage = 'Ayuda! Estoy en peligro';
  String unsafetyMessage = 'Siganme! Me siento inseguro';
  String suspicionMessage = 'Creo que me estan siguiendo';
  String locationMessage = 'Estoy de camino a casa';

  @override
  void initState() {
    super.initState();
    updateNotifier = Provider.of<ContactUpdateNotifier>(context, listen: false);
    Provider.of<EmergencyContactsNotifier>(context, listen: false).loadData();
    updateNotifier.addListener(updateContacts);
    loadMessages();
  }

  void loadMessages() async {
    dangerMessage = await SharedPrefManager.getString('dangerMessage') ?? 'Ayuda! Estoy en peligro';
    unsafetyMessage = await SharedPrefManager.getString('unsafetyMessage') ?? 'Siganme! Me siento inseguro';
    suspicionMessage = await SharedPrefManager.getString('suspicionMessage') ?? 'Creo que me estan siguiendo';
    locationMessage = await SharedPrefManager.getString('locationMessage') ?? 'Estoy de camino a casa';
    setState(() {});
  }

  @override
  void dispose() {
    updateNotifier.removeListener(updateContacts);
    super.dispose();
  }

  void updateContacts() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    EmergencyContactsNotifier emergencyContactsNotifier = Provider.of<EmergencyContactsNotifier>(context);
    List<EmergencyContact> dangerContacts = context.watch<EmergencyContactsNotifier>().dangerContacts;
    List<EmergencyContact> unsafetyContacts = emergencyContactsNotifier.unsafetyContacts;
    List<EmergencyContact> suspicionContacts = emergencyContactsNotifier.suspicionContacts;
    List<EmergencyContact> locationContacts = emergencyContactsNotifier.locationContacts;
    return Scaffold(
      backgroundColor: const Color(0XFF1C1C1C),
      body: SizedBox(
        height: double.infinity,
        width: double.infinity,
        child: Stack(
          children: <Widget>[
            CustomPaint(
              painter: BuildCustomPaintAppbar(),
              size: const Size(double.infinity, double.infinity),
            ),
            Column(
              children: [
                const BuildCustomAppbarContent(),
                const BuildMessagesScrollLimit(),
                Expanded(
                  child: ListView(
                    children: [
                      BuildDefaultMessage(
                        description: 'Estás en la sección de Peligro. Escribe un mensaje breve y claro para alertar a tus contactos de emergencia en caso de un incidente peligroso.',
                        message: dangerMessage,
                        icon: Icons.error,
                        backgroundImageURL: 'assets/background/danger_default_message_background.jpeg',
                        marginTop: 0,
                        marginBottom: 0,
                        effect3DColor: const Color(0xFF700000),
                        onMessagePressed: (newMessage){
                          setState(() {
                            dangerMessage = newMessage;
                            SharedPrefManager.setString('dangerMessage', newMessage);
                          });
                        },
                        acceptButtonColor: Colors.redAccent,
                        textFieldDownBorderColor: Colors.redAccent,
                        contacts: dangerContacts,
                        contactType: EmergencyContactType.danger,
                        contactTypeToDelete: EmergencyContactType.danger,
                        updateContacts: widget.updateContacts,
                        index: 0,
                      ),
                      BuildDefaultMessage(
                        description: 'Estás en la sección de Inseguridad. Redacta un mensaje conciso que exprese tu sensación de incomodidad o riesgo potencial a tus contactos de emergencia.',
                        message: unsafetyMessage,
                        icon: Icons.lock_open,
                        backgroundImageURL: 'assets/background/unsafety_default_message_background.jpeg',
                        marginTop: MediaQuery.of(context).size.height * 0.05,
                        marginBottom: 0,
                        effect3DColor: const Color(0xFF49005A),
                        onMessagePressed: (newMessage) {
                          setState(() {
                            unsafetyMessage = newMessage;
                            SharedPrefManager.setString('unsafetyMessage', newMessage);
                          });
                        },
                        acceptButtonColor: Colors.purple,
                        textFieldDownBorderColor: Colors.purple,
                        contacts: unsafetyContacts,
                        contactType: EmergencyContactType.unsafety,
                        contactTypeToDelete: EmergencyContactType.unsafety,
                        updateContacts: widget.updateContacts,
                        index: 1,
                      ),
                      BuildDefaultMessage(
                        description: 'Estás en la sección de Sospecha. Crea un mensaje breve que informe a tus contactos de emergencia sobre cualquier situación sospechosa que hayas percibido.',
                        message: suspicionMessage,
                        icon: Icons.visibility_off,
                        backgroundImageURL: 'assets/background/suspicion_default_message_background.jpeg',
                        marginTop: MediaQuery.of(context).size.height * 0.05,
                        marginBottom: 0,
                        effect3DColor: const Color(0xFF005A52),
                        onMessagePressed: (newMessage) {
                          setState(() {
                            suspicionMessage = newMessage;
                            SharedPrefManager.setString('suspicionMessage', newMessage);
                          });
                        },
                        acceptButtonColor: Colors.teal,
                        textFieldDownBorderColor: Colors.teal,
                        contacts: suspicionContacts,
                        contactType: EmergencyContactType.suspicion,
                        contactTypeToDelete: EmergencyContactType.suspicion,
                        updateContacts: widget.updateContacts,
                        index: 2,
                      ),
                      BuildDefaultMessage(
                        description: 'Estás en la sección de Tránsito. Redacta un mensaje corto para informar a tus contactos de emergencia que te encuentras en camino a tu destino.',
                        message: locationMessage,
                        icon: Icons.directions_car,
                        backgroundImageURL: 'assets/background/home_default_message_background.jpeg',
                        marginTop: MediaQuery.of(context).size.height * 0.05,
                        marginBottom: MediaQuery.of(context).size.height * 0.05,
                        effect3DColor: const Color(0xFF0B3C01),
                        onMessagePressed: (newMessage) {
                          setState(() {
                            locationMessage = newMessage;
                            SharedPrefManager.setString('locationMessage', newMessage);
                          });
                        },
                        acceptButtonColor: Colors.green,
                        textFieldDownBorderColor: Colors.green,
                        contacts: locationContacts,
                        contactType: EmergencyContactType.transit,
                        contactTypeToDelete: EmergencyContactType.transit,
                        updateContacts: widget.updateContacts,
                        index: 3,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}