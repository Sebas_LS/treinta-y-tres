import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildMessagesScrollLimit extends StatefulWidget {
  const BuildMessagesScrollLimit({Key? key}) : super(key: key);

  @override
  State<BuildMessagesScrollLimit> createState() => _BuildMessagesScrollLimitState();
}

class _BuildMessagesScrollLimitState extends State<BuildMessagesScrollLimit> {

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        top: MediaQuery.of(context).size.height * 0.1,
      ),
    );
  }
}
