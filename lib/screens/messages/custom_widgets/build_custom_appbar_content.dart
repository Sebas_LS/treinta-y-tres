import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildCustomAppbarContent extends StatefulWidget {
  const BuildCustomAppbarContent({Key? key}) : super(key: key);

  @override
  State<BuildCustomAppbarContent> createState() => _BuildCustomAppbarContentState();
}

class _BuildCustomAppbarContentState extends State<BuildCustomAppbarContent> {

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: MediaQuery.of(context).size.height * 0.05,
        left: MediaQuery.of(context).size.width * 0.05,
        right: MediaQuery.of(context).size.width * 0.02,
      ),
      alignment: Alignment.center,
      child: Stack(
        children: [
          Container(
            alignment: Alignment.centerLeft,
            width: MediaQuery.of(context).size.width * 0.9,
            child: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: const Icon(
                Icons.arrow_circle_left,
                color: Color(0XFF1C1C1C),
                size: 35,
              ),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width * 0.9,
            alignment: Alignment.center,
            margin: EdgeInsets.only(
              top: MediaQuery.of(context).size.height * 0.02,
            ),
            child: Text(
              'Personaliza tus datos',
              style: GoogleFonts.montserrat(
                fontSize: 17,
                fontWeight: FontWeight.bold,
                color: const Color(0XFF1C1C1C),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
