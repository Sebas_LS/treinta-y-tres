import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildDefaultMessage extends StatefulWidget {

  final String? description;
  final String? message;
  final String? backgroundImageURL;
  final double? marginTop;
  final double? marginBottom;
  final Color? effect3DColor;
  final Color? textFieldDownBorderColor;
  final Color? acceptButtonColor;
  final IconData icon;
  final Function(String) onMessagePressed;
  final List<EmergencyContact> contacts;
  final EmergencyContactType contactType;
  final EmergencyContactType contactTypeToDelete;
  final Function updateContacts;
  final int? index;

  const BuildDefaultMessage({
    Key? key,
    required this.description,
    required this.message,
    required this.backgroundImageURL,
    required this.marginTop,
    required this.marginBottom,
    required this.effect3DColor,
    required this.textFieldDownBorderColor,
    required this.acceptButtonColor,
    required this.icon,
    required this.onMessagePressed,
    required this.contacts,
    required this.contactType,
    required this.contactTypeToDelete,
    required this.updateContacts,
    required this.index,
  }) : super(key: key);

  @override
  State<BuildDefaultMessage> createState() => _BuildDefaultMessageState();
}

class _BuildDefaultMessageState extends State<BuildDefaultMessage> {
  late IconData _icon;
  int? _index;

  @override
  void initState() {
    super.initState();
    _icon = widget.icon;
    _index = widget.index;
    loadIconIndex();
    SharedPrefManager.loadData(navigatorKey.currentState!.context);
  }

  void loadIconIndex() async {
    _index = await SharedPrefManager.getInt('iconIndexSelected${widget.index}') ?? widget.index;
    IconDataNotifier iconDataNotifier = Provider.of<IconDataNotifier>(context, listen: false);
    _icon = iconDataNotifier.globalIcons[_index!];
  }

  @override
  Widget build(BuildContext context) {
    EmergencyContactsNotifier emergencyContactsNotifier = Provider.of<EmergencyContactsNotifier>(context, listen: false);
    EmergencyContact contacts = EmergencyContact(name: 'Contactos');
    IconDataNotifier iconDataNotifier = Provider.of<IconDataNotifier>(context, listen: false);
    return Container(
      margin: EdgeInsets.only(
        left: MediaQuery.of(context).size.width * 0.02,
        right: MediaQuery.of(context).size.width * 0.02,
      ),
      alignment: Alignment.center,
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              color: Colors.white,
              image: DecorationImage(
                image: AssetImage(widget.backgroundImageURL!),
                fit: BoxFit.cover,
              ),
              borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(40),
                bottomRight: Radius.circular(80),
                topRight: Radius.circular(5),
                bottomLeft: Radius.circular(5),
              ),
              boxShadow: [
                BoxShadow(
                  color: Color(widget.effect3DColor!.value),
                  offset: const Offset(5, 5),
                  spreadRadius: 4.0,
                ),
              ],
            ),
            margin: EdgeInsets.only(
              top: widget.marginTop!,
              left: MediaQuery.of(context).size.width * 0.05,
              right: MediaQuery.of(context).size.width * 0.05,
              bottom: widget.marginBottom!,
            ),
            width: MediaQuery.of(context).size.width,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * 0.02,
                    left: MediaQuery.of(context).size.width * 0.05,
                    right: MediaQuery.of(context).size.width * 0.06,
                    bottom: MediaQuery.of(context).size.height * 0.03,
                  ),
                  child: Row(
                    children: [
                      SizedBox(
                        width: MediaQuery.of(context).size.width * 0.6,
                        child: GestureDetector(
                          onTap: () => showChangeMessagePopup(
                            context,
                            widget.backgroundImageURL!,
                            widget.description!,
                            widget.message!,
                            widget.onMessagePressed,
                            widget.acceptButtonColor!,
                            widget.textFieldDownBorderColor!,
                          ),
                          child: Container(
                            padding: EdgeInsets.only(
                              top: MediaQuery.of(context).size.height * 0.01,
                              bottom: MediaQuery.of(context).size.height * 0.01,
                              right: MediaQuery.of(context).size.height * 0.01,
                              left: MediaQuery.of(context).size.height * 0.01,
                            ),
                            decoration: BoxDecoration(
                              color: widget.effect3DColor,
                              borderRadius: const BorderRadius.only(
                                topLeft: Radius.circular(5),
                                topRight: Radius.circular(5),
                                bottomLeft: Radius.circular(5),
                                bottomRight: Radius.circular(20),
                              ),
                            ),
                            child: Container(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                widget.message!,
                                overflow: TextOverflow.ellipsis,
                                style: GoogleFonts.montserrat(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      const Spacer(),
                      SizedBox(
                        height: MediaQuery.of(context).size.height * 0.06,
                        width: MediaQuery.of(context).size.width * 0.1,
                        child: GestureDetector(
                          onTap: () {
                            showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return AlertDialog(
                                  backgroundColor: const Color(0XFF1C1C1C),
                                  title: Column(
                                    children: [
                                      Text(
                                        'Elige un ícono',
                                        style: GoogleFonts.montserrat(
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white,
                                        ),
                                      ),
                                      Container(
                                        margin: EdgeInsets.only(
                                          top: MediaQuery.of(context).size.height * 0.02,
                                        ),
                                        child: const Divider(
                                          color: Colors.white,
                                          thickness: 1.5,
                                        ),
                                      ),
                                    ],
                                  ),
                                  content: SizedBox(
                                    width: double.maxFinite,
                                    height: MediaQuery.of(context).size.height * 0.5,
                                    child: GridView.count(
                                      crossAxisCount: 3,
                                      children: List.generate(iconDataNotifier.globalIcons.length, (index) {
                                        return GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              IconDataNotifier iconDataNotifier = Provider.of<IconDataNotifier>(context, listen: false);
                                              iconDataNotifier.updateIcon(widget.index!, iconDataNotifier.globalIcons[index]);
                                              _icon = iconDataNotifier.globalIcons[index];
                                              SharedPrefManager.setInt('iconIndex${widget.index}', index);
                                              Navigator.of(context).pop();
                                            });
                                          },
                                          child: Container(
                                            color: Colors.transparent,
                                            child: Icon(
                                              iconDataNotifier.globalIcons[index],
                                              color: Colors.white,
                                              size: 25,
                                            ),
                                          ),
                                        );
                                      }),
                                    ),
                                  ),
                                );
                              }
                            );
                          },
                          child: CircleAvatar(
                            backgroundColor: widget.effect3DColor,
                            child: Icon(
                              _icon,
                              color: Colors.white,
                              size: 25,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                // Container(
                //   margin: EdgeInsets.only(
                //     top: MediaQuery.of(context).size.height * 0.005,
                //     left: MediaQuery.of(context).size.width * 0.05,
                //     right: MediaQuery.of(context).size.width * 0.05,
                //   ),
                //   child: Row(
                //     children: [
                //       Expanded(
                //         flex: 1,
                //         child: SizedBox(
                //           width: MediaQuery.of(context).size.width,
                //           child: Container(
                //             decoration: const BoxDecoration(
                //               borderRadius: BorderRadius.only(
                //                 topLeft: Radius.circular(5),
                //                 topRight: Radius.circular(5),
                //                 bottomLeft: Radius.circular(5),
                //                 bottomRight: Radius.circular(20),
                //               ),
                //             ),
                //             padding: EdgeInsets.only(
                //               left: MediaQuery.of(context).size.height * 0.005,
                //             ),
                //             child: DropdownButton<EmergencyContact>(
                //               isExpanded: true,
                //               dropdownColor: Colors.white,
                //               icon: Icon(
                //                 Icons.arrow_drop_down,
                //                 color: widget.effect3DColor,
                //                 size: 25,
                //               ),
                //               iconSize: 24,
                //               elevation: 16,
                //               underline: Container(
                //                 height: 2,
                //                 color: widget.effect3DColor,
                //               ),
                //               style: GoogleFonts.montserrat(
                //                 fontSize: 15,
                //                 fontWeight: FontWeight.w500,
                //                 color: const Color(0XFF1C1C1C),
                //               ),
                //               value: contacts,
                //               items: [contacts, ...widget.contacts].map<DropdownMenuItem<EmergencyContact>>((EmergencyContact contact) {
                //                 return DropdownMenuItem<EmergencyContact>(
                //                   value: contact,
                //                   child: Row(
                //                     children: [
                //                       Expanded(
                //                         child: Text(
                //                           contact.name,
                //                           style: GoogleFonts.montserrat(
                //                             fontSize: 15,
                //                             fontWeight: FontWeight.bold,
                //                             color: const Color(0XFF1C1C1C),
                //                           ),
                //                         ),
                //                       ),
                //                       if (contact.name != 'Contactos')
                //                         IconButton(
                //                           icon: const Icon(
                //                             Icons.delete,
                //                             color: Colors.redAccent,
                //                           ),
                //                           onPressed: () {
                //                             Navigator.pop(context);
                //                             context.read<EmergencyContactsNotifier>().removeContact(widget.contactTypeToDelete, contact.name);
                //                             context.read<ContactUpdateNotifier>().update();
                //                           },
                //                         ),
                //                     ],
                //                   ),
                //                 );
                //               }).toList(),
                //               onChanged: (EmergencyContact? newValue) {},
                //             ),
                //           ),
                //         ),
                //       ),
                //       Expanded(
                //         flex: 1,
                //         child: Container(
                //           alignment: Alignment.centerLeft,
                //           child: IconButton(
                //             onPressed: () {
                //               addContact(
                //                 emergencyContactsNotifier,
                //                 widget.contactType,
                //                 () => setState((){}),
                //               );
                //             },
                //             icon: CircleAvatar(
                //               backgroundColor: widget.effect3DColor,
                //               radius: 20,
                //               child: const Icon(
                //                 Icons.person_add_alt,
                //                 color: Colors.white,
                //                 size: 20,
                //               ),
                //             )
                //           ),
                //         ),
                //       ),
                //     ],
                //   ),
                // ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}