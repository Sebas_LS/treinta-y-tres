import 'package:treinta_y_tres/application/application_dependencies.dart';

class DisclaimerAndTerms extends StatefulWidget {
  const DisclaimerAndTerms({Key? key}) : super(key: key);

  @override
  State<DisclaimerAndTerms> createState() => _DisclaimerAndTermsState();
}

class _DisclaimerAndTermsState extends State<DisclaimerAndTerms> {

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: Color(0XFF1C1C1C),
      body: DisclaimerAndTermsBody(),
    );
  }
}