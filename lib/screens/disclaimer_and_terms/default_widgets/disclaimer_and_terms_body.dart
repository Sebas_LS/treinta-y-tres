import 'package:treinta_y_tres/application/application_dependencies.dart';
import 'package:treinta_y_tres/reusable_widgets/user_agreement_information.dart' as user_agreement_information;

class DisclaimerAndTermsBody extends StatefulWidget {
  const DisclaimerAndTermsBody({Key? key}) : super(key: key);

  @override
  State<DisclaimerAndTermsBody> createState() => _DisclaimerAndTermsBodyState();
}

class _DisclaimerAndTermsBodyState extends State<DisclaimerAndTermsBody> {

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        buildContent(context, 'Descargo de Responsabilidad', 0.11,
          children: [
            buildCustomText('Actualizado el 2022-08-12', 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Treinta y Tres por la presente le otorga acceso a la plataforma y lo invita a comprar los servicios que se ofrecen aquí.', 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Definiciones y términos clave', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.disclaimerFirstPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('De Responsabilidad Limitada', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.disclaimerSecondPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Descargo de Responsabilidad de Enlaces a otros Sitios Web', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.disclaimerThirdPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Descargo de Responsabilidad por Errores y Omisiones', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.disclaimerFourthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Descargo de Responsabilidad General', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.disclaimerFifthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Divulgación Legal', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.disclaimerSyxthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Su consentimiento', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.disclaimerSeventhPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Cambios en nuestro Descargo de Responsabilidad', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.disclaimerEighthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Contáctenos', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.disclaimerNinethPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
          ],
        ),
      ],
    );
  }
}
