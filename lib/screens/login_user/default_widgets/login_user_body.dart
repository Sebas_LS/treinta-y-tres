import 'package:treinta_y_tres/application/application_dependencies.dart';

class LoginUserBody extends StatefulWidget {
  const LoginUserBody({Key? key}) : super(key: key);

  @override
  State<LoginUserBody> createState() => _LoginUserBodyState();
}

class _LoginUserBodyState extends State<LoginUserBody> {

  @override
  Widget build(BuildContext context) {
    return const SingleChildScrollView(
      child: Column(
        children: [
          BuildAppLogo(),
          BuildLoginForm(),
        ],
      ),
    );
  }
}