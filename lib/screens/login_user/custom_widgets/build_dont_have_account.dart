import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildDontHaveAccount extends StatefulWidget {
  const BuildDontHaveAccount({Key? key}) : super(key: key);

  @override
  State<BuildDontHaveAccount> createState() => _BuildDontHaveAccountState();
}

class _BuildDontHaveAccountState extends State<BuildDontHaveAccount> {

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.075),
      child: TextButton(
        onPressed: () => Navigator.pushNamed(context, '/register_user_data'),
        child: Text(
          '¿No tienes una cuenta?',
          style: GoogleFonts.poppins(
            fontSize: 16,
            fontWeight: FontWeight.w500,
            color: Colors.black,
          ),
        ),
      ),
    );
  }
}