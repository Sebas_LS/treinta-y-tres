import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildLoginForm extends StatefulWidget {
  const BuildLoginForm({Key? key}) : super(key: key);

  @override
  State<BuildLoginForm> createState() => _BuildLoginFormState();
}

class _BuildLoginFormState extends State<BuildLoginForm> {

  String email = '';
  String password = '';

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: MediaQuery.of(context).size.height * 0.7,
      decoration: const BoxDecoration(
        color: Color(0xFFF1F1F1),
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(120),
          bottomRight: Radius.circular(120),
          topRight: Radius.circular(15),
          bottomLeft: Radius.circular(15),
        ),
      ),
      child: Column(
        children: [
          const BuildLoginFormHeader(),
          BuildActiveInput(
            marginTop: MediaQuery.of(context).size.height * 0.03,
            icon: Icons.email,
            hintText: 'Correo electrónico',
            onChanged: (value) => email = value,
            obscureText: false,
          ),
          BuildActiveInput(
            marginTop: MediaQuery.of(context).size.height * 0.05,
            icon: Icons.lock,
            hintText: 'Contraseña',
            onChanged: (value) => password = value,
            obscureText: true,
          ),
          BuildUserLoginNextButton(callback: () {loginUser(context: context, email: email, password: password);}),
          const BuildDontHaveAccount(),
        ],
      ),
    );
  }
}