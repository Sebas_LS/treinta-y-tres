import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildLoginFormHeader extends StatefulWidget {
  const BuildLoginFormHeader({Key? key}) : super(key: key);

  @override
  State<BuildLoginFormHeader> createState() => _BuildLoginFormHeaderState();
}

class _BuildLoginFormHeaderState extends State<BuildLoginFormHeader> {

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.05),
          child: Text(
            'Iniciar sesion',
            style: GoogleFonts.poppins(
              fontSize: 26,
              fontWeight: FontWeight.w400,
              color: Colors.black,
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(
            left:  MediaQuery.of(context).size.width * 0.2,
            right: MediaQuery.of(context).size.width * 0.2,
            top: MediaQuery.of(context).size.height * 0.01,
          ),
          child: const Divider(
            thickness: 1,
            color: Colors.black54,
          ),
        ),
      ],
    );
  }
}
