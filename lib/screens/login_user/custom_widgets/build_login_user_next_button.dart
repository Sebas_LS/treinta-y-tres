import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildUserLoginNextButton extends StatefulWidget {
  final Function? callback;
  const BuildUserLoginNextButton({Key? key, required this.callback}) : super(key: key);

  @override
  State<BuildUserLoginNextButton> createState() => _BuildUserLoginNextButtonState();
}

class _BuildUserLoginNextButtonState extends State<BuildUserLoginNextButton> {

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.08),
      child: ElevatedButton(
        onPressed: () => widget.callback!(),
        style: ElevatedButton.styleFrom(
          backgroundColor: Colors.black87,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30),
          ),
          minimumSize: const Size(300, 50),
        ),
        child: const Text(
          'Login',
          style: TextStyle(
            fontSize: 19,
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ),
      ),
    );
  }
}