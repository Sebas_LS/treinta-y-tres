import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildAppLogo extends StatefulWidget {
  const BuildAppLogo({Key? key}) : super(key: key);

  @override
  State<BuildAppLogo> createState() => _BuildAppLogoState();
}

class _BuildAppLogoState extends State<BuildAppLogo> {

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.3,
      width: double.infinity,
      decoration: const BoxDecoration(
        color: Color(0XFF1C1C1C),
      ),
      child: Container(
        margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.04),
        child: Image.asset(
          'assets/application_logo/big_app_logo_login.png',
        ),
      ),
    );
  }
}