import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildActiveInput extends StatefulWidget {

  double? marginTop;
  String? hintText;
  IconData? icon;
  Function? onChanged;
  bool? obscureText;
  bool? showEndIcon;

  BuildActiveInput({
    Key? key,
    required this.marginTop,
    required this.hintText,
    required this.icon,
    this.onChanged,
    this.obscureText = false,
    this.showEndIcon = false,
  }) : super(key: key);

  @override
  State<BuildActiveInput> createState() => _BuildActiveInputState();
}

class _BuildActiveInputState extends State<BuildActiveInput> {

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 11, vertical: 3.5),
      margin: EdgeInsets.only(top: widget.marginTop!, left: 35, right: 35),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(30),
        boxShadow: const [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 10,
            offset: Offset(0, 3),
          ),
        ],
      ),
      child: TextField(
        obscureText: widget.obscureText!,
        onChanged: (value) => widget.onChanged!(value),
        style: const TextStyle(
          fontSize: 19,
          fontWeight: FontWeight.w500,
          color: Colors.black,
        ),
        decoration: InputDecoration(
          border: InputBorder.none,
          contentPadding: const EdgeInsets.only(top: 8.0, left: 10.0),
          prefixIcon: Icon(
            widget.icon,
            color: Colors.black,
          ),
          suffixIcon: widget.showEndIcon!
            ? IconButton(
            onPressed: () => {
              showCustomDialog(
                context,
                'Esto servira para que los demas usuarios verifiquen que eres tu',
                'Aceptar',
                () => {
                  Navigator.pop(context),
                }
              ),
            },
            icon: Icon(
              Icons.info,
              color: Colors.black,
              size: MediaQuery.of(context).size.height * 0.04,
            ),
          ) : null,
          hintText: widget.hintText!,
          hintStyle: TextStyle(
            color: Colors.black.withOpacity(0.5),
            fontSize: 17,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
    );
  }
}