import 'package:treinta_y_tres/application/application_dependencies.dart';
import '../../main.dart';
import '../../screens_logic/lobby/user_location_tracking.dart';

class UserLogin extends StatefulWidget {
  const UserLogin({Key? key}) : super(key: key);

  @override
  State<UserLogin> createState() => _UserLoginState();
}

class _UserLoginState extends State<UserLogin> {

  bool _isLocationInitialized = false;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    if (!_isLocationInitialized) {
      _getCurrentLocation();
      _isLocationInitialized = true;
    }
  }

  void _getCurrentLocation() async {
    var locationService = Provider.of<LocationService>(context);
    var permissionGranted = await locationService.requestPermission();
    if (permissionGranted) {
      var currentLocation = await locationService.getCurrentLocation();
      if (currentLocation != null) {
        print("Ubicacion obtenida: Latitud: ${currentLocation.latitude}, Longitud: ${currentLocation.longitude}");
      } else {
        print("No se pudo obtener la ubicación.");
      }
    } else {
      print("Permisos de ubicación no concedidos.");
    }
  }

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: Color(0XFF1C1C1C),
      body: LoginUserBody(),
    );
  }
}
