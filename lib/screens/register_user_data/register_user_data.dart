import 'package:treinta_y_tres/application/application_dependencies.dart';

class RegisterUserData extends StatefulWidget {
  const RegisterUserData({Key? key}) : super(key: key);

  @override
  State<RegisterUserData> createState() => _RegisterUserDataState();
}

class _RegisterUserDataState extends State<RegisterUserData> {

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: Color(0XFF1C1C1C),
      body: RegisterUserDataBody(),
    );
  }
}