import 'package:treinta_y_tres/application/application_dependencies.dart';

class RegisterUserDataBody extends StatefulWidget {
  const RegisterUserDataBody({Key? key}) : super(key: key);

  @override
  State<RegisterUserDataBody> createState() => _RegisterUserDataBodyState();
}

class _RegisterUserDataBodyState extends State<RegisterUserDataBody> {

  bool isNameValid = false;
  bool isUsernameValid = false;
  bool isEmailValid = false;
  bool isPasswordValid = false;
  bool doPasswordsMatch = false;
  String password = '';
  String confirmPassword = '';

  @override
  Widget build(BuildContext context) {
    final registerNotifier = Provider.of<RegisterNotifier>(context, listen: false);
    return SingleChildScrollView(
      child: Column(
        children: [
          const BuildCustomPaintBackNavigation(title: 'Registrarse', fontSize: 25),
          Container(
            width: double.infinity,
            height: MediaQuery.of(context).size.height * 0.8,
            decoration: const BoxDecoration(
              color: Color(0xFFF1F1F1),
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(120),
                bottomRight: Radius.circular(120),
                topRight: Radius.circular(15),
                bottomLeft: Radius.circular(15),
              ),
            ),
            child: Column(
              children: [
                const BuildRegisterHeaderText(
                  title: 'Datos personales',
                  subtitle: 'Registro 1 / 3',
                ),
                BuildActiveInput(
                  marginTop: MediaQuery.of(context).size.height * 0.03,
                  hintText: 'Nombre completo',
                  icon: Icons.person,
                  onChanged: (value) {
                    setState(() {
                      isNameValid = value.isNotEmpty;
                      registerNotifier.updateName(value);
                    });
                  },
                ),
                BuildActiveInput(
                  marginTop: MediaQuery.of(context).size.height * 0.03,
                  hintText: 'Nombre de usuario',
                  icon: Icons.person,
                  onChanged: (value) {
                    setState(() {
                      isUsernameValid = value.isNotEmpty;
                      registerNotifier.updateUsername(value);
                    });
                  },
                ),
                BuildActiveInput(
                  marginTop: MediaQuery.of(context).size.height * 0.03,
                  hintText: 'Correo electronico',
                  icon: Icons.email,
                  onChanged: (value) {
                    setState(() {
                      isEmailValid = value.isNotEmpty;
                      registerNotifier.updateEmail(value);
                    });
                  },
                ),
                BuildActiveInput(
                  marginTop: MediaQuery.of(context).size.height * 0.03,
                  hintText: 'Contraseña',
                  icon: Icons.password,
                  obscureText: true,
                  onChanged: (value) {
                    setState(() {
                      isPasswordValid = value.isNotEmpty;
                      registerNotifier.updatePassword(value);
                      password = value;
                    });
                  },
                ),
                BuildActiveInput(
                  marginTop: MediaQuery.of(context).size.height * 0.03,
                  hintText: 'Confirmar contraseña',
                  icon: Icons.password,
                  obscureText: true,
                  onChanged: (value) {
                    setState(() {
                      confirmPassword = value;
                      if (confirmPassword == password) {
                        doPasswordsMatch = true;
                      } else {
                        doPasswordsMatch = false;
                      }
                    });
                  },
                ),
                BuildRegisterNextButton(
                  callback: () {
                    if (isUsernameValid &&
                        isEmailValid &&
                        isNameValid &&
                        isPasswordValid &&
                        doPasswordsMatch) {
                      Navigator.pushNamed(context, '/register_user_car_data');
                    } else if (!isUsernameValid ||
                        !isEmailValid ||
                        !isNameValid) {
                      customScaffoldMessenger(context, Icons.wrap_text, Colors.red, 'Llena todos los campos');
                    } else if (!doPasswordsMatch) {
                      customScaffoldMessenger(context, Icons.lock, Colors.red, 'Las contraseña no coinciden');
                    }
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}