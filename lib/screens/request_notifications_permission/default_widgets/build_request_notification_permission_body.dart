import 'package:treinta_y_tres/application/application_dependencies.dart';

import '../custom_widgets/build_next_button.dart';
import '../custom_widgets/build_notification_logo.dart';

class RequestNotificationPermissionBody extends StatefulWidget {
  const RequestNotificationPermissionBody({Key? key}) : super(key: key);

  @override
  State<RequestNotificationPermissionBody> createState() => _RequestNotificationPermissionBodyState();
}

class _RequestNotificationPermissionBodyState extends State<RequestNotificationPermissionBody> {

  @override
  Widget build(BuildContext context) {
    return const Stack(
      children: [
        BuildNotificationLogo(),
        BuildRequestNotificationPermissionNextButton(),
      ],
    );
  }
}
