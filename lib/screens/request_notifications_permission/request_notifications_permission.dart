import 'package:treinta_y_tres/application/application_dependencies.dart';
import 'default_widgets/build_request_notification_permission_body.dart';

class RequestNotificationPermission extends StatefulWidget {
  const RequestNotificationPermission({Key? key}) : super(key: key);

  @override
  State<RequestNotificationPermission> createState() => _RequestNotificationPermissionState();
}

class _RequestNotificationPermissionState extends State<RequestNotificationPermission> {

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: Color(0XFF1C1C1C),
      body: RequestNotificationPermissionBody(),
    );
  }
}