import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class BuildNotificationLogo extends StatefulWidget {
  const BuildNotificationLogo({Key? key}) : super(key: key);

  @override
  State<BuildNotificationLogo> createState() => _BuildNotificationLogoState();
}

class _BuildNotificationLogoState extends State<BuildNotificationLogo> {

  @override
  Widget build(BuildContext context) {

    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    return Positioned(
      top: 0,
      child: SizedBox(
        height: MediaQuery.of(context).size.height * 0.8,
        width: screenWidth,
        child: SizedBox(
          height: MediaQuery.of(context).size.height * 0.8,
          width: double.infinity,
          child: Container(
            margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.22),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Image.asset(
                    'assets/images/request_notification_loco.png',
                    width: screenWidth * 1,
                    height: screenHeight * 0.3,
                  ),
                  Text(
                    "Notificaciones",
                    style: GoogleFonts.poppins(
                      fontSize: 27,
                      fontWeight: FontWeight.w500,
                      color: Colors.white,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0.05,
                      right: MediaQuery.of(context).size.height * 0.03,
                      left: MediaQuery.of(context).size.height * 0.03,
                    ),
                    child: Text(
                      "Por favor, permite el uso de notificaciones para el uso correcto de la aplicacion",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}