import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildRequestNotificationPermissionNextButton extends StatefulWidget {
  const BuildRequestNotificationPermissionNextButton({Key? key}) : super(key: key);

  @override
  State<BuildRequestNotificationPermissionNextButton> createState() => _BuildRequestNotificationPermissionNextButtonState();
}

class _BuildRequestNotificationPermissionNextButtonState extends State<BuildRequestNotificationPermissionNextButton> {

  Future<void> _requestNotificationPermission() async {
    var status = await Permission.notification.status;
    if (status.isDenied) {
      await Permission.notification.request();
      openAppSettings();
    }

    if (await Permission.notification.isGranted) {
      Navigator.pushNamedAndRemoveUntil(context, '/login_user', (route) => false);
    }
  }

  @override
  Widget build(BuildContext context) {

    double screenWidth = MediaQuery.of(context).size.width;

    return Positioned(
      top: MediaQuery.of(context).size.height * 0.8,
      child: SizedBox(
        height: MediaQuery.of(context).size.height * 0.2,
        width: screenWidth,
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.8,
                child: ElevatedButton(
                  onPressed: _requestNotificationPermission,
                  style: ElevatedButton.styleFrom(
                    padding: const EdgeInsets.symmetric(vertical: 12),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                    backgroundColor: Colors.white,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Aceptor',
                        style: GoogleFonts.poppins(
                          fontSize: 17,
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}