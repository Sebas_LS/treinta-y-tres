import 'package:treinta_y_tres/application/application_dependencies.dart';

class StealthModeDrawer extends StatefulWidget {

  final PageController controller;
  final bool showItem;
  final Function deleteBackground;

  const StealthModeDrawer({
    Key? key,
    required this.controller,
    required this.showItem,
    required this.deleteBackground
  }) : super(key: key);

  @override
  State<StealthModeDrawer> createState() => _StealthModeDrawerState();
}

class _StealthModeDrawerState extends State<StealthModeDrawer> {

  ValueNotifier<bool> isConnected = ValueNotifier<bool>(false);
  String connectionText = 'Desconectado';
  Color connectionColor = Colors.red;
  IconData connectionIcon = Icons.power_off_outlined;

  void checkConnectionStatus() {
    if (Provider.of<ConnectionStatusNotifier>(context, listen: false).isOnline) {
      isConnected.value = true;
      connectionText = 'Conectado';
      connectionColor = Colors.green;
      connectionIcon = Icons.power;
    } else {
      isConnected.value = false;
      connectionText = 'Desconectado';
      connectionColor = Colors.red;
      connectionIcon = Icons.power_off_outlined;
    }
  }

  @override
  Widget build(BuildContext context) {
    final dataTransmissionSocket = Provider.of<DataTransmissionSocketService>(context, listen: false);
    return Drawer(
      backgroundColor: const Color(0xFF1C1C1C),
      child: ListView(
        children: [
          Consumer2<ConnectionStatusNotifier, AlertStatusNotifier>(
            builder: (context, connectionStatus, alertStatus, child) {
              String connectionText;
              Color connectionColor;
              IconData connectionIcon;

              if (alertStatus.isAlertTriggered.value) {
                connectionText = 'Emitiendo ubicacion';
                connectionColor = Colors.orange;
                connectionIcon = Icons.map;
              } else {
                if (connectionStatus.isOnline) {
                  connectionText = 'Conectado';
                  connectionColor = Colors.green;
                  connectionIcon = Icons.power;
                } else {
                  connectionText = 'Desconectado';
                  connectionColor = Colors.red;
                  connectionIcon = Icons.power_off_outlined;
                }
              }

              return DrawerHeader(
                decoration: BoxDecoration(
                  color: connectionColor,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      connectionText,
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                        fontSize: MediaQuery.of(context).size.height * 0.025,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * 0.01,
                      ),
                      child: Icon(
                        connectionIcon,
                        color: Colors.white,
                        size: MediaQuery.of(context).size.height * 0.05,
                      ),
                    ),
                  ],
                ),
              );
            },
          ),
          BuildDrawerItem(
            title: 'Posicion',
            subtitle: 'Dejar de emitir posicion',
            icon: Icons.map,
            marginTop: MediaQuery.of(context).size.height * 0.05,
            onTap: Provider.of<AlertStatusNotifier>(context, listen: false).isAlertTriggered.value &&
                !Provider.of<AlertStatusNotifier>(context, listen: false).stopEventFired
                ? () {
              Provider.of<AlertStatusNotifier>(context, listen: false).stopSendingData();
              final userID = Provider.of<User>(context, listen: false).id;
              dataTransmissionSocket.endDanger(userID);
              final lobbySocketService = Provider.of<LobbySocketService>(context, listen: false);
              lobbySocketService.stopSendingLocation(userID.toString());
              lobbySocketService.onConnectToLobbySocket(Provider.of<User>(context, listen: false).id, navigatorKey.currentState!.context);
              Provider.of<AlertStatusNotifier>(context, listen: false).stopEventFired = true;
              Provider.of<ButtonLockNotifier>(context, listen: false).unlockButtons();
            }
                : () {},
          ),
          if (widget.showItem)
            BuildDrawerItem(
              title: 'Fondo de pantalla',
              subtitle: 'Eliminar fondo de pantalla',
              icon: Icons.photo,
              marginTop: MediaQuery.of(context).size.height * 0.02,
              onTap: widget.deleteBackground,
            ),
          if (widget.showItem)
            BuildDrawerItem(
              title: 'Estado',
              subtitle: 'Pasar a modo normal',
              icon: Icons.swap_horiz,
              marginTop: MediaQuery.of(context).size.height * 0.02,
              onTap: () => {
                widget.controller.animateToPage(
                  0,
                  duration: const Duration(milliseconds: 300),
                  curve: Curves.ease,
                ),
              },
            ),
        ],
      ),
    );
  }
}
