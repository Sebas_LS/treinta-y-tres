import 'package:treinta_y_tres/application/application_dependencies.dart';
import 'package:path_provider/path_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:path/path.dart';

class StealthModeBody extends StatefulWidget {
  final PageController controller;
  const StealthModeBody({Key? key, required this.controller}) : super(key: key);

  @override
  State<StealthModeBody> createState() => _StealthModeBodyState();
}

class _StealthModeBodyState extends State<StealthModeBody> {
  File? _image;
  bool _backgroundSet = false;

  @override
  void initState() {
    super.initState();
    loadBackgroundImage();
  }

  Future<void> loadBackgroundImage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? imagePath = prefs.getString('backgroundImagePath');

    if (imagePath != null) {
      setState(() {
        _image = File(imagePath);
        _backgroundSet = true;
      });
    }
  }

  Future<void> changeBackground() async {
    final pickedFile = await ImagePicker().getImage(source: ImageSource.gallery);

    if (pickedFile != null) {
      _image = File(pickedFile.path);

      final appDir = await getApplicationDocumentsDirectory();
      final fileName = basename(_image!.path);
      final savedImage = await _image!.copy('${appDir.path}/$fileName');

      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('backgroundImagePath', savedImage.path);

      setState(() {
        _backgroundSet = true;
      });
    } else {
      print('No image selected.');
    }
  }

  void resetBackground() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('backgroundImagePath');

    setState(() {
      _image = null;
      _backgroundSet = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        // MoveToBackground.moveTaskToBack();
        return false;
      },
      child: Scaffold(
        backgroundColor: _image == null ? const Color(0XFF1C1C1C) : null,
        body: Container(
          decoration: _image != null ? BoxDecoration(image: DecorationImage(image: FileImage(_image!), fit: BoxFit.cover)) : null,
          child: ListView(
            children: [
              if (!_backgroundSet) ... [
                BuildLobbyHeader(
                  controller: widget.controller,
                  stateText: 'Pasar a normal',
                  state: 0,
                  showDrawerIcon: true,
                ),
                const BuildLobbyHeaderDivider(),
                const BuildStealthInfo(),
                BuildChangeBackground(changeBackground: changeBackground),
              ],
            ],
          ),
        ),
        drawer: StealthModeDrawer(
          controller: widget.controller,
          showItem: _backgroundSet,
          deleteBackground: () => {
            resetBackground(),
            Navigator.pop(context),
          },
        ),
      ),
    );
  }
}