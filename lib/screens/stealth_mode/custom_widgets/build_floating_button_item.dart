import 'package:treinta_y_tres/application/application_dependencies.dart';

Widget buildFloatingButtonItem(Icon icon, BuildContext context, Function callback) {
  final buttonLock = Provider.of<ButtonLockNotifier>(context, listen: false);
  return ActionButton(
    tooltip: '',
    color: Theme.of(context).primaryColor,
    onPressed: () {
      if(!buttonLock.areButtonsLocked){
        buttonLock.lockButtons();
        callback();
      }
    },
    icon: IconTheme(
      data: const IconThemeData(
        color: Colors.black,
        size: 30,
      ),
      child: icon,
    ),
  );
}