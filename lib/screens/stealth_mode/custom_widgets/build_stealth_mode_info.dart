import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildStealthInfo extends StatefulWidget {
  const BuildStealthInfo({Key? key}) : super(key: key);

  @override
  State<BuildStealthInfo> createState() => _BuildStealthInfoState();
}

class _BuildStealthInfoState extends State<BuildStealthInfo> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        top: MediaQuery.of(context).size.width * 0.05,
        bottom: MediaQuery.of(context).size.width * 0.05,
        left: MediaQuery.of(context).size.width * 0.08,
        right: MediaQuery.of(context).size.width * 0.05,
      ),
      margin: EdgeInsets.only(
        top: MediaQuery.of(context).size.width * 0.1,
        left: MediaQuery.of(context).size.width * 0.05,
        right: MediaQuery.of(context).size.width * 0.05,
      ),
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage('assets/background/drawer_header_background.png'),
          fit: BoxFit.cover,
        ),
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(50),
          bottomRight: Radius.circular(50),
          topRight: Radius.circular(10),
          bottomLeft: Radius.circular(10),
        ),
      ),
      child: Text(
        'Estas en Modo Sigiloso. Para optimizar la discrecion, te recomendamos cambiar el fondo de pantalla a un diseño menos llamativo. De esta manera, podras utilizar la aplicacion de manera segura sin atraer atención no deseada. Recuerda, tu seguridad es nuestra prioridad',
        style: GoogleFonts.poppins(
          fontSize: MediaQuery.of(context).size.height * 0.015,
          fontWeight: FontWeight.bold,
          color: Colors.black,
        )
      ),
    );
  }
}
