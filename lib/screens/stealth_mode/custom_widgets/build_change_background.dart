import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildChangeBackground extends StatefulWidget {
  final Function changeBackground;
  const BuildChangeBackground({Key? key, required this.changeBackground}) : super(key: key);

  @override
  State<BuildChangeBackground> createState() => _BuildChangeBackgroundState();
}

class _BuildChangeBackgroundState extends State<BuildChangeBackground> {

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: MediaQuery.of(context).size.height * 0.07,
        left: MediaQuery.of(context).size.width * 0.05,
        right: MediaQuery.of(context).size.width * 0.05,
      ),
      child: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/background/drawer_header_background.png'),
            fit: BoxFit.cover,
          ),
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30),
            bottomRight: Radius.circular(30),
            bottomLeft: Radius.circular(5),
            topRight: Radius.circular(5),
          ),
        ),
        child: ListTile(
          title: Text(
            'Cambiar fondo de pantalla',
            style: GoogleFonts.poppins(
              fontSize: 13,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
          ),
          trailing: Container(
            margin: EdgeInsets.only(
              right: MediaQuery.of(context).size.width * 0.01,
            ),
            child: IconButton(
              onPressed: () => widget.changeBackground(),
              icon: Icon(
                Icons.image,
                color: Colors.black,
                size: MediaQuery.of(context).size.height * 0.04,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
