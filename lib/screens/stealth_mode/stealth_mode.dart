import 'package:geolocator/geolocator.dart';
import 'package:treinta_y_tres/application/application_dependencies.dart';
import 'package:background_location/background_location.dart';

class StealthMode extends StatefulWidget {
  final PageController controller;
  const StealthMode({Key? key, required this.controller}) : super(key: key);

  @override
  State<StealthMode> createState() => _StealthModeState();
}

class _StealthModeState extends State<StealthMode> {
  late IconDataNotifier iconDataNotifier;
  bool _isReadingNFC = false;

  @override
  void initState() {
    super.initState();
    iconDataNotifier = Provider.of<IconDataNotifier>(context, listen: false);
    loadData();
    _startNFCReading();
  }

  Future<void> _startLocationService() async {
    BackgroundLocation.setAndroidNotification(
      title: "Emitiendo posicion...",
      message: "Emicion en progreso...",
      icon: "@mipmap/ic_launcher",
    );
    BackgroundLocation.setAndroidConfiguration(1000);
    BackgroundLocation.startLocationService(distanceFilter: 10);
    BackgroundLocation.getLocationUpdates((location) {
      print(location);
    });
  }

  Future<void> _startNFCReading() async {
    if (!_isReadingNFC) {
      _isReadingNFC = true;
      try {
        NFCTag tag = await FlutterNfcKit.poll();
        if (tag != null) {
          handleSocketTransition(context, 'Tu mensaje deseado');
        }
      } catch (e) {
        print("Error al leer NFC: $e");
      } finally {
        _isReadingNFC = false;
      }
    }
  }

  Future<void> loadData() async {
    for (int i = 0; i < 4; i++) {
      int? iconIndex = await SharedPrefManager.getInt('iconIndex$i');
      if (iconIndex != null) {
        IconData iconData = iconDataNotifier.globalIcons[iconIndex];
        iconDataNotifier.updateIcon(i, iconData);
      }
    }
  }

  @override
  void dispose() {
    super.dispose();
    BackgroundLocation.stopLocationService();
  }

  @override
  Widget build(BuildContext context) {
    IconDataNotifier iconDataNotifier = Provider.of<IconDataNotifier>(context);
    return Scaffold(
      backgroundColor: const Color(0XFF1C1C1C),
      body: StealthModeBody(controller: widget.controller),
      floatingActionButton: BuildFloatingButton(
        initialOpen: false,
        distance: 112.0,
        children: [
          buildFloatingButtonItem(Icon(iconDataNotifier.messageIcons[3]), context, () => {handleSocketTransition(context, 'Estoy de camino a casa')}),
          buildFloatingButtonItem(Icon(iconDataNotifier.messageIcons[2]), context, () => {handleSocketTransition(context, 'Siganme, me siento inseguro')}),
          buildFloatingButtonItem(Icon(iconDataNotifier.messageIcons[1]), context, () => {handleSocketTransition(context, 'Creo que me estan siguiendo')}),
          buildFloatingButtonItem(Icon(iconDataNotifier.messageIcons[0]), context, () => {handleSocketTransition(context, 'Ayuda, estoy en peligro')}),
        ],
      ),
    );
  }

  void handleSocketTransition(BuildContext context, String messageToSend) async {
    final lobbySocketService = Provider.of<LobbySocketService>(context, listen: false);
    final dataTransmissionSocketService = Provider.of<DataTransmissionSocketService>(context, listen: false);
    final alertStatusNotifier = Provider.of<AlertStatusNotifier>(context, listen: false);

    Provider.of<AlertStatusNotifier>(context, listen: false).stopEventFired = false;
    Provider.of<AlertHistoryNotifier>(context, listen: false).addAlert(
      Alert(
        date: DateTime.now(),
        title: messageToSend,
      ),
    );

    if (lobbySocketService.isConnected() && !alertStatusNotifier.isAlertTriggered.value) {

      Provider.of<AlertHistoryNotifier>(context, listen: false).incrementSentAlerts();
      alertStatusNotifier.stopEventFired = false;

      Position currentPosition = await Geolocator.getCurrentPosition();

      lobbySocketService.onEmergencyButtonPress(
          Provider.of<User>(context, listen: false),
          messageToSend,
          currentPosition,
          navigatorKey.currentState!.context
      );

      final userID = Provider.of<User>(context, listen: false).id;

      dataTransmissionSocketService.onDataTransmissionSocketConnect(
          userID,
          navigatorKey.currentState!.context
      );

      _startLocationService();
      _startNFCReading();
    }
  }
}