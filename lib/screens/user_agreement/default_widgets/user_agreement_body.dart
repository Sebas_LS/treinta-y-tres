import 'package:treinta_y_tres/application/application_dependencies.dart';

class UserAgreementBody extends StatefulWidget {
  const UserAgreementBody({Key? key}) : super(key: key);

  @override
  State<UserAgreementBody> createState() => _UserAgreementBodyState();
}

class _UserAgreementBodyState extends State<UserAgreementBody> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0XFF1C1C1C),
      body: SingleChildScrollView(
        child: Column(
          children: [
            const BuildCustomPaintBackNavigation(title: 'Acuerdo de usuario', fontSize: 18),
            Container(
              width: double.infinity,
              height: MediaQuery.of(context).size.height * 0.8,
              decoration: const BoxDecoration(
                color: Color(0xFFF1F1F1),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(120),
                  bottomRight: Radius.circular(120),
                  topRight: Radius.circular(15),
                  bottomLeft: Radius.circular(15),
                ),
              ),
              child: Column(
                children: [
                  BuildUserAgreementItem(
                    title: 'Terminos y Condiciones',
                    subtitle: 'Leer terminos y condiciones',
                    topMargin: MediaQuery.of(context).size.height * 0.08,
                    callback: () => Navigator.pushNamed(context, '/terms_and_conditions'),
                  ),
                  BuildUserAgreementItem(
                    title: 'Politica de Privacidad',
                    subtitle: 'Definiciones y terminos clave',
                    topMargin: MediaQuery.of(context).size.height * 0.03,
                    callback: () => Navigator.pushNamed(context, '/policy_and_privacy'),
                  ),
                  BuildUserAgreementItem(
                    title: 'Descargo de Responsabilidad',
                    subtitle: 'Responsabilidad limitada',
                    topMargin: MediaQuery.of(context).size.height * 0.03,
                    callback: () => Navigator.pushNamed(context, '/disclaimer_and_terms'),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
