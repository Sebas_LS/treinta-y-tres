import 'package:treinta_y_tres/application/application_dependencies.dart';

class PolicyAndPrivacy extends StatefulWidget {
  const PolicyAndPrivacy({Key? key}) : super(key: key);

  @override
  State<PolicyAndPrivacy> createState() => _PolicyAndPrivacyState();
}

class _PolicyAndPrivacyState extends State<PolicyAndPrivacy> {

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: Color(0XFF1C1C1C),
      body: PolicyAndPrivacyBody(),
    );
  }
}