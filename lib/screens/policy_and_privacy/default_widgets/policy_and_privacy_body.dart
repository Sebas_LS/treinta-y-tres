import 'package:treinta_y_tres/application/application_dependencies.dart';
import 'package:treinta_y_tres/reusable_widgets/user_agreement_information.dart' as user_agreement_information;

class PolicyAndPrivacyBody extends StatefulWidget {
  const PolicyAndPrivacyBody({Key? key}) : super(key: key);

  @override
  State<PolicyAndPrivacyBody> createState() => _PolicyAndPrivacyBodyState();
}

class _PolicyAndPrivacyBodyState extends State<PolicyAndPrivacyBody> {

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        buildContent(context, 'Política de Privacidad', 0.11,
          children: [
            buildCustomText('Actualizado el 2022-08-12', 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.policyAndSecurityFirstPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Definiciones y Términos Clave', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.policyAndSecuritySecondPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('¿Qué información recopilamos?', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.policyAndSecurityThirdPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('¿Cómo usamos la información que recopilamos?', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.policyAndSecurityFourthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('¿Cuándo usa Treinta y Tres la información del usuario final de terceros?', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.policyAndSecurityFifthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('¿Cuándo usa Treinta y Tres la información del cliente de terceros?', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.policyAndSecuritySixthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('¿Compartimos la información que recopilamos con terceros? ', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.policyAndSecuritySeventhPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('¿Dónde y cuándo se recopila la información de los clientes y usuarios finales?', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.policyAndSecurityNinethPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('¿Cómo utilizamos su dirección de correo electrónico?', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.policyAndSecurityTenthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('¿Cuánto tiempo conservamos su información?', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.policyAndSecurityEleventhPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('¿Cómo protegemos su información?', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.policyAndSecurityTwelfthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('¿Podría transferirse mi información a otros países?', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.policyAndSecurityThirteenthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('¿La información recopilada a través del Servicio Treinta y Tres es segura?', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.policyAndSecurityFourteenthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('¿Puedo actualizar o corregir mi información?', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.policyAndSecurityFifteenthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Personal', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.policyAndSecuritySixteenthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Venta de Negocio', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.policyAndSecuritySeventeenthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Afiliados', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.policyAndSecurityEighteenthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Ley que Rige', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.policyAndSecurityNineteenthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Tu consentimiento', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.policyAndSecurityTwentiethPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Enlaces a otros Sitios Web', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.policyAndSecurityTwentyFirstPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Cookies', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.policyAndSecurityTwentySecondPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Bloquear y deshabilitar Cookies y tecnologías similares', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.policyAndSecurityTwentyThirdPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Detalles del Pago', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.policyAndSecurityTwentyFourthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Privacidad de los Niños', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.policyAndSecurityTwentyFifthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Cambios en nuestra Política de Privacidad', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.policyAndSecurityTwentySyxthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Servicios de terceros', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.policyAndSecurityTwentySeventhPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Tecnologías de Seguimiento', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.policyAndSecurityTwentyEighthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Información sobre el Reglamento general de protección de datos (RGPD)', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.policyAndSecurityTwentyNinethPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('¿Qué es RGPD?', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.policyAndSecurityThirtiethPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('¿Qué son los Datos Personales?', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.policyAndSecurityThirtyOnePart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('¿Por qué es importante el RGPD?', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.policyAndSecurityThirtyTwoPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Derechos individuales del interesado: acceso, portabilidad y eliminación de datos', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.policyAndSecurityThirtyThirdPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Residentes de California', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.policyAndSecurityThirtyFourthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Ley de protección de la privacidad en línea de California (CalOPPA)', 20, Alignment.centerLeft, 23, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.policyAndSecurityThirtyFifthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText('Contáctenos', 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
            buildCustomText(user_agreement_information.policyAndSecurityThirtySixthPart, 20, Alignment.centerLeft, 16, marginLeft: 0, marginRight: 0, horizontalPadding: 0, color: Colors.white),
          ],
        ),
      ],
    );
  }
}
