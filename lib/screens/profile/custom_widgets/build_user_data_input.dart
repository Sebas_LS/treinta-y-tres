import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildUserDataInput extends StatefulWidget {

  final String? inputData;
  final double? marginTop;
  final IconData? icon;

  const BuildUserDataInput({
    Key? key,
    required this.inputData,
    required this.marginTop,
    required this.icon,
  }) : super(key: key);

  @override
  State<BuildUserDataInput> createState() => _BuildUserDataInputState();
}

class _BuildUserDataInputState extends State<BuildUserDataInput> {

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: MediaQuery.of(context).size.width * 0.05,
        vertical: MediaQuery.of(context).size.height * 0.015,
      ),
      margin: EdgeInsets.only(
        top: widget.marginTop!,
        left: MediaQuery.of(context).size.width * 0.02,
        right: MediaQuery.of(context).size.width * 0.02,
      ),
      decoration: BoxDecoration(
        color: const Color(0xFFF1F1F1),
        borderRadius: BorderRadius.circular(30),
        boxShadow: const [
          BoxShadow(
            color: Colors.black26,
            blurRadius: 6,
            offset: Offset(0, 3),
          ),
        ],
      ),
      child: Row(
        children: [
           Icon(
            widget.icon,
            color: const Color(0XFF1C1C1C),
          ),
          const SizedBox(width: 10),
          Flexible(
            child: Text(
              widget.inputData!,
              overflow: TextOverflow.ellipsis,
              style: GoogleFonts.poppins(
                fontSize: 19,
                fontWeight: FontWeight.w300,
                color: const Color(0XFF1C1C1C),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
