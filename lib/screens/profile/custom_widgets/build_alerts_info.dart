import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildAlertsInfo extends StatefulWidget {
  final String? numberAlerts;
  final String? textAlerts;
  const BuildAlertsInfo({Key? key, required this.numberAlerts, required this.textAlerts}) : super(key: key);

  @override
  State<BuildAlertsInfo> createState() => _BuildAlertsInfoState();
}

class _BuildAlertsInfoState extends State<BuildAlertsInfo> {

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: EdgeInsets.only(
          left: MediaQuery.of(context).size.width * 0.03,
          right: MediaQuery.of(context).size.width * 0.03,
        ),
        child: SizedBox(
          height: MediaQuery.of(context).size.height * 0.12,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: const Color(0xFFF1F1F1),
              image: const DecorationImage(
                image: AssetImage('assets/background/alerts_records_background.jpg'),
                fit: BoxFit.cover,
              ),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  alignment: Alignment.center,
                  child: Text(
                    widget.numberAlerts!,
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                      color: const Color(0XFF1C1C1C),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01),
                  alignment: Alignment.center,
                  child: Text(
                    widget.textAlerts!,
                    textAlign: TextAlign.center,
                    style: GoogleFonts.poppins(
                      fontSize: 10,
                      fontWeight: FontWeight.bold,
                      color: const Color(0XFF1C1C1C),
                    ),
                  ),
                ),
                // Text('10'),
              ],
            ),
          ),
        ),
      ),
    );
  }
}