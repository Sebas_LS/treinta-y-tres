import 'package:treinta_y_tres/application/application_dependencies.dart';

import '../../../models/update_profile_picture.dart';

class BuildUserData extends StatefulWidget {
  const BuildUserData({Key? key}) : super(key: key);

  @override
  State<BuildUserData> createState() => _BuildUserDataState();
}

class _BuildUserDataState extends State<BuildUserData> {

  List<Item> data = [];
  String? keyWord;
  String? carModel;
  String? plaque;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (data.isEmpty) {
      data = generateItems(3, context);
    }

    final user = Provider.of<User>(context, listen: false);
    keyWord = user.keyword;
    carModel = user.carModel.toString();
    plaque = user.plaque;
  }

  @override
  Widget build(BuildContext context) {
    final registerData = Provider.of<RegisterNotifier>(context, listen: false).data;
    final user = Provider.of<User>(context, listen: false);
    return Container(
      margin: EdgeInsets.only(
        top: MediaQuery.of(context).size.height * 0.02,
        left: MediaQuery.of(context).size.width * 0.06,
        right: MediaQuery.of(context).size.width * 0.06,
      ),
      child: Column(
        children: [
          ExpansionPanelList(
            expandIconColor: Colors.white,
            expansionCallback: (int index, bool isExpanded) {
              setState(() {
                if (data[index].isExpanded == false) {
                  data[index].isExpanded = true;
                } else if (data[index].isExpanded == true) {
                  data[index].isExpanded = false;
                }
              });
            },
            children: data.map<ExpansionPanel>((Item item) {
              return ExpansionPanel(
                backgroundColor: const Color(0XFF1C1C1C),
                headerBuilder: (BuildContext context, bool isExpanded) {
                  return Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ListTile(
                      title: Text(
                        item.headerValue,
                        style: GoogleFonts.poppins(
                          fontSize: 15,
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  );
                },
                body: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: item.expandedValue,
                  ),
                ),
                isExpanded: item.isExpanded,
              );
            }).toList(),
          ),
          BuildUpdateProfileButton(
            onPressed: () {
              final userProfileNotifier = Provider.of<UserProfileNotifier>(context, listen: false);
              File? imageFile;

              String? imagePath = userProfileNotifier.userProfile.imagePath;
              if (imagePath != null && !imagePath.startsWith('http')) {
                imageFile = File(imagePath);
              }

              updateProfile(context, user.id, keyWord!, carModel!, plaque!, imageFile);
            },
          ),
        ],
      ),
    );
  }

  List<Item> generateItems(int numberOfItems, BuildContext context) {
    User user = Provider.of<User>(context, listen: false);
    return <Item>[
      Item(
        isExpanded: false,
        headerValue: 'Informacion personal',
        expandedValue: <Widget>[
          const BuildUserDataInputHeader(title: 'Nombre completo', marginTop: 0),
          BuildUserDataInput(
            inputData: user.name,
            marginTop: MediaQuery.of(context).size.height * 0.01,
            icon: Icons.person,
          ),
          BuildUserDataInputHeader(title: 'Nombre de usuario', marginTop: MediaQuery.of(context).size.height * 0.01),
          BuildUserDataInput(
            inputData: user.username,
            marginTop: MediaQuery.of(context).size.height * 0.01,
            icon: Icons.person,
          ),
          BuildUserDataInputHeader(title: 'Correo electronico', marginTop: MediaQuery.of(context).size.height * 0.01),
          BuildUserDataInput(
            inputData: user.email,
            marginTop: MediaQuery.of(context).size.height * 0.01,
            icon: Icons.email,
          ),
        ],
      ),
      Item(
        isExpanded: false,
        headerValue: 'Informacion del vehiculo',
        expandedValue: <Widget>[
          const BuildUserDataInputHeader(title: 'Modelo', marginTop: 0),
          BuildProfileActiveInput(
            marginTop: MediaQuery.of(context).size.height * 0.01,
            icon: Icons.car_rental,
            textData: user.carModel.toString(),
            onChanged: (value) {
              setState(() {
                carModel = value;
              });
            },
          ),
          BuildUserDataInputHeader(title: 'Placa', marginTop: MediaQuery.of(context).size.height * 0.01),
          BuildProfileActiveInput(
            marginTop: MediaQuery.of(context).size.height * 0.01,
            icon: Icons.car_rental,
            textData: user.plaque,
            onChanged: (value) {
              setState(() {
                plaque = value;
              });
            },
          ),
        ],
      ),
      Item(
        isExpanded: false,
        headerValue: 'Informacion adicional',
        expandedValue: <Widget>[
          const BuildUserDataInputHeader(title: 'Palabra clave', marginTop: 0),
          BuildProfileActiveInput(
            marginTop: MediaQuery.of(context).size.height * 0.01,
            icon: Icons.car_rental,
            textData: user.keyword,
            onChanged: (value) {
              setState(() {
                keyWord = value;
              });
            },
          ),
          BuildUserDataInputHeader(title: 'Tipo de conductor', marginTop: MediaQuery.of(context).size.height * 0.01),
          BuildUserDataInput(
            inputData: user.driverType.toString(),
            marginTop: MediaQuery.of(context).size.height * 0.01,
            icon: Icons.car_rental,
          ),
        ],
      ),
    ];
  }
}

class Item {
  Item({
    required this.expandedValue,
    required this.headerValue,
    this.isExpanded = false,
  });

  List<Widget> expandedValue;
  String headerValue;
  bool isExpanded;
}