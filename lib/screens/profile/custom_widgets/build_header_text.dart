import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildHeaderText extends StatefulWidget {
  const BuildHeaderText({Key? key}) : super(key: key);

  @override
  State<BuildHeaderText> createState() => _BuildHeaderTextState();
}

class _BuildHeaderTextState extends State<BuildHeaderText> {

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
      alignment: Alignment.center,
      width: double.infinity,
      child: Text(
        'Perfil',
        style: GoogleFonts.poppins(
          fontSize: 20,
          fontWeight: FontWeight.bold,
          color: Colors.white,
        ),
      ),
    );
  }
}
