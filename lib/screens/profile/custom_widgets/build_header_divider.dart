import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildHeaderDivider extends StatefulWidget {
  const BuildHeaderDivider({Key? key}) : super(key: key);

  @override
  State<BuildHeaderDivider> createState() => _BuildHeaderDividerState();
}

class _BuildHeaderDividerState extends State<BuildHeaderDivider> {

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        left: MediaQuery.of(context).size.width * 0.05,
        right: MediaQuery.of(context).size.width * 0.05,
        bottom: MediaQuery.of(context).size.height * 0.03,
      ),
      margin: EdgeInsets.only(
        top: MediaQuery.of(context).size.height * 0.01,
        left: MediaQuery.of(context).size.width * 0.1,
        right: MediaQuery.of(context).size.width * 0.1,
      ),
      child: const Divider(
        thickness: 1.5,
        color: Colors.white
      ),
    );
  }
}
