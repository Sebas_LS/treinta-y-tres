import 'package:treinta_y_tres/application/application_dependencies.dart';
import '../../../models/update_profile_picture.dart';

class BuildUserPhoto extends StatefulWidget {
  const BuildUserPhoto({Key? key}) : super(key: key);

  @override
  State<BuildUserPhoto> createState() => _BuildUserPhotoState();
}

class _BuildUserPhotoState extends State<BuildUserPhoto> {
  late String currentImagePath;

  @override
  void initState() {
    super.initState();
    _updateImagePath();
    User user = Provider.of<User>(context, listen: false);
    currentImagePath = user.image;
  }

  void _updateImagePath() {
    final userProfile = Provider.of<UserProfileNotifier>(context, listen: false).userProfile;
    setState(() {
      currentImagePath = userProfile.imagePath;
    });
  }

  Future<void> _selectAndSetImage() async {
    final String? imagePath = await pickImageToPhoto(ImageSource.gallery);

    if (imagePath != null) {
      Provider.of<UserProfileNotifier>(context, listen: false).updateImagePath(imagePath);
      _updateImagePath();
    }
  }

  Widget _buildImageWidget() {
    if (currentImagePath.isNotEmpty) {
      Uri uri = Uri.parse(currentImagePath);
      if (uri.scheme.contains('http')) {
        return Image.network(currentImagePath, fit: BoxFit.fill);
      } else {
        return Image.file(File(currentImagePath), fit: BoxFit.fill);
      }
    } else {
      return Image.asset('assets/images/user_logo.png', fit: BoxFit.fill);
    }
  }

  @override
  Widget build(BuildContext context) {
    User user = Provider.of<User>(context, listen: false);
    String currentImagePath = user.image;

    Widget imageWidget;
    if (currentImagePath.isNotEmpty) {
      Uri uri = Uri.parse(currentImagePath);
      if (uri.scheme.contains('http')) {
        imageWidget = Image.network(currentImagePath, fit: BoxFit.fill);
      } else {
        imageWidget = Image.file(File(currentImagePath), fit: BoxFit.fill);
      }
    } else {
      imageWidget = Image.asset('assets/images/user_logo.png', fit: BoxFit.fill);
    }

    return Container(
      margin: EdgeInsets.only(
        top: MediaQuery.of(context).size.height * 0.05,
      ),
      height: MediaQuery.of(context).size.height * 0.23,
      child: Stack(
        children: [
          Container(
            alignment: Alignment.topLeft,
            width: MediaQuery.of(context).size.width * 0.9,
            child: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: const Icon(
                Icons.arrow_circle_left,
                color: Colors.black87,
                size: 50,
              ),
            ),
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width * 0.9,
            child: GestureDetector(
              onTap: _selectAndSetImage,
              child: Container(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.015,
                  left: MediaQuery.of(context).size.width * 0.24,
                  right: MediaQuery.of(context).size.width * 0.24,
                  bottom: MediaQuery.of(context).size.height * 0.015,
                ),
                width: MediaQuery.of(context).size.width * 0.9,
                height: MediaQuery.of(context).size.height * 0.8,
                decoration: const BoxDecoration(
                  color: Color(0XFF1C1C1C),
                  shape: BoxShape.circle,
                ),
                child: ClipOval(child: _buildImageWidget()),
              ),
            ),
          ),
        ],
      ),
    );
  }
}