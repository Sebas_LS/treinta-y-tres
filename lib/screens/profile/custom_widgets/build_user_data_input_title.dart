import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildUserDataInputHeader extends StatefulWidget {
  final String? title;
  final double? marginTop;
  const BuildUserDataInputHeader({Key? key, required this.title, required this.marginTop}) : super(key: key);

  @override
  State<BuildUserDataInputHeader> createState() => _BuildUserDataInputHeaderState();
}

class _BuildUserDataInputHeaderState extends State<BuildUserDataInputHeader> {

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      padding: EdgeInsets.symmetric(
        horizontal: MediaQuery.of(context).size.width * 0.025,
        vertical: MediaQuery.of(context).size.height * 0.01,
      ),
      margin: EdgeInsets.only(
        top: widget.marginTop!,
        left: MediaQuery.of(context).size.width * 0.02,
        right: MediaQuery.of(context).size.width * 0.08,
      ),
      child: Text(
        widget.title!,
        textAlign: TextAlign.start,
        style: GoogleFonts.poppins(
          fontSize: 15,
          fontWeight: FontWeight.bold,
          color: Colors.white,
        ),
      ),
    );
  }
}
