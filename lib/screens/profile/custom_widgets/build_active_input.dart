import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildProfileActiveInput extends StatefulWidget {

  final double marginTop;
  final IconData? icon;
  final String? textData;
  final Function? onChanged;

  const BuildProfileActiveInput({
    Key? key,
    required this.marginTop,
    required this.icon,
    required this.textData,
    required this.onChanged,
  }) : super(key: key);

  @override
  State<BuildProfileActiveInput> createState() => _BuildProfileActiveInputState();
}

class _BuildProfileActiveInputState extends State<BuildProfileActiveInput> {

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).size.width * 0.05,
          vertical: MediaQuery.of(context).size.height * 0.005,
        ),
        margin: EdgeInsets.only(
          top: widget.marginTop!,
          left: MediaQuery.of(context).size.width * 0.02,
          right: MediaQuery.of(context).size.width * 0.02,
        ),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(30),
          boxShadow: const [
            BoxShadow(
              color: Colors.black26,
              blurRadius: 6,
              offset: Offset(0, 3),
            ),
          ],
        ),
        child: TextField(
          onChanged: widget.onChanged as void Function(String)?,
          style: const TextStyle(
            fontSize: 19,
            fontWeight: FontWeight.w300,
          ),
          decoration: InputDecoration(
            border: InputBorder.none,
            contentPadding: const EdgeInsets.only(top: 14.0),
            prefixIcon: Icon(
              widget.icon,
              color: Colors.black87,
            ),
            hintText: widget.textData,
            hintStyle: const TextStyle(
              fontSize: 19,
              fontWeight: FontWeight.w300,
            ),
          ),
        )
    );
  }
}
