import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildUpdateProfileButton extends StatefulWidget {
  final Function? onPressed;
  const BuildUpdateProfileButton({Key? key, required this.onPressed}) : super(key: key);

  @override
  State<BuildUpdateProfileButton> createState() => _BuildUpdateProfileButtonState();
}

class _BuildUpdateProfileButtonState extends State<BuildUpdateProfileButton> {

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(
        bottom: MediaQuery.of(context).size.height * 0.05,
      ),
      margin: EdgeInsets.only(
        top: MediaQuery.of(context).size.height * 0.06,
        left: MediaQuery.of(context).size.width * 0.15,
        right: MediaQuery.of(context).size.width * 0.15,
      ),
      width: double.infinity,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          elevation: 5.0,
          padding: const EdgeInsets.all(13.0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
          backgroundColor: const Color(0xFFF1F1F1),
        ),
        onPressed: widget.onPressed as void Function()?,
        child: Text(
          'Actualizar',
          textAlign: TextAlign.center,
          style: GoogleFonts.poppins(
            fontSize: 19,
            fontWeight: FontWeight.bold,
            color: const Color(0XFF1C1C1C),
          ),
        ),
      ),
    );
  }
}