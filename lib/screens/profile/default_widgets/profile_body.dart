import 'package:treinta_y_tres/application/application_dependencies.dart';

class ProfileBody extends StatefulWidget {
  const ProfileBody({Key? key}) : super(key: key);

  @override
  State<ProfileBody> createState() => _ProfileBodyState();
}

class _ProfileBodyState extends State<ProfileBody> {

  @override
  Widget build(BuildContext context) {
    final alertCountNotifier = Provider.of<AlertHistoryNotifier>(context, listen: false);
    return Scaffold(
      body: Container(
        color: const Color(0XFF1C1C1C),
        height: double.infinity,
        width: double.infinity,
        child: CustomPaint(
          painter: BuildCustomPaintAppbar(),
          child: Column(
            children: [
              const BuildUserPhoto(),
              const BuildHeaderText(),
              const BuildHeaderDivider(),
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.01,
                          left: MediaQuery.of(context).size.width * 0.02,
                          right: MediaQuery.of(context).size.width * 0.02,
                        ),
                        alignment: Alignment.center,
                        width: double.infinity,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            BuildAlertsInfo(numberAlerts: '${alertCountNotifier.sentAlerts}', textAlerts: 'A. enviadas'),
                            BuildAlertsInfo(numberAlerts: '${alertCountNotifier.receivedAlerts}', textAlerts: 'A. recibidas'),
                            BuildAlertsInfo(numberAlerts: '${alertCountNotifier.takenAlerts}', textAlerts: 'A. aceptadas'),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * 0.035,
                          left: MediaQuery.of(context).size.width * 0.07,
                          right: MediaQuery.of(context).size.width * 0.07,
                        ),
                        child: Row(
                          children: [
                            Text(
                              'Resetear datos de alertas',
                              style: GoogleFonts.poppins(
                                fontSize: 15,
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                              ),
                            ),
                            const Spacer(),
                            IconButton(
                              onPressed: () {
                                setState(() {
                                  final alertCountNotifier = Provider.of<AlertHistoryNotifier>(context, listen: false);
                                  alertCountNotifier.resetAlerts();
                                });
                              },
                              icon: Icon(
                                Icons.delete,
                                color: Colors.red,
                                size: MediaQuery.of(context).size.height * 0.04,
                              ),
                            ),
                          ],
                        ),
                      ),
                      const BuildUserData(),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}