import 'package:treinta_y_tres/application/application_dependencies.dart';

class HelpRequestDataScreen extends StatefulWidget {
  const HelpRequestDataScreen({Key? key}) : super(key: key);

  @override
  State<HelpRequestDataScreen> createState() => _HelpRequestDataScreenState();
}

class _HelpRequestDataScreenState extends State<HelpRequestDataScreen> {

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: Color(0XFF1C1C1C),
      body: BuildHelpRequestDataScreenBody(),
    );
  }
}
