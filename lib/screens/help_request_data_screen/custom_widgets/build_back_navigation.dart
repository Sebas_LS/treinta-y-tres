import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildHelpRequestDataBackNavigation extends StatefulWidget {
  const BuildHelpRequestDataBackNavigation({Key? key}) : super(key: key);

  @override
  State<BuildHelpRequestDataBackNavigation> createState() => _BuildHelpRequestDataBackNavigationState();
}

class _BuildHelpRequestDataBackNavigationState extends State<BuildHelpRequestDataBackNavigation> {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      width: MediaQuery.of(context).size.width * 0.9,
      child: IconButton(
        onPressed: () {
          Navigator.pop(context);
        },
        icon: const Icon(
          Icons.arrow_circle_left,
          color: Color(0XFF1C1C1C),
          size: 35,
        ),
      ),
    );
  }
}
