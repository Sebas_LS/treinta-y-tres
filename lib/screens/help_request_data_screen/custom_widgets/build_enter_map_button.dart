import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildEnterMapButton extends StatefulWidget {
  const BuildEnterMapButton({Key? key}) : super(key: key);

  @override
  State<BuildEnterMapButton> createState() => _BuildEnterMapButtonState();
}

class _BuildEnterMapButtonState extends State<BuildEnterMapButton> {

  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: MediaQuery.of(context).size.height * 0.02,
        left: MediaQuery.of(context).size.width * 0.07,
        right: MediaQuery.of(context).size.width * 0.07,
        bottom: MediaQuery.of(context).size.height * 0.02,
      ),
      width: double.infinity,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          backgroundColor: Colors.white,
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(10),
              bottomRight: Radius.circular(20),
              bottomLeft: Radius.circular(20),
              topRight: Radius.circular(20),
            ),
          ),
          padding: EdgeInsets.symmetric(
            vertical: MediaQuery.of(context).size.height * 0.015,
          ),
        ),
        onPressed: () async {

          if (_isLoading) return;

          setState(() {
            _isLoading = true;
          });

          Provider.of<AlertHistoryNotifier>(context, listen: false).incrementTakenAlerts();
          LobbySocketService lobbySocketService = Provider.of<LobbySocketService>(context, listen: false);
          DataTransmissionSocketService dataTransmissionSocketService = Provider.of<DataTransmissionSocketService>(context, listen: false);

          final helperID = Provider.of<User>(context, listen: false).id;
          final userInDangerId = Provider.of<UserInDangerNotifier>(context, listen: false).userInDangerId;

          bool canProceed = true;

          if (userInDangerId != null) {
            canProceed = await lobbySocketService.offerHelp(helperID.toString(), userInDangerId);
          } else {
            print('No user in danger ID found');
          }

          setState(() {
            _isLoading = false;
          });

          if (canProceed) {
            dataTransmissionSocketService.onDataTransmissionSocketConnect(int.parse('$userInDangerId'), navigatorKey.currentState!.context);
            dataTransmissionSocketService.emitHelperOnTheWay(helperID.toString(), userInDangerId.toString());
            Navigator.pushNamed(context, '/google_map');
          } else {
            showDialog(
              barrierDismissible: false,
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  title: Text(
                    'Sala Llena',
                    style: GoogleFonts.poppins(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      color: Colors.black87,
                    ),
                  ),
                  content: Text(
                    'La sala para ayudar al usuario en peligro ha alcanzado su límite de participantes. Mantente alerta para futuras alertas',
                    style: GoogleFonts.poppins(
                      fontSize: 15,
                      fontWeight: FontWeight.w500,
                      color: Colors.black,
                    ),
                  ),
                  actions: <Widget>[
                    Container(
                      margin: EdgeInsets.only(right: MediaQuery.of(context).size.width * 0.03),
                      child: TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                          Navigator.pushNamedAndRemoveUntil(navigatorKey.currentState!.context, '/lobby', (route) => false);
                        },
                        child: Text(
                          'Aceptar',
                          style: GoogleFonts.poppins(
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ),
                  ],
                );
              },
            );
          }
        },
        child: _isLoading
            ? const SizedBox(
            width: 20,
            height: 20,
            child: CircularProgressIndicator()
        )
            : Text(
          'Aceptar peticion',
          style: GoogleFonts.poppins(
            fontSize: 14.0,
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),
        ),
      ),
    );
  }
}
