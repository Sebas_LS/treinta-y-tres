import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildHelpRequestHeader extends StatefulWidget {

  final String? username;
  final String? imageURL;

  const BuildHelpRequestHeader({
    Key? key,
    required this.username,
    this.imageURL,
  }) : super(key: key);

  @override
  State<BuildHelpRequestHeader> createState() => _BuildHelpRequestHeaderState();
}

class _BuildHelpRequestHeaderState extends State<BuildHelpRequestHeader> {

  @override
  Widget build(BuildContext context) {
    final imagePath = widget.imageURL;
    return Container(
      width: MediaQuery.of(context).size.width * 0.9,
      alignment: Alignment.center,
      margin: EdgeInsets.only(
        top: MediaQuery.of(context).size.height * 0.01,
      ),
      child: Column(
        children: [
          CircleAvatar(
            backgroundColor: const Color(0XFF1C1C1C),
            radius: 60,
            child: Container(
              height: imagePath != ''
                ? MediaQuery.of(context).size.height * 0.21
                : MediaQuery.of(context).size.height * 0.1,
              width: imagePath != ''
                ? MediaQuery.of(context).size.width * 0.31
                : MediaQuery.of(context).size.width * 0.2,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                  image: imagePath!.isNotEmpty
                      ? NetworkImage(imagePath)
                      : const AssetImage('assets/images/user_logo.png') as ImageProvider<Object>,
                  fit: BoxFit.fill,
                ),
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              top: MediaQuery.of(context).size.height * 0.015,
            ),
            child: Text(
              widget.username!,
              style: GoogleFonts.poppins(
                fontSize: 17,
                fontWeight: FontWeight.bold,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
