import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildHelpRequestItem extends StatefulWidget {

  final String? firstTitle;
  final bool showSecondTitle;
  final String? firstData;
  final String? secondData;
  final bool isLoading;

  const BuildHelpRequestItem({
    Key? key,
    required this.firstTitle,
    required this.showSecondTitle,
    required this.firstData,
    this.secondData,
    this.isLoading = false,
  }) : super(key: key);

  @override
  State<BuildHelpRequestItem> createState() => _BuildHelpRequestItemState();
}

class _BuildHelpRequestItemState extends State<BuildHelpRequestItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: MediaQuery.of(context).size.height * 0.03,
        left: MediaQuery.of(context).size.width * 0.07,
        right: MediaQuery.of(context).size.width * 0.07,
      ),
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage(
            'assets/background/drawer_header_background.png',
          ),
          fit: BoxFit.cover,
        ),
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30),
          bottomRight: Radius.circular(70),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              widget.firstTitle!,
              style: GoogleFonts.poppins(
                fontSize: 15.0,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                top: MediaQuery.of(context).size.height * 0.01,
              ),
              padding: EdgeInsets.only(
                right: MediaQuery.of(context).size.height * 0.05,
              ),
              child: Text(
                widget.isLoading
                    ? 'Calculando la distancia...'
                    : widget.firstData!,
                style: GoogleFonts.poppins(
                  fontSize: 14.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.black,
                ),
              ),
            ),
            if (widget.showSecondTitle)
              Container(
                margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.01,
                ),
                child: Text(
                  widget.secondData!,
                  style: GoogleFonts.poppins(
                    fontSize: 14.0,
                    fontWeight: FontWeight.w500,
                    color: Colors.black,
                  ),
                ),
              ),
          ],
        ),
      ),
    );
  }
}
