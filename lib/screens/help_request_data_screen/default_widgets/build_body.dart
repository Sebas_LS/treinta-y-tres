import 'package:geolocator/geolocator.dart';
import 'package:treinta_y_tres/application/application_dependencies.dart';
import 'package:http/http.dart' as http;

class BuildHelpRequestDataScreenBody extends StatefulWidget {
  const BuildHelpRequestDataScreenBody({Key? key}) : super(key: key);

  @override
  State<BuildHelpRequestDataScreenBody> createState() => BuildHelpRequestDataScreenBodyState();
}

class BuildHelpRequestDataScreenBodyState extends State<BuildHelpRequestDataScreenBody> {

  String routeDistance = '';
  LatLng? myPosition;
  bool isDistanceLoading = true;

  @override
  void initState() {
    super.initState();
    _getCurrentPosition().then((position) {
      setState(() {
        myPosition = LatLng(position.latitude, position.longitude);
      });
    });
  }

  Future<Position> _getCurrentPosition() async {
    LocationPermission permission;
    bool serviceEnabled = await Geolocator.isLocationServiceEnabled();

    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();

    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      return Future.error('Location permissions are permanently denied');
    }

    Position position = await Geolocator.getCurrentPosition();
    final userInDangerData = Provider.of<UserInDangerData>(context, listen: false);

    getUserInDangerDistance(LatLng(position.latitude, position.longitude), LatLng(userInDangerData.latitude, userInDangerData.longitude));

    return position;
  }

  void getUserInDangerDistance(LatLng position1, LatLng position2) async {
    String url = "https://maps.googleapis.com/maps/api/directions/json?origin=${position1.latitude},${position1.longitude}&destination=${position2.latitude},${position2.longitude}&key=AIzaSyCGTuc8dbPTfTe5zTdQ58pnvYEonb0RE6I";

    try {
      http.Response response = await http.get(Uri.parse(url));
      if (response.statusCode == 200) {
        Map values = jsonDecode(response.body);

        if (values["routes"].isNotEmpty && values["routes"][0]["legs"].isNotEmpty) {
          values["routes"][0]["legs"][0]["steps"].forEach((leg) {
            var distance = values["routes"][0]["legs"][0]["distance"]["text"];
            routeDistance = distance;
          });
        } else {
          print("No se encontró una ruta entre los dos puntos.");
        }
      } else {
        print('Error en la respuesta de la API: ${response.statusCode}');
      }
    } catch (e) {
      print('Error en la solicitud: $e');
    }

    setState(() {
      isDistanceLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    final userInDangerData = Provider.of<UserInDangerData>(context, listen: false);
    return SizedBox(
      height: double.infinity,
      width: double.infinity,
      child: Stack(
        children: <Widget>[
          CustomPaint(
            painter: BuildCustomPaintAppbar(),
            size: const Size(double.infinity, double.infinity),
          ),
          Column(
            children: [
              Container(
                margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.05,
                  left: MediaQuery.of(context).size.width * 0.05,
                  right: MediaQuery.of(context).size.width * 0.02,
                ),
                alignment: Alignment.center,
                child: Stack(
                  children: [
                    const BuildHelpRequestDataBackNavigation(),
                    BuildHelpRequestHeader(
                      username: userInDangerData.name,
                      imageURL: userInDangerData.imageUrl,
                    ),
                  ],
                ),
              ),
              Container(padding: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.01)),
              Expanded(
                child: ListView(
                  children: [
                    BuildHelpRequestItem(
                      firstTitle: 'Mensaje enviado',
                      firstData: userInDangerData.message,
                      showSecondTitle: false,
                    ),
                    BuildHelpRequestItem(
                      firstTitle: 'Informacion del vehiculo',
                      firstData: 'Modelo: ${userInDangerData.carModel}',
                      secondData: 'Placa: ${userInDangerData.plaque}',
                      showSecondTitle: true,
                    ),
                    BuildHelpRequestItem(
                      firstTitle: 'Palabra clave',
                      firstData: userInDangerData.keyword,
                      showSecondTitle: false,
                    ),
                    BuildHelpRequestItem(
                      firstTitle: 'Distancia',
                      firstData: isDistanceLoading
                          ? null
                          : 'El usuario en peligro se encuentra a $routeDistance de tu ubicacion actual',
                      showSecondTitle: false,
                      isLoading: isDistanceLoading,
                    ),
                  ],
                ),
              ),
              const BuildEnterMapButton(),
            ],
          ),
        ],
      ),
    );
  }
}