import 'package:treinta_y_tres/application/application_dependencies.dart';

class GoogleMapScreen extends StatefulWidget {
  const GoogleMapScreen({Key? key}) : super(key: key);

  @override
  State<GoogleMapScreen> createState() => _GoogleMapScreenState();
}

class _GoogleMapScreenState extends State<GoogleMapScreen> {

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: BuildGoogleMapScreenBody(),
    );
  }
}