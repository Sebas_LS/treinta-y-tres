import 'package:location/location.dart';
import 'package:treinta_y_tres/application/application_dependencies.dart';
import 'package:geolocator/geolocator.dart';
import 'dart:math' as Math;
import 'dart:ui' as ui;
import 'package:http/http.dart' as http;

class BuildGoogleMapScreenBody extends StatefulWidget {
  const BuildGoogleMapScreenBody({Key? key}) : super(key: key);

  @override
  State<BuildGoogleMapScreenBody> createState() => BuildGoogleMapScreenBodyState();
}

class BuildGoogleMapScreenBodyState extends State<BuildGoogleMapScreenBody> {

  GoogleMapController? mapController;
  LatLng? myPosition;
  Set<Marker> markers = {};
  Set<Polyline> polylines = {};
  String routeDistance = '';
  String routeDuration = '';
  Location location = Location();
  DateTime? lastInteractionTime;
  bool isPaused = false;
  Timer? interactionTimer;
  StreamSubscription? _locationSubscription;

  @override
  void initState() {
    super.initState();
    _getCurrentPosition();
    _locationSubscription = location.onLocationChanged.listen((LocationData currentLocation) {
      if (mounted) {
        setState(() {
          myPosition = LatLng(currentLocation.latitude!, currentLocation.longitude!);
          updateMap();
        });
      }
    });
  }

  @override
  void dispose() {
    _locationSubscription?.cancel();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    updateMap();
  }

  void updateMap() {
    final userInDangerData = Provider.of<UserInDangerData>(context, listen: false);
    LatLng userInDangerPosition = LatLng(userInDangerData.latitude, userInDangerData.longitude);

    if (myPosition != null) {
      createMyMarker(Position(
          latitude: myPosition!.latitude,
          longitude: myPosition!.longitude,
          accuracy: 0.0,
          altitude: 0.0,
          heading: 0.0,
          speed: 0.0,
          speedAccuracy: 0.0,
          timestamp: DateTime.now(),
          altitudeAccuracy: 0.0,
          headingAccuracy: 0.0,
      )).then((marker) {
        setState(() {
          markers.removeWhere((m) => m.markerId == const MarkerId('myMarker'));
          markers.add(marker);
        });
      });

      createUserInDangerMarker(userInDangerData).then((marker) {
        setState(() {
          markers.removeWhere((m) => m.markerId == const MarkerId('userInDangerMarker'));
          markers.add(marker);
        });
      });

      _addPolyline(myPosition!, userInDangerPosition);

      if (!isPaused) {
        LatLngBounds bounds = LatLngBounds(
          southwest: LatLng(
            Math.min(myPosition!.latitude, userInDangerPosition.latitude),
            Math.min(myPosition!.longitude, userInDangerPosition.longitude),
          ),
          northeast: LatLng(
            Math.max(myPosition!.latitude, userInDangerPosition.latitude),
            Math.max(myPosition!.longitude, userInDangerPosition.longitude),
          ),
        );

        CameraUpdate cameraUpdate = CameraUpdate.newLatLngBounds(bounds, 50);
        mapController?.animateCamera(cameraUpdate);
      }
    }
  }

  void _addPolyline(LatLng position1, LatLng position2) async {
    String url = "https://maps.googleapis.com/maps/api/directions/json?origin=${position1.latitude},${position1.longitude}&destination=${position2.latitude},${position2.longitude}&key=AIzaSyCGTuc8dbPTfTe5zTdQ58pnvYEonb0RE6I";

    http.Response response = await http.get(Uri.parse(url));
    Map values = jsonDecode(response.body);

    if (values["routes"].isNotEmpty && values["routes"][0]["legs"].isNotEmpty) {
      List<LatLng> polylineCoordinates = [];
      values["routes"][0]["legs"][0]["steps"].forEach((leg) {
        String polylinePoints = leg["polyline"]["points"];
        List decodedPolylinePoints = decodePolyline(polylinePoints);

        var distance = values["routes"][0]["legs"][0]["distance"]["text"];
        var duration = values["routes"][0]["legs"][0]["duration"]["text"];

        setState(() {
          routeDistance = distance;
          routeDuration = duration;
        });

        for (int i = 0; i < decodedPolylinePoints.length; i += 2) {
          polylineCoordinates.add(LatLng(decodedPolylinePoints[i], decodedPolylinePoints[i + 1]));
        }
      });

      Polyline polyline = Polyline(
        width: 3,
        polylineId: const PolylineId('route1'),
        visible: true,
        points: polylineCoordinates,
        color: Colors.white,
      );

      setState(() {
        polylines.clear();
        polylines.add(polyline);
      });
    } else {
      print("No se encontró una ruta entre los dos puntos.");
    }
  }

  List decodePolyline(String polyline) {
    var list = polyline.codeUnits;
    var lList = [];
    int index = 0;
    int len = polyline.length;
    int c = 0;

    do {
      var shift = 0;
      int result = 0;

      do {
        c = list[index] - 63;
        result |= (c & 0x1F) << (shift * 5);
        index++;
        shift++;
      } while (c >= 32);

      if (result & 1 == 1) {
        result = ~result;
      }
      var result1 = (result >> 1) * 0.00001;
      lList.add(result1);
    } while (index < len);

    for (var i = 2; i < lList.length; i++) lList[i] += lList[i - 2];

    return lList;
  }

  Future<Position> _getCurrentPosition() async {
    LocationPermission permission;
    bool serviceEnabled = await Geolocator.isLocationServiceEnabled();

    if (!serviceEnabled) {
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();

    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.denied) {
        return Future.error('Location permissions are denied');
      }
    }

    if (permission == LocationPermission.deniedForever) {
      return Future.error('Location permissions are permanently denied');
    }

    Position position = await Geolocator.getCurrentPosition();

    createMyMarker(position).then((marker) {
      setState(() {
        markers.add(marker);
        final userInDangerData = Provider.of<UserInDangerData>(context, listen: false);
        _addPolyline(myPosition!, LatLng(userInDangerData.latitude, userInDangerData.longitude));
      });
    });

    return position;
  }

  Future<Position> getUserInDangerPosition() async {
    final userInDangerData = Provider.of<UserInDangerData>(context, listen: false);

    double userInDangerLatitude = userInDangerData.latitude;
    double userInDangerLongitude = userInDangerData.longitude;

    return Position(
      latitude: userInDangerLatitude,
      longitude: userInDangerLongitude,
      accuracy: 0.0,
      altitude: 0.0,
      heading: 0.0,
      speed: 0.0,
      speedAccuracy: 0.0,
      timestamp: DateTime.now(),
      altitudeAccuracy: 0.0,
      headingAccuracy: 0.0,
    );
  }

  void _onMapCreated(GoogleMapController controller) {
    mapController = controller;
    mapController!.setMapStyle('[{"elementType":"geometry","stylers":[{"color":"#212121"}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"elementType":"labels.text.stroke","stylers":[{"color":"#212121"}]},{"featureType":"administrative","elementType":"geometry","stylers":[{"color":"#757575"}]},{"featureType":"administrative.country","elementType":"labels.text.fill","stylers":[{"color":"#9e9e9e"}]},{"featureType":"administrative.land_parcel","stylers":[{"visibility":"off"}]},{"featureType":"administrative.locality","elementType":"labels.text.fill","stylers":[{"color":"#bdbdbd"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#181818"}]},{"featureType":"poi.park","elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"featureType":"poi.park","elementType":"labels.text.stroke","stylers":[{"color":"#1b1b1b"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"color":"#2c2c2c"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#8a8a8a"}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#373737"}]},{"featureType":"road.highway","elementType":"geometry","stylers":[{"color":"#3c3c3c"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry","stylers":[{"color":"#4e4e4e"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"color":"#616161"}]},{"featureType":"transit","elementType":"labels.text.fill","stylers":[{"color":"#757575"}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#3d3d3d"}]}]');
  }

  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(), targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    ByteData? byteData = await fi.image.toByteData(format: ui.ImageByteFormat.png);
    return byteData != null ? byteData.buffer.asUint8List() : Uint8List(0);
  }

  Future<Marker> createMyMarker(Position myPosition) async {
    final Uint8List markerIcon = await getBytesFromAsset('assets/images/map_personal_user_icon.png', 55);

    return Marker(
      markerId: const MarkerId('myMarker'),
      position: LatLng(myPosition.latitude, myPosition.longitude),
      icon: BitmapDescriptor.fromBytes(markerIcon),
    );
  }

  Future<BitmapDescriptor> getUserInDangerIcon() async {
    return await BitmapDescriptor.fromAssetImage(
      const ImageConfiguration(devicePixelRatio: 2.5),
      'assets/images/map_user_in_danger_icon.png',
    );
  }

  Future<Marker> createUserInDangerMarker(UserInDangerData userInDangerData) async {
    final icon = await getUserInDangerIcon();
    return Marker(
      markerId: const MarkerId('userInDangerMarker'),
      position: LatLng(userInDangerData.latitude, userInDangerData.longitude),
      icon: icon,
    );
  }

  LatLng calculateMidPoint(LatLng point1, LatLng point2) {
    return LatLng(
      (point1.latitude + point2.latitude) / 2,
      (point1.longitude + point2.longitude) / 2,
    );
  }

  double calculateZoomLevel(double radius) {
    double scale = radius / 600;
    return (16 - Math.log(scale) / Math.log(2));
  }

  double calculateDistance(LatLng point1, LatLng point2) {
    return Geolocator.distanceBetween(
        point1.latitude,
        point1.longitude,
        point2.latitude,
        point2.longitude
    );
  }

  void checkResumeTracking() {
    interactionTimer?.cancel();
    interactionTimer = Timer(const Duration(seconds: 8), () {
      setState(() {
        isPaused = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<UserInDangerData>(
      builder: (context, userInDangerData, child) {
        if (myPosition == null) {
          return Container(
            height: double.infinity,
            width: double.infinity,
            color: const Color(0XFF1C1C1C),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('Cargando mapa...',
                    style: GoogleFonts.poppins(
                      fontSize: MediaQuery.of(context).size.height * 0.02,
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                    )
                  ),
                  Container(
                    margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.05),
                    child: const CircularProgressIndicator(
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
          );
        }
        LatLng midPoint = calculateMidPoint(myPosition!, LatLng(userInDangerData.latitude, userInDangerData.longitude));
        double distance = calculateDistance(myPosition!, LatLng(userInDangerData.latitude, userInDangerData.longitude));
        double zoomLevel = calculateZoomLevel(distance);
        return Scaffold(
          body: Stack(
            children: [
              GoogleMap(
                polylines: polylines,
                onMapCreated: _onMapCreated,
                initialCameraPosition: CameraPosition(
                  target: midPoint,
                  zoom: zoomLevel,
                ),
                markers: markers,
                onCameraMoveStarted: () {
                  lastInteractionTime = DateTime.now();
                  isPaused = true;
                  checkResumeTracking();
                },
              ),
              Row(
                children: [
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.5,
                    height: MediaQuery.of(context).size.height,
                    child: const Align(
                      alignment: Alignment.topLeft,
                      child: BuildExitToTheMapButton(),
                    ),
                  ),
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.5,
                    height: MediaQuery.of(context).size.height,
                    child: Align(
                      alignment: Alignment.topRight,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          GestureDetector(
                            onTap: () => userDataDialog(context, userInDangerData),
                            child: Container(
                              margin: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.14,
                                right: MediaQuery.of(context).size.height * 0.02,
                              ),
                              child: const CircleAvatar(
                                backgroundColor: Colors.white,
                                radius: 25,
                                child: Icon(
                                  Icons.person,
                                  color: Colors.black,
                                  size: 35,
                                ),
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () async {
                              Position currentPosition = await _getCurrentPosition();
                              Position userInDangerPosition = await getUserInDangerPosition();
                              double intermediateLatitude = (currentPosition.latitude + userInDangerPosition.latitude) / 2;
                              double intermediateLongitude = (currentPosition.longitude + userInDangerPosition.longitude) / 2;
                              mapController?.animateCamera(
                                CameraUpdate.newCameraPosition(
                                  CameraPosition(
                                    target: LatLng(intermediateLatitude, intermediateLongitude),
                                    zoom: zoomLevel,
                                  ),
                                ),
                              );
                            },
                            child: Container(
                              margin: EdgeInsets.only(
                                top: MediaQuery.of(context).size.height * 0.05,
                                right: MediaQuery.of(context).size.height * 0.02,
                              ),
                              child: const CircleAvatar(
                                backgroundColor: Colors.white,
                                radius: 25,
                                child: Icon(
                                  Icons.my_location,
                                  color: Colors.black,
                                  size: 35,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        );
      },
    );
  }
}