import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildExitToTheMapButton extends StatefulWidget {
  const BuildExitToTheMapButton({Key? key}) : super(key: key);

  @override
  State<BuildExitToTheMapButton> createState() => _BuildExitToTheMapButtonState();
}

class _BuildExitToTheMapButtonState extends State<BuildExitToTheMapButton> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: MediaQuery.of(context).size.height * 0.12,
        left: MediaQuery.of(context).size.height * 0.01,
      ),
      child: IconButton(
        onPressed: () {
          showCustomDialog(
            context,
            '¿Estas seguro que quieres cancelar tu asistencia?',
            'Si',
            () {
              final dataTransmissionSocketService = Provider.of<DataTransmissionSocketService>(context, listen: false);
              final userInDangerId = Provider.of<UserInDangerNotifier>(context, listen: false).userInDangerId;
              final roomID = Provider.of<UserInDangerNotifier>(context, listen: false).userInDangerId;
              dataTransmissionSocketService.helperLeft(roomID.toString(), userInDangerId.toString());
              dataTransmissionSocketService.onDisconnectToDataTransmissionSocket();
              final lobbySocketService = Provider.of<LobbySocketService>(context, listen: false);
              lobbySocketService.onConnectToLobbySocket(Provider.of<User>(context, listen: false).id, navigatorKey.currentState!.context);
              Navigator.pushNamedAndRemoveUntil(context, '/lobby', (route) => false);
            },
            cancelCallback: () {
              Navigator.pop(context);
            },
            showSecondButton: true,
            secondButtonTitle: 'No'
          );
        },
        icon: const Icon(
          Icons.arrow_circle_left,
          color: Colors.white,
          size: 57,
        ),
      ),
    );
  }
}
