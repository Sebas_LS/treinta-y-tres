import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildStatsInfo extends StatefulWidget {
  final String? message;
  const BuildStatsInfo({Key? key, required this.message}) : super(key: key);

  @override
  State<BuildStatsInfo> createState() => _BuildStatsInfoState();
}

class _BuildStatsInfoState extends State<BuildStatsInfo> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.02),
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          bottomRight: Radius.circular(20),
          topLeft: Radius.circular(20),
          topRight: Radius.circular(5),
          bottomLeft: Radius.circular(5),
        ),
      ),
      padding: EdgeInsets.only(
        top: MediaQuery.of(context).size.height * 0.008,
        bottom: MediaQuery.of(context).size.height * 0.008,
        left: MediaQuery.of(context).size.height * 0.02,
        right: MediaQuery.of(context).size.height * 0.02,
      ),
      width: MediaQuery.of(context).size.width * 0.75,
      child: Text(
        widget.message!,
        textAlign: TextAlign.start,
        style: GoogleFonts.montserrat(
          fontSize: 15,
          fontWeight: FontWeight.bold,
          color: Colors.black,
        ),
      ),
    );
  }
}
