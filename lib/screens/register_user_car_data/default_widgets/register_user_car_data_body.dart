import 'package:treinta_y_tres/application/application_dependencies.dart';

class RegisterUserCarDataBody extends StatefulWidget {
  const RegisterUserCarDataBody({Key? key}) : super(key: key);

  @override
  State<RegisterUserCarDataBody> createState() => _RegisterUserCarDataBodyState();
}

class _RegisterUserCarDataBodyState extends State<RegisterUserCarDataBody> {

  bool isCarModelValid = false;
  bool isPlaqueValid = false;

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          const BuildCustomPaintBackNavigation(title: 'Registrarse', fontSize: 25),
          Container(
            width: double.infinity,
            height: MediaQuery.of(context).size.height * 0.8,
            decoration: const BoxDecoration(
              color: Color(0xFFF1F1F1),
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(120),
                bottomRight: Radius.circular(120),
                topRight: Radius.circular(15),
                bottomLeft: Radius.circular(15),
              ),
            ),
            child: Column(
              children: [
                const BuildRegisterHeaderText(
                  title: 'Datos del vehiculo',
                  subtitle: 'Registro 2 / 3',
                ),
                BuildActiveInput(
                  marginTop: MediaQuery.of(context).size.height * 0.03,
                  hintText: 'Modelo',
                  icon: Icons.directions_car,
                  onChanged: (value) {
                    setState(() {
                      isCarModelValid = value.isNotEmpty;
                      Provider.of<RegisterNotifier>(context, listen: false).updateCarModel(value);
                    });
                  },
                ),
                BuildActiveInput(
                  marginTop: MediaQuery.of(context).size.height * 0.03,
                  hintText: 'Placa',
                  icon: Icons.directions_car,
                  onChanged: (value) {
                    setState(() {
                      isPlaqueValid = value.isNotEmpty;
                      Provider.of<RegisterNotifier>(context, listen: false).updatePlaque(value);
                    });
                  },
                ),
                BuildRegisterNextButton(
                  callback: () {
                    if (isCarModelValid && isPlaqueValid) {
                      Navigator.pushNamed(context, '/register_user_extra_data');
                    } else if (!isCarModelValid || !isPlaqueValid) {
                      customScaffoldMessenger(context, Icons.wrap_text, Colors.red, 'Llena todos los campos');
                    }
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}