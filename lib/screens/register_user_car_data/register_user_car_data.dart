import 'package:treinta_y_tres/application/application_dependencies.dart';

class RegisterUserCarData extends StatefulWidget {
  const RegisterUserCarData({Key? key}) : super(key: key);

  @override
  State<RegisterUserCarData> createState() => _RegisterUserCarDataState();
}

class _RegisterUserCarDataState extends State<RegisterUserCarData> {

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: Color(0XFF1C1C1C),
      body: RegisterUserCarDataBody(),
    );
  }
}
