import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class BuildDeniedLogo extends StatefulWidget {
  const BuildDeniedLogo({Key? key}) : super(key: key);

  @override
  State<BuildDeniedLogo> createState() => _BuildDeniedLogoState();
}

class _BuildDeniedLogoState extends State<BuildDeniedLogo> {

  @override
  Widget build(BuildContext context) {

    double screenWidth = MediaQuery.of(context).size.width;
    double screenHeight = MediaQuery.of(context).size.height;

    return Positioned(
      top: 0,
      child: SizedBox(
        height: MediaQuery.of(context).size.height * 0.8,
        width: screenWidth,
        child: SizedBox(
          height: MediaQuery.of(context).size.height * 0.8,
          width: double.infinity,
          child: Container(
            margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.22),
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Image.asset(
                    'assets/images/request_map_permission_denied_logo.png',
                    width: screenWidth * 0.3,
                    height: screenHeight * 0.25,
                  ),
                  Text(
                    "Lo sentimos",
                    style: GoogleFonts.poppins(
                      fontSize: 27,
                      fontWeight: FontWeight.w500,
                      color: Colors.white,
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(
                      top: MediaQuery.of(context).size.height * 0.05,
                      right: MediaQuery.of(context).size.height * 0.03,
                      left: MediaQuery.of(context).size.height * 0.03,
                    ),
                    child: Text(
                      "Es necesario autorizar los permisos de localizacion para el uso correcto y seguro de esta aplicacion",
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}