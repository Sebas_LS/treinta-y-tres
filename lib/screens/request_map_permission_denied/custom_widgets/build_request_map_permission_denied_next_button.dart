import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildRequestMapPermissionDeniedNextButton extends StatefulWidget {
  const BuildRequestMapPermissionDeniedNextButton({Key? key}) : super(key: key);

  @override
  State<BuildRequestMapPermissionDeniedNextButton> createState() => _BuildRequestMapPermissionDeniedNextButtonState();
}

class _BuildRequestMapPermissionDeniedNextButtonState extends State<BuildRequestMapPermissionDeniedNextButton> {

  @override
  Widget build(BuildContext context) {

    double screenWidth = MediaQuery.of(context).size.width;

    return Positioned(
      top: MediaQuery.of(context).size.height * 0.8,
      child: SizedBox(
        height: MediaQuery.of(context).size.height * 0.2,
        width: screenWidth,
        child: Center(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                width: MediaQuery.of(context).size.width * 0.8,
                child: ElevatedButton(
                  onPressed: () {
                    showMapsRequestPermission(
                      context,
                      () => Navigator.pushNamedAndRemoveUntil(context, '/request_notifications_permission', (route) => false),
                      () => Navigator.pop(context),
                    );
                  },
                  style: ElevatedButton.styleFrom(
                    padding: const EdgeInsets.symmetric(vertical: 12),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(30),
                    ),
                    backgroundColor: Colors.white,
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Entiendo',
                        style: GoogleFonts.poppins(
                          fontSize: 17,
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
