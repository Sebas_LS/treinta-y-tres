import 'package:treinta_y_tres/application/application_dependencies.dart';

class RequestMapPermissionDeniedBody extends StatefulWidget {
  const RequestMapPermissionDeniedBody({Key? key}) : super(key: key);

  @override
  State<RequestMapPermissionDeniedBody> createState() => _RequestMapPermissionDeniedBodyState();
}

class _RequestMapPermissionDeniedBodyState extends State<RequestMapPermissionDeniedBody> {

  @override
  Widget build(BuildContext context) {
    return const Stack(
      children: [
        BuildDeniedLogo(),
        BuildRequestMapPermissionDeniedNextButton(),
      ],
    );
  }
}
