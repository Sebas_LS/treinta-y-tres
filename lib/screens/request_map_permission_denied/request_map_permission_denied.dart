import 'package:treinta_y_tres/application/application_dependencies.dart';

class RequestMapPermissionDenied extends StatefulWidget {
  const RequestMapPermissionDenied({Key? key}) : super(key: key);

  @override
  State<RequestMapPermissionDenied> createState() => _RequestMapPermissionDeniedState();
}

class _RequestMapPermissionDeniedState extends State<RequestMapPermissionDenied> {

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: Color(0XFF1C1C1C),
      body: RequestMapPermissionDeniedBody(),
    );
  }
}