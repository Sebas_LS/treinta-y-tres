import 'package:flutter/material.dart';

class RegisterData {
  String username;
  String email;
  String password;
  String name;
  String plaque;
  String keyword;
  String carModelName;
  int driverTypeId;
  String imagePath;

  RegisterData({
    required this.username,
    required this.email,
    required this.password,
    required this.name,
    required this.plaque,
    required this.keyword,
    required this.carModelName,
    required this.driverTypeId,
    required this.imagePath,
  });
}

class RegisterNotifier extends ChangeNotifier {
  RegisterData _data = RegisterData(username: '', email: '', password: '', name: '', plaque: '', keyword: '', carModelName: '', driverTypeId: 1, imagePath: '');

  RegisterNotifier() {
    _data = RegisterData(username: '', email: '', password: '', name: '', plaque: '', keyword: '', carModelName: '', driverTypeId: 1, imagePath: '');
  }

  RegisterData get data => _data;

  void updateName(String name) {
    _data.name = name;
    notifyListeners();
  }

  void updateUsername(String username) {
    _data.username = username;
    notifyListeners();
  }

  void updateEmail(String email) {
    _data.email = email;
    notifyListeners();
  }

  void updatePassword(String password) {
    _data.password = password;
    notifyListeners();
  }

  void updateCarModel(String carModelName) {
    _data.carModelName = carModelName;
    notifyListeners();
  }

  void updatePlaque(String plaque) {
    _data.plaque = plaque;
    notifyListeners();
  }

  void updateDriverType(int value) {
    _data.driverTypeId = value;
    notifyListeners();
  }

  void updateKeyword(String keyword) {
    _data.keyword = keyword;
    notifyListeners();
  }

  void updateImagePath(String imagePath) {
    _data.imagePath = imagePath;
    notifyListeners();
  }

  void clearImagePath() {
    _data.imagePath = '';
    notifyListeners();
  }
}