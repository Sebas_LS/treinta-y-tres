import 'package:treinta_y_tres/application/application_dependencies.dart';

class EmergencyContact {
  final String name;

  EmergencyContact({required this.name});

  Map<String, dynamic> toMap() {
    return {
      'name': name,
    };
  }

  factory EmergencyContact.fromMap(Map<String, dynamic> map) {
    return EmergencyContact(
      name: map['name'],
    );
  }

  static List<Map<String, dynamic>> contactsToMapList(List<EmergencyContact> contacts) {
    return contacts.map((contact) => contact.toMap()).toList();
  }

  static List<EmergencyContact> contactsFromMapList(List<dynamic> mapList) {
    return mapList.map((map) => EmergencyContact.fromMap(Map<String, dynamic>.from(map))).toList();
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
          other is EmergencyContact &&
              runtimeType == other.runtimeType &&
              name == other.name;

  @override
  int get hashCode => name.hashCode;
}

class EmergencyContactsNotifier extends ChangeNotifier {
  List<EmergencyContact> _dangerContacts = [];
  List<EmergencyContact> _unsafetyContacts = [];
  List<EmergencyContact> _suspicionContacts = [];
  List<EmergencyContact> _locationContacts = [];

  List<EmergencyContact> get dangerContacts => _dangerContacts;
  List<EmergencyContact> get unsafetyContacts => _unsafetyContacts;
  List<EmergencyContact> get suspicionContacts => _suspicionContacts;
  List<EmergencyContact> get locationContacts => _locationContacts;

  void addContactToList(EmergencyContact contact, List<EmergencyContact> contactList) {
    if (!contactList.any((c) => c.name == contact.name)) {
      contactList.add(contact);
      print("Added ${contact.name} to the list.");
      notifyListeners();
    } else {
      print("Contact ${contact.name} is already in the list.");
    }
  }

  List<EmergencyContact> getAllContacts() {
    return [
      ..._dangerContacts,
      ..._unsafetyContacts,
      ..._suspicionContacts,
      ..._locationContacts
    ].toSet().toList();
  }

  void addDangerContact(EmergencyContact contact) {
    addContactToList(contact, _dangerContacts);
  }

  void addUnsafetyContact(EmergencyContact contact) {
    addContactToList(contact, _unsafetyContacts);
  }

  void addSuspicionContact(EmergencyContact contact) {
    addContactToList(contact, _suspicionContacts);
  }

  void addLocationContact(EmergencyContact contact) {
    addContactToList(contact, _locationContacts);
  }

  void removeContact(EmergencyContactType type, String contactName) {
    bool existsInOtherLists = false;

    switch (type) {
      case EmergencyContactType.danger:
        _dangerContacts.removeWhere((contact) => contact.name == contactName);
        existsInOtherLists = _unsafetyContacts.any((contact) => contact.name == contactName) ||
            _suspicionContacts.any((contact) => contact.name == contactName) ||
            _locationContacts.any((contact) => contact.name == contactName);
        break;
      case EmergencyContactType.unsafety:
        _unsafetyContacts.removeWhere((contact) => contact.name == contactName);
        existsInOtherLists = _dangerContacts.any((contact) => contact.name == contactName) ||
            _suspicionContacts.any((contact) => contact.name == contactName) ||
            _locationContacts.any((contact) => contact.name == contactName);
        break;
      case EmergencyContactType.suspicion:
        _suspicionContacts.removeWhere((contact) => contact.name == contactName);
        existsInOtherLists = _dangerContacts.any((contact) => contact.name == contactName) ||
            _unsafetyContacts.any((contact) => contact.name == contactName) ||
            _locationContacts.any((contact) => contact.name == contactName);
        break;
      case EmergencyContactType.transit:
        _locationContacts.removeWhere((contact) => contact.name == contactName);
        existsInOtherLists = _dangerContacts.any((contact) => contact.name == contactName) ||
            _unsafetyContacts.any((contact) => contact.name == contactName) ||
            _suspicionContacts.any((contact) => contact.name == contactName);
        break;
    }

    notifyListeners();
    saveData();
  }

  Future<void> saveData() async {
    await SharedPrefManager.putStringList('dangerContacts', _dangerContacts.map((contact) => contact.toMap()).toList());
    await SharedPrefManager.putStringList('unsafetyContacts', _unsafetyContacts.map((contact) => contact.toMap()).toList());
    await SharedPrefManager.putStringList('suspicionContacts', _suspicionContacts.map((contact) => contact.toMap()).toList());
    await SharedPrefManager.putStringList('locationContacts', _locationContacts.map((contact) => contact.toMap()).toList());
  }

  Future<void> loadData() async {
    List<Map<String, dynamic>>? dangerContactsMaps = await SharedPrefManager.getStringListEmergencyContacts('dangerContacts');
    if (dangerContactsMaps != null) {
      _dangerContacts = EmergencyContact.contactsFromMapList(dangerContactsMaps);
    }

    List<Map<String, dynamic>>? unsafetyContactsMaps = await SharedPrefManager.getStringListEmergencyContacts('unsafetyContacts');
    if (unsafetyContactsMaps != null) {
      _unsafetyContacts = EmergencyContact.contactsFromMapList(unsafetyContactsMaps);
    }

    List<Map<String, dynamic>>? suspicionContactsMaps = await SharedPrefManager.getStringListEmergencyContacts('suspicionContacts');
    if (suspicionContactsMaps != null) {
      _suspicionContacts = EmergencyContact.contactsFromMapList(suspicionContactsMaps);
    }

    List<Map<String, dynamic>>? locationContactsMaps = await SharedPrefManager.getStringListEmergencyContacts('locationContacts');
    if (locationContactsMaps != null) {
      _locationContacts = EmergencyContact.contactsFromMapList(locationContactsMaps);
    }

    notifyListeners();
  }
}