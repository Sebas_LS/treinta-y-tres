import 'package:treinta_y_tres/application/application_dependencies.dart';

class AlertHistoryNotifier with ChangeNotifier {
  List<Alert> _alertHistory = [];
  int _sentAlerts = 0;
  int _receivedAlerts = 0;
  int _takenAlerts = 0;

  AlertHistoryNotifier() {
    initValues();
  }

  Future<void> initValues() async {
    _sentAlerts = await SharedPrefManager.getSentAlerts();
    _receivedAlerts = await SharedPrefManager.getReceivedAlerts();
    _takenAlerts = await SharedPrefManager.getTakenAlerts();
    _alertHistory = await SharedPrefManager.getAlertHistory();
    notifyListeners();
  }

  List<Alert> get alertHistory => _alertHistory;
  int get sentAlerts => _sentAlerts;
  int get receivedAlerts => _receivedAlerts;
  int get takenAlerts => _takenAlerts;

  void addAlert(Alert alert) async {
    _alertHistory.add(alert);
    await SharedPrefManager.setAlertHistory(_alertHistory);
    notifyListeners();
  }

  void clearAlerts() async {
    _alertHistory.clear();
    await SharedPrefManager.setAlertHistory(_alertHistory);
    notifyListeners();
  }

  void incrementSentAlerts() async {
    _sentAlerts++;
    await SharedPrefManager.setSentAlerts(_sentAlerts);
    notifyListeners();
  }

  void incrementReceivedAlerts() async {
    _receivedAlerts++;
    await SharedPrefManager.setReceivedAlerts(_receivedAlerts);
    notifyListeners();
  }

  void incrementTakenAlerts() async {
    _takenAlerts++;
    await SharedPrefManager.setTakenAlerts(_takenAlerts);
    notifyListeners();
  }

  void resetAlerts() {
    _sentAlerts = 0;
    _receivedAlerts = 0;
    _takenAlerts = 0;
    notifyListeners();
  }
}

class Alert {
  final DateTime date;
  final String title;

  Alert({
    required this.date,
    required this.title,
  });

  Map<String, dynamic> toJSON() {
    return {
      'date': date.toIso8601String(),
      'title': title,
    };
  }

  static Alert fromJSON(Map<String, dynamic> json) {
    DateTime date = DateTime.parse(json['date']);
    String title = json['title'];
    return Alert(date: date, title: title);
  }
}