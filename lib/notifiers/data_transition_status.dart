import 'package:treinta_y_tres/application/application_dependencies.dart';

class AlertStatusNotifier with ChangeNotifier {
  ValueNotifier<bool> _isAlertTriggered = ValueNotifier<bool>(false);
  bool stopEventFired = false;

  ValueNotifier<bool> get isAlertTriggered => _isAlertTriggered;

  void triggerAlert() {
    _isAlertTriggered.value = true;
    notifyListeners();
  }

  void stopSendingData() {
    _isAlertTriggered.value = false;
    notifyListeners();
  }
}