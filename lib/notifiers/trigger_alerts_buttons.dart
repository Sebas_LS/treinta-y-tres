import 'package:flutter/foundation.dart';

class ButtonLockNotifier extends ChangeNotifier {
  bool _areButtonsLocked = false;

  bool get areButtonsLocked => _areButtonsLocked;

  void lockButtons() {
    _areButtonsLocked = true;
    notifyListeners();
  }

  void unlockButtons() {
    _areButtonsLocked = false;
    notifyListeners();
  }
}
