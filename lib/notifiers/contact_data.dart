import 'package:flutter/foundation.dart';

class CustomContact {
  String? name;
  String? phoneNumber;

  CustomContact({this.name, this.phoneNumber});
}

class ContactData extends ChangeNotifier {

  List<CustomContact> _dangerContacts = [];
  List<CustomContact> _unsafetyContacts = [];
  List<CustomContact> _suspicionContacts = [];
  List<CustomContact> _locationInfoContacts = [];

  List<CustomContact> get dangerContacts => _dangerContacts;
  List<CustomContact> get unsafetyContacts => _unsafetyContacts;
  List<CustomContact> get suspicionContacts => _suspicionContacts;
  List<CustomContact> get locationInfoContacts => _locationInfoContacts;

  void addDangerContact(String name, String phoneNumber) {
    _dangerContacts.add(CustomContact(name: name, phoneNumber: phoneNumber));
    notifyListeners();
  }

  void addUnsafetyContact(String name, String phoneNumber) {
    _unsafetyContacts.add(CustomContact(name: name, phoneNumber: phoneNumber));
    notifyListeners();
  }

  void addSuspicionContact(String name, String phoneNumber) {
    _suspicionContacts.add(CustomContact(name: name, phoneNumber: phoneNumber));
    notifyListeners();
  }

  void addLocationInfoContact(String name, String phoneNumber) {
    _locationInfoContacts.add(CustomContact(name: name, phoneNumber: phoneNumber));
    notifyListeners();
  }

  void removeDangerContact(CustomContact contact) {
    _dangerContacts.remove(contact);
    notifyListeners();
  }

  void removeUnsafetyContact(CustomContact contact) {
    _unsafetyContacts.remove(contact);
    notifyListeners();
  }

  void removeSuspicionContact(CustomContact contact) {
    _suspicionContacts.remove(contact);
    notifyListeners();
  }

  void removeLocationInfoContact(CustomContact contact) {
    _locationInfoContacts.remove(contact);
    notifyListeners();
  }
}