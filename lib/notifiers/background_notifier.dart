import 'package:flutter/material.dart';

class BackgroundNotifier with ChangeNotifier {
  String? backgroundImagePath;

  String get backgroundImagePaths => backgroundImagePath!;

  void setBackgroundImagePath(String path) {
    backgroundImagePath = path;
    notifyListeners();
  }

  void removeBackgroundImage() {
    backgroundImagePath = null;
    notifyListeners();
  }
}