import 'package:flutter/foundation.dart';

class UserInDangerNotifier extends ChangeNotifier {
  String? _userInDangerId;

  String? get userInDangerId => _userInDangerId;

  void setUserInDangerId(String? id) {
    _userInDangerId = id;
    notifyListeners();
  }

  void clearUserInDangerId() {
    _userInDangerId = null;
    notifyListeners();
  }
}