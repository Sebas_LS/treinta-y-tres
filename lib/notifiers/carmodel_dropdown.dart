import 'package:flutter/foundation.dart';

class DropdownSelectionModel with ChangeNotifier {
  String _selectedValue = 'Uber';
  String get selectedValue => _selectedValue;

  void setSelectedValue(String value) {
    _selectedValue = value;
    notifyListeners();
  }
}
