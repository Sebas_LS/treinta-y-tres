import 'package:flutter/material.dart';

class ConnectionStatusNotifier extends ChangeNotifier {
  bool _isOnline = false;

  bool get isOnline => _isOnline;

  void toggle() {
    _isOnline = !_isOnline;
    notifyListeners();
  }
}