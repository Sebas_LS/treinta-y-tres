import 'package:treinta_y_tres/application/application_dependencies.dart';

class ContactUpdateNotifier extends ChangeNotifier {
  void update() {
    notifyListeners();
  }
}