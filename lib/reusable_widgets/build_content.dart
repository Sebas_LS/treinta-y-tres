import 'package:treinta_y_tres/application/application_dependencies.dart';

Widget buildContent(BuildContext context, String headerText, double porcentualHeight,
    {List<Widget>? children, String headerSubtitleText = '', double fontSize = 30, double marginTop = 0, double horizontalPadding = 40}
) {
  double initialHeight = MediaQuery.of(context).size.height * porcentualHeight;
  return SizedBox(
    width: double.infinity,
    height: double.infinity,
    child: SingleChildScrollView(
      physics: const AlwaysScrollableScrollPhysics(),
      padding: EdgeInsets.symmetric(horizontal: horizontalPadding, vertical: initialHeight),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            width: double.infinity,
            alignment: Alignment.centerLeft,
            padding: EdgeInsets.only(bottom: MediaQuery.of(context).size.height * 0.035),
            child: InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: const Icon(
                Icons.arrow_circle_left,
                size: 40,
                color: Colors.white
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: marginTop),
            child: Container(
              width: double.infinity,
              alignment: Alignment.centerLeft,
              child: Text(
                headerText,
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.white,
                    fontFamily: 'OpenSans',
                    fontSize: fontSize,
                    fontWeight: FontWeight.bold
                ),
              ),
            ),
          ),
          Container(
            margin: const EdgeInsets.only(top: 8.0),
            child: Text(
              headerSubtitleText,
              textAlign: TextAlign.center,
              style: const TextStyle(
                  color: Colors.white,
                  fontFamily: 'OpenSans',
                  fontSize: 15.0,
                  fontWeight: FontWeight.bold
              ),
            ),
          ),
          ...children!,
        ],
      ),
    ),
  );
}