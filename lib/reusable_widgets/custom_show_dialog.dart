import 'package:treinta_y_tres/application/application_dependencies.dart';

Future<void> showCustomDialog(
  BuildContext context,
  String title,
  String buttonTitle,
  Function acceptCallback,
  {
    Function? cancelCallback = null,
    Widget? calcelButton = null,
    bool showSecondButton = false,
    String secondButtonTitle = '',
  }
) {
  return showDialog<void>(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        backgroundColor: Colors.white,
        elevation: 15,
        title: Text(
          title,
          style: GoogleFonts.poppins(
            fontSize: 16,
            fontWeight: FontWeight.w500,
            color: Colors.black,
          )
        ),
        actions: <Widget>[
          if (showSecondButton)
            TextButton(
              onPressed: () => {
                cancelCallback!()
              },
              child: Text(
                secondButtonTitle,
                style: GoogleFonts.poppins(
                  fontSize: 14,
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                )
              ),
            ),
          TextButton(
            onPressed: () => {
              acceptCallback()
            },
            child: Text(
              buttonTitle,
              style: GoogleFonts.poppins(
                fontSize: 14,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              )
            ),
          ),
        ],
      );
    },
  );
}