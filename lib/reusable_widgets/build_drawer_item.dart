import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildDrawerItem extends StatefulWidget {

  final String? title;
  final String? subtitle;
  final IconData? icon;
  final double? marginTop;
  final Function? onTap;

  const BuildDrawerItem({
    Key? key,
    required this.title,
    required this.subtitle,
    required this.icon,
    required this.marginTop,
    required this.onTap
  }) : super(key: key);

  @override
  State<BuildDrawerItem> createState() => _BuildDrawerItemState();
}

class _BuildDrawerItemState extends State<BuildDrawerItem> {

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: widget.marginTop!),
      child: InkWell(
        onTap: () => widget.onTap!(),
        child: ListTile(
          leading: Icon(
            widget.icon!,
            color: Colors.white,
          ),
          title: Text(
            widget.title!,
            style: GoogleFonts.poppins(
              fontSize: 15,
              fontWeight: FontWeight.bold,
              color: Colors.white,
            )
          ),
          subtitle: Text(
            widget.subtitle!,
            style: GoogleFonts.poppins(
              fontSize: 10,
              fontWeight: FontWeight.bold,
              color: Colors.white,
            )
          ),
        ),
      ),
    );
  }
}
