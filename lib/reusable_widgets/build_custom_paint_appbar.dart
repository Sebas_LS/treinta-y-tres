import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildCustomPaintAppbar extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = const Color(0xFFF1F1F1)
      ..strokeWidth = 5
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.fill;

    final path = Path();

    path.lineTo(0, size.height * 0.1);
    path.quadraticBezierTo(size.width * 0.1, size.height * 0.13, size.width * 0.5, size.height * 0.13);
    path.quadraticBezierTo(size.width * 0.9, size.height * 0.13, size.width, size.height * 0.2);
    path.lineTo(size.width, 0);
    path.lineTo(0, 0);

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(BuildCustomPaintAppbar oldDelegate) => true;
}