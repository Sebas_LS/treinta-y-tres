import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildRegisterHeaderText extends StatefulWidget {

  final String? title;
  final String? subtitle;

  const BuildRegisterHeaderText({
    Key? key,
    required this.title,
    required this.subtitle,
  }) : super(key: key);

  @override
  State<BuildRegisterHeaderText> createState() => _BuildRegisterHeaderTextState();
}

class _BuildRegisterHeaderTextState extends State<BuildRegisterHeaderText> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.03),
      child: Column(
        children: [
          Text(
            widget.title!,
            style: GoogleFonts.poppins(
              fontSize: 24,
              fontWeight: FontWeight.w400,
              color: Colors.black,
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.005),
            child: Text(
              widget.subtitle!,
              style: GoogleFonts.poppins(
                fontSize: 15,
                fontWeight: FontWeight.w400,
                color: Colors.black,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              left:  MediaQuery.of(context).size.width * 0.2,
              right: MediaQuery.of(context).size.width * 0.2,
              top: MediaQuery.of(context).size.height * 0.01,
            ),
            child: const Divider(
              thickness: 1,
              color: Colors.black54,
            ),
          ),
        ],
      ),
    );
  }
}