import 'package:treinta_y_tres/application/application_dependencies.dart';

Widget buildCustomNextButton(BuildContext context, String buttonText, double marginTop,
    {String routeName = '', bool deletePreviousRoutes = false, double leftMargin = 0.0, double rightMargin = 0.0, Function? callback}
) {
  return Container(
    margin: EdgeInsets.only(top: marginTop, left: leftMargin, right: rightMargin),
    width: double.infinity,
    child: ElevatedButton(
      style: ElevatedButton.styleFrom(
        elevation: 5.0,
        padding: const EdgeInsets.all(17.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
        backgroundColor: Colors.white,
      ),
      onPressed: () {
        if (deletePreviousRoutes) {
          Navigator.pushNamedAndRemoveUntil(context, routeName, (route) => false);
        } else {
          if (routeName != '') Navigator.pushNamed(context, routeName);
        }
        if (callback != null) callback();
      },
      child: Text(
        buttonText,
        textAlign: TextAlign.center,
        style: const TextStyle(
          color: Color(0xFF527DAA),
          letterSpacing: 1.0,
          fontSize: 18.0,
          fontWeight: FontWeight.bold,
          fontFamily: 'OpenSans',
        ),
      ),
    ),
  );
}