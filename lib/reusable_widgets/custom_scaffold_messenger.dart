import 'package:treinta_y_tres/application/application_dependencies.dart';

void customScaffoldMessenger(BuildContext context, IconData startIcon, Color iconColor, String message) {
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      content: Row(
        children: [
          Icon(
            startIcon,
            color: iconColor,
          ),
          Container(
            margin: EdgeInsets.only(
              left: MediaQuery.of(context).size.width * 0.02
            ),
            child: Text(
              message,
              style: GoogleFonts.poppins(
                fontSize: 13,
                fontWeight: FontWeight.bold,
                color: Colors.white,
              )
            ),
          ),
        ],
      ),
    ),
  );
}