import 'package:treinta_y_tres/application/application_dependencies.dart';

Widget buildUserImage(BuildContext context, double radius, double marginTop, {Color color = Colors.blue}) {
  bool isDarkTheme = Theme.of(context).brightness == Brightness.dark;
  return Container(
    margin: EdgeInsets.only(top: marginTop),
    alignment: Alignment.center,
    child: CircleAvatar(
      radius: radius,
      backgroundColor: isDarkTheme ? Colors.white : color,
      child: const Text('Foto'),
    ),
  );
}