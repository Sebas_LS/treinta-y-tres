import 'package:treinta_y_tres/application/application_dependencies.dart';

Widget buildCustomText(String textData, double marginTop, Alignment alignment, double fontSize,
    {double marginLeft = 30, double marginRight = 30, double horizontalPadding = 10, Color color = Colors.black, TextAlign textAlign = TextAlign.start}
) {
  return Container(
    alignment: alignment,
    padding: EdgeInsets.symmetric(horizontal: horizontalPadding, vertical: 4),
    margin: EdgeInsets.only(top: marginTop, left: marginLeft, right: marginRight),
    child: Text(
      textData,
      textAlign: textAlign,
      style: TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: fontSize,
        color: color,
      ),
    ),
  );
}