import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildCustomPaintBackNavigation extends StatefulWidget {
  final String? title;
  final double? fontSize;
  const BuildCustomPaintBackNavigation({Key? key, required this.title, required this.fontSize}) : super(key: key);

  @override
  State<BuildCustomPaintBackNavigation> createState() => _BuildCustomPaintBackNavigationState();
}

class _BuildCustomPaintBackNavigationState extends State<BuildCustomPaintBackNavigation> {

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.2,
      width: double.infinity,
      decoration: const BoxDecoration(
        color: Color(0XFF1C1C1C),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.05),
            child: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: const Icon(
                Icons.arrow_circle_left,
                color: Colors.white,
                size: 40,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              left: MediaQuery.of(context).size.width * 0.05,
              top: MediaQuery.of(context).size.height * 0.01,
            ),
            child: Text(
              widget.title!,
              textAlign: TextAlign.center,
              style: GoogleFonts.poppins(
                fontSize: widget.fontSize,
                fontWeight: FontWeight.w500,
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }
}