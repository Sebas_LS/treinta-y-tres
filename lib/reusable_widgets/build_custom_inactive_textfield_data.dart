import 'package:treinta_y_tres/application/application_dependencies.dart';

Widget buildCustomInactiveTextFieldData(IconData icon, String textData, double paddingTop) {
  return Container(
    padding: const EdgeInsets.symmetric(horizontal: 22, vertical: 15),
    margin: EdgeInsets.only(top: paddingTop, left: 25, right: 25),
    decoration: BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(30),
      boxShadow: const [
        BoxShadow(
          color: Colors.black26,
          blurRadius: 6,
          offset: Offset(0, 3),
        ),
      ],
    ),
    child: Row(
      children: [
        Icon(icon, color: Colors.black54),
        const SizedBox(width: 10),
        Text(
          textData,
          style: const TextStyle(
            fontSize: 19,
            fontWeight: FontWeight.w300,
          ),
        ),
      ],
    ),
  );
}