import 'package:treinta_y_tres/application/application_dependencies.dart';

class BuildUserAgreementItem extends StatefulWidget {

  final String? title;
  final String? subtitle;
  final double? topMargin;
  final Function? callback;

  const BuildUserAgreementItem({
    Key? key,
    required this.title,
    required this.subtitle,
    required this.topMargin,
    required this.callback,
  }) : super(key: key);

  @override
  State<BuildUserAgreementItem> createState() => _BuildUserAgreementItemState();
}

class _BuildUserAgreementItemState extends State<BuildUserAgreementItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: widget.topMargin!, // MediaQuery.of(context).size.height * 0.08,
        left: MediaQuery.of(context).size.width * 0.07,
        right: MediaQuery.of(context).size.width * 0.07,
      ),
      child: InkWell(
        onTap: () => widget.callback!(),
        child: ListTile(
          title: Text(
            widget.title!,
            style: GoogleFonts.poppins(
              fontWeight: FontWeight.bold,
              fontSize: 15,
              color: Colors.black87,
            ),
          ),
          subtitle: Text(
            widget.subtitle!,
            style: GoogleFonts.poppins(
              fontWeight: FontWeight.bold,
              fontSize: 12,
              color: Colors.black87,
            ),
          ),
          trailing: const Icon(
            Icons.arrow_circle_right,
            color: Colors.black87,
          ),
        ),
      ),
    );
  }
}
