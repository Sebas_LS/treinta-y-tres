// Flutter packages
export 'package:flutter/material.dart';
export 'package:image_picker/image_picker.dart';
export 'package:google_maps_flutter/google_maps_flutter.dart';
export 'package:clipboard/clipboard.dart';
export 'package:provider/provider.dart';
export 'dart:io';
export 'dart:convert';
export 'dart:async';
export 'package:permission_handler/permission_handler.dart';
export 'package:flutter_native_contact_picker/flutter_native_contact_picker.dart';
export 'package:flutter/services.dart';
export 'package:google_fonts/google_fonts.dart';
export 'package:treinta_y_tres/screens_logic/shared_preferences_manager/shared_preferences.dart';
export 'package:flutter_nfc_kit/flutter_nfc_kit.dart';
// export 'package:move_to_background/move_to_background.dart';

// Notifiers
export 'package:treinta_y_tres/notifiers/contact_data.dart';
export 'package:treinta_y_tres/notifiers/floating_button_custom_icons_data.dart';
export 'package:treinta_y_tres/notifiers/register_data.dart';
export 'package:treinta_y_tres/notifiers/carmodel_dropdown.dart';
export 'package:treinta_y_tres/notifiers/background_notifier.dart';
export 'package:treinta_y_tres/notifiers/connection_status.dart';
export 'package:treinta_y_tres/notifiers/emergency_contacts.dart';
export 'package:treinta_y_tres/notifiers/emergency_contacts_update.dart';
export 'package:treinta_y_tres/notifiers/data_transition_status.dart';
export 'package:treinta_y_tres/notifiers/trigger_alerts_buttons.dart';
export 'package:treinta_y_tres/notifiers/alert_history.dart';
export 'package:treinta_y_tres/notifiers/user_in_danger_id.dart';

// UI : Request map permission
export 'package:treinta_y_tres/screens/request_map_permission/request_map_permission.dart';
export 'package:treinta_y_tres/screens/request_map_permission/default_widgets/request_map_permission_body.dart';
export 'package:treinta_y_tres/screens/request_map_permission/custom_widgets/build_map_logo.dart';
export 'package:treinta_y_tres/screens/request_map_permission/custom_widgets/build_request_map_permission_next_button.dart';

// UI : Request map permission denied
export 'package:treinta_y_tres/screens/request_map_permission_denied/request_map_permission_denied.dart';
export 'package:treinta_y_tres/screens/request_map_permission_denied/default_widgets/request_map_permission_denied_body.dart';
export 'package:treinta_y_tres/screens/request_map_permission_denied/custom_widgets/build_denied_logo.dart';
export 'package:treinta_y_tres/screens/request_map_permission_denied/custom_widgets/build_request_map_permission_denied_next_button.dart';

// UI : Login
export 'package:treinta_y_tres/screens/login_user/login_user.dart';
export 'package:treinta_y_tres/screens/login_user/default_widgets/login_user_body.dart';
export 'package:treinta_y_tres/screens/login_user/custom_widgets/build_app_logo.dart';
export 'package:treinta_y_tres/screens/login_user/custom_widgets/build_login_form.dart';
export 'package:treinta_y_tres/screens/login_user/custom_widgets/build_active_input.dart';
export 'package:treinta_y_tres/screens/login_user/custom_widgets/build_dont_have_account.dart';
export 'package:treinta_y_tres/screens/login_user/custom_widgets/build_login_form_header.dart';
export 'package:treinta_y_tres/screens/login_user/custom_widgets/build_login_user_next_button.dart';

// UI : Register user data
export 'package:treinta_y_tres/screens/register_user_data/register_user_data.dart';
export 'package:treinta_y_tres/screens/register_user_data/default_widgets/register_user_data_body.dart';
export 'package:treinta_y_tres/reusable_widgets/build_custom_paint_back_navigation.dart';
export 'package:treinta_y_tres/reusable_widgets/build_register_header_text.dart';
export 'package:treinta_y_tres/reusable_widgets/build_register_next_button.dart';

// UI : Register user car data
export 'package:treinta_y_tres/screens/register_user_car_data/register_user_car_data.dart';
export 'package:treinta_y_tres/screens/register_user_car_data/default_widgets/register_user_car_data_body.dart';

// UI : Register user extra data
export 'package:treinta_y_tres/screens/register_user_extra_data/default_widgets/register_user_extra_data_body.dart';
export 'package:treinta_y_tres/screens/register_user_extra_data/register_user_extra_data.dart';
export 'package:treinta_y_tres/screens/register_user_extra_data/custom_widgets/build_user_photo_and_header_text.dart';
export 'package:treinta_y_tres/screens/register_user_extra_data/custom_widgets/build_terms_and_conditions_info.dart';
export 'package:treinta_y_tres/screens/register_user_extra_data/custom_widgets/build_drivers_type_dropdown.dart';

// UI : User agreement
export 'package:treinta_y_tres/screens/user_agreement/user_agreement.dart';
export 'package:treinta_y_tres/screens/user_agreement/default_widgets/user_agreement_body.dart';
export 'package:treinta_y_tres/reusable_widgets/build_user_agreement_item.dart';

// UI : Terms and conditions
export 'package:treinta_y_tres/screens/terms_and_conditions/terms_and_conditions.dart';
export 'package:treinta_y_tres/screens/terms_and_conditions/default_widgets/terms_and_conditions_body.dart';

// UI : Policy and privacy
export 'package:treinta_y_tres/screens/policy_and_privacy/policy_and_privacy.dart';
export 'package:treinta_y_tres/screens/policy_and_privacy/default_widgets/policy_and_privacy_body.dart';

// UI : Disclaimer and terms
export 'package:treinta_y_tres/screens/disclaimer_and_terms/disclaimer_and_terms.dart';
export 'package:treinta_y_tres/screens/disclaimer_and_terms/default_widgets/disclaimer_and_terms_body.dart';

// UI : Lobby
export 'package:treinta_y_tres/screens/lobby/lobby.dart';
export 'package:treinta_y_tres/screens/lobby/default_widgets/lobby_body.dart';
export 'package:treinta_y_tres/screens/lobby/custom_widgets/build_lobby_header.dart';
export 'package:treinta_y_tres/screens/lobby/custom_widgets/build_lobby_header_divider.dart';
export 'package:treinta_y_tres/screens/lobby/custom_widgets/build_section_title.dart';
export 'package:treinta_y_tres/screens/lobby/custom_widgets/build_connection_status.dart';
export 'package:treinta_y_tres/screens/lobby/custom_widgets/build_emergency_contacts.dart';
export 'package:treinta_y_tres/screens/lobby/custom_widgets/build_security_tips.dart';
export 'package:treinta_y_tres/screens/lobby/custom_widgets/build_alerts_record.dart';
export 'package:treinta_y_tres/screens/lobby/default_widgets/lobby_drawer.dart';
export 'package:treinta_y_tres/screens/lobby/custom_widgets/build_drawer_header.dart';
export 'package:treinta_y_tres/reusable_widgets/build_drawer_item.dart';
export 'package:treinta_y_tres/screens_logic/lobby/get_and_send_position.dart';
export 'package:treinta_y_tres/models/user_in_danger_data.dart';

// UI : Profile
export 'package:treinta_y_tres/screens/profile/profile.dart';
export 'package:treinta_y_tres/screens/profile/default_widgets/profile_body.dart';
export 'package:treinta_y_tres/screens/profile/custom_widgets/build_user_photo.dart';
export 'package:treinta_y_tres/screens/profile/custom_widgets/build_header_divider.dart';
export 'package:treinta_y_tres/screens/profile/custom_widgets/build_header_text.dart';
export 'package:treinta_y_tres/screens/profile/custom_widgets/build_alerts_info.dart';
export 'package:treinta_y_tres/screens/profile/custom_widgets/build_update_profile_button.dart';
export 'package:treinta_y_tres/screens/profile/custom_widgets/build_user_data_input.dart';
export 'package:treinta_y_tres/screens/profile/custom_widgets/build_user_data_input_title.dart';
export 'package:treinta_y_tres/screens/profile/custom_widgets/build_active_input.dart';
export 'package:treinta_y_tres/screens_logic/profile/update_profile.dart';
export 'package:treinta_y_tres/screens/profile/custom_widgets/build_user_data.dart';

// UI : Messages
export 'package:treinta_y_tres/screens/messages/messages.dart';
export 'package:treinta_y_tres/screens/messages/default_widgets/messages_body.dart';
export 'package:treinta_y_tres/screens/messages/custom_widgets/build_custom_appbar_content.dart';
export 'package:treinta_y_tres/screens/messages/custom_widgets/build_messages_scroll_limit.dart';
export 'package:treinta_y_tres/screens/messages/custom_widgets/build_default_message.dart';

// UI : Stealth mode
export 'package:treinta_y_tres/screens/stealth_mode/stealth_mode.dart';
export 'package:treinta_y_tres/screens/stealth_mode/default_widgets/stealth_mode_floating_button.dart';
export 'package:treinta_y_tres/screens/stealth_mode/default_widgets/stealth_mode_body.dart';
export 'package:treinta_y_tres/screens/stealth_mode/custom_widgets/build_floating_button_item.dart';
export 'package:treinta_y_tres/screens/stealth_mode/custom_widgets/build_stealth_mode_info.dart';
export 'package:treinta_y_tres/screens/stealth_mode/custom_widgets/build_change_background.dart';
export 'package:treinta_y_tres/screens/stealth_mode/default_widgets/stealth_mode_drawer.dart';

// UI : Help request data
export 'package:treinta_y_tres/screens/help_request_data_screen/default_widgets/build_body.dart';
export 'package:treinta_y_tres/screens/help_request_data_screen/custom_widgets/build_back_navigation.dart';
export 'package:treinta_y_tres/screens/help_request_data_screen/custom_widgets/build_enter_map_button.dart';
export 'package:treinta_y_tres/screens/help_request_data_screen/custom_widgets/build_help_request_header.dart';
export 'package:treinta_y_tres/screens/help_request_data_screen/custom_widgets/build_help_request_item.dart';
export 'package:treinta_y_tres/screens/help_request_data_screen/help_request_data_screen.dart';

// UI : Google Map
export 'package:treinta_y_tres/screens/google_map_screen/default_widgets/build_body.dart';
export 'package:treinta_y_tres/screens/google_map_screen/google_map_screen.dart';
export 'package:treinta_y_tres/screens/google_map_screen/custom_widgets/build_exit_to_the_map.dart';
export 'package:treinta_y_tres/screens/google_map_screen/custom_widgets/build_user_data_dialog.dart';
export 'package:treinta_y_tres/screens/google_map_screen/custom_widgets/build_stats_info.dart';

// UI : Reusable widgets
export 'package:treinta_y_tres/reusable_widgets/build_custom_paint_appbar.dart';
export 'package:treinta_y_tres/screens_logic/messages/build_change_message_popup.dart';
export 'package:treinta_y_tres/reusable_widgets/custom_show_dialog.dart';
export 'package:treinta_y_tres/reusable_widgets/custom_scaffold_messenger.dart';
export 'package:treinta_y_tres/reusable_widgets/build_content.dart';
export 'package:treinta_y_tres/reusable_widgets/global_context_key.dart';
export 'package:treinta_y_tres/reusable_widgets/build_custom_text.dart';
export 'package:treinta_y_tres/reusable_widgets/build_custom_inactive_textfield_data.dart';
export 'package:treinta_y_tres/reusable_widgets/build_user_image.dart';

// Logic
export 'package:treinta_y_tres/screens_logic/select_image/pick_image.dart';
export 'package:treinta_y_tres/screens_logic/messages/contact_picker.dart';
export 'package:treinta_y_tres/screens_logic/lobby/copy_email.dart';
export 'package:treinta_y_tres/screens_logic/user_login/login_user.dart';
export 'package:treinta_y_tres/screens_logic/user_login/get_carmodel_name.dart';
export 'package:treinta_y_tres/screens_logic/user_login/get_drivertype_name.dart';
export 'package:treinta_y_tres/screens_logic/user_register/get_all_drivers_type.dart';
export 'package:treinta_y_tres/screens_logic/user_register/register_user.dart';
export 'package:treinta_y_tres/screens_logic/request_map_permissions/show_maps_request_permission.dart';
export 'package:treinta_y_tres/screens_logic/notifications/notification_popup.dart';

// Models
export 'package:treinta_y_tres/models/user.dart';

// API
export 'package:treinta_y_tres/application/application_endpoint.dart';