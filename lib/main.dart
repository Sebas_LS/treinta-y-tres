import 'package:treinta_y_tres/application/application_dependencies.dart';
import 'package:treinta_y_tres/screens/request_notifications_permission/request_notifications_permission.dart';
import 'package:treinta_y_tres/screens_logic/lobby/user_location_tracking.dart';
import 'models/update_profile_picture.dart';
import 'package:awesome_notifications/awesome_notifications.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  AwesomeNotifications().initialize(
      null,
      [
        NotificationChannel(
            channelGroupKey: 'basic_channel_group',
            channelKey: 'basic_channel',
            channelName: 'Basic notifications',
            channelDescription: 'Notification channel for basic tests',
            defaultColor: const Color(0xFF9D50DD),
            ledColor: Colors.white)
      ],
      channelGroups: [
        NotificationChannelGroup(
            channelGroupKey: 'basic_channel_group',
            channelGroupName: 'Basic group',
        )
      ],
  );

  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => ContactData()),
        ChangeNotifierProvider(create: (_) => IconDataNotifier()),
        ChangeNotifierProvider(create: (_) => RegisterNotifier()),
        ChangeNotifierProvider(create: (_) => DropdownSelectionModel()),
        ChangeNotifierProvider(create: (_) => BackgroundNotifier()),
        ChangeNotifierProvider(create: (_) => ConnectionStatusNotifier()),
        ChangeNotifierProvider(create: (_) => EmergencyContactsNotifier()),
        ChangeNotifierProvider(create: (_) => ContactUpdateNotifier()),
        ChangeNotifierProvider(create: (_) => AlertStatusNotifier()),
        ChangeNotifierProvider(create: (_) => ButtonLockNotifier()),
        ChangeNotifierProvider(create: (_) => AlertHistoryNotifier()),
        ChangeNotifierProvider(create: (_) => UserInDangerNotifier()),
        ChangeNotifierProvider(create: (_) => UserProfileNotifier()),
        ChangeNotifierProvider(create: (_) => LocationService()),
        ChangeNotifierProvider(create: (_) => User(
          id: 0, name: '',
          username: '',
          email: '',
          keyword: '',
          carModel: '',
          plaque: '',
          driverType: '',
          image: '',
        )),
        ChangeNotifierProvider(create: (_) => UserInDangerData(
          imageUrl: '',
          name: '',
          message: '',
          carModel: '',
          plaque: '',
          keyword: '',
          latitude: 0,
          longitude: 0,
        )),
        Provider<LobbySocketService>(create: (_) => LobbySocketService()),
        Provider<DataTransmissionSocketService>(create: (_) => DataTransmissionSocketService()),
      ],
      child: const TreintaYTresApplication(),
    ),);
  });
}

class TreintaYTresApplication extends StatefulWidget {
  static final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
  const TreintaYTresApplication({Key? key}) : super(key: key);

  @override
  State<TreintaYTresApplication> createState() => _TreintaYTresApplicationState();
}

class _TreintaYTresApplicationState extends State<TreintaYTresApplication> with WidgetsBindingObserver  {

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {checkLocationPermission();});
    WidgetsBinding.instance.addObserver(this);
    AwesomeNotifications().setListeners(
      onActionReceivedMethod: NotificationController.onActionReceivedMethod,
    );
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  void checkLocationPermission() async {
    bool? isLocationPermissionGranted = await SharedPrefManager.getBool('locationPermissionGranted');
    if (isLocationPermissionGranted == true) {
      Navigator.pushNamedAndRemoveUntil(navigatorKey.currentState!.context, '/login_user', (route) => false);
    } else {
      Navigator.pushNamedAndRemoveUntil(navigatorKey.currentState!.context, '/request_map_permission', (route) => false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: navigatorKey,
      debugShowCheckedModeBanner: false,
      title: 'Treinta Y Tres',
      initialRoute: '/request_map_permission',
      routes: {
        '/request_map_permission': (context) => const RequestMapPermission(),
        '/request_map_permission_denied': (context) => const RequestMapPermissionDenied(),
        '/request_notifications_permission': (context) => const RequestNotificationPermission(),
        '/login_user': (context) => const UserLogin(),
        '/register_user_data': (context) => const RegisterUserData(),
        '/register_user_car_data': (context) => const RegisterUserCarData(),
        '/register_user_extra_data': (context) => const RegisterUserExtraData(),
        '/user_agreement': (context) => const UserAgreement(),
        '/terms_and_conditions': (context) => const TermsAndConditions(),
        '/policy_and_privacy': (context) => const PolicyAndPrivacy(),
        '/disclaimer_and_terms': (context) => const DisclaimerAndTerms(),
        '/lobby' : (context) => const Lobby(),
        '/profile': (context) => const Profile(),
        '/messages': (context) => const Messages(),
        '/help_request_data': (context) => const HelpRequestDataScreen(),
        '/google_map': (context) => const GoogleMapScreen(),
      },
    );
  }
}

class NotificationController {

  @pragma("vm:entry-point")
  static Future <void> onActionReceivedMethod(ReceivedAction receivedAction) async {
    Navigator.pushNamedAndRemoveUntil(navigatorKey.currentState!.context, '/lobby', (route) => false);
    Provider.of<AlertHistoryNotifier>(navigatorKey.currentState!.context, listen: false).incrementReceivedAlerts();
    if (navigatorKey.currentState != null) {
      showDialog(
        context: navigatorKey.currentState!.context,
        builder: (BuildContext context) {
          return const UserInDangerDialog();
        },
      );
    }
  }
}
